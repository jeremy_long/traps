package traps.resources;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static traps.api.TechniqueTest.TECHNIQUE;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.mockito.Mockito;

import traps.api.Technique;
import traps.dao.TechniqueDAO;

import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientResponse;
import com.yammer.dropwizard.testing.ResourceTest;

public class TechniqueResourceTest extends ResourceTest {

	private final TechniqueDAO dao = Mockito.mock(TechniqueDAO.class);

	private static final String URL = "/techniques";

	@Override
	protected void setUpResources() {
		addResource(new TechniqueResource(dao));
	}

	@Test
	public void getAll() throws Exception {
		Mockito.when(dao.getAll()).thenReturn(Sets.newHashSet(TECHNIQUE));

		assertThat(client().resource(URL).get(Technique[].class)[0],
				is(equalTo(TECHNIQUE)));
	}

	@Test
	public void getByID() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(TECHNIQUE);

		assertThat(client().resource(URL + "/1").get(Technique.class),
				is(equalTo(TECHNIQUE)));
	}

	@Test
	public void getByIDNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1").get(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void post() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(TECHNIQUE);

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, TECHNIQUE);

		assertThat(response.getStatus(),
				is(equalTo(Status.CREATED.getStatusCode())));
		assertThat(response.getEntity(Technique.class), is(equalTo(TECHNIQUE)));
	}

	@Test
	public void postError() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenThrow(
				new NullPointerException());

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, TECHNIQUE);

		assertThat(response.getStatus(),
				is(equalTo(Status.BAD_REQUEST.getStatusCode())));
	}

	@Test
	public void put() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(TECHNIQUE);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, TECHNIQUE);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
		assertThat(response.getEntity(Technique.class), is(equalTo(TECHNIQUE)));
	}

	@Test
	public void putNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, TECHNIQUE);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void delete() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(TECHNIQUE);

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
	}

	@Test
	public void deleteNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}
}
