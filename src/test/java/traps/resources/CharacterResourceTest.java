package traps.resources;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static traps.api.CharacterClassTest.CHARACTER_CLASS;

import java.util.HashSet;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.mockito.Mockito;

import traps.api.Character;
import traps.dao.CharacterClassDAO;
import traps.dao.CharacterDAO;

import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientResponse;
import com.yammer.dropwizard.testing.ResourceTest;

public class CharacterResourceTest extends ResourceTest {

	private final Character CHARACTER = new Character(1, "John Smith", 1, 12,
			4, 6, 3, 3, 3, 2, 3, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			0, 3);

	private final CharacterDAO characterDAO = Mockito.mock(CharacterDAO.class);
	private final CharacterClassDAO characterClassDAO = Mockito
			.mock(CharacterClassDAO.class);

	private static final String URL = "/characters";

	@Override
	protected void setUpResources() {
		addResource(new CharacterResource(characterDAO, characterClassDAO));

		CHARACTER.setCharacterClass(CHARACTER_CLASS);
	}

	@Test
	public void getAll() throws Exception {
		Mockito.when(characterDAO.getAll()).thenReturn(
				Sets.newHashSet(CHARACTER));

		assertThat(client().resource(URL).get(Character[].class)[0],
				is(equalTo(CHARACTER)));
	}

	@Test
	public void getByID() throws Exception {
		Mockito.when(characterDAO.getByID(Mockito.anyInt())).thenReturn(
				Sets.newHashSet(CHARACTER));

		assertThat(client().resource(URL + "/1").get(Character.class),
				is(equalTo(CHARACTER)));
	}

	@Test
	public void getByIDNotFound() throws Exception {
		Mockito.when(characterDAO.getByID(Mockito.anyInt())).thenReturn(
				new HashSet<Character>());

		ClientResponse response = client().resource(URL + "/1").get(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void post() throws Exception {
		Mockito.when(characterDAO.getByID(Mockito.anyInt())).thenReturn(
				Sets.newHashSet(CHARACTER));
		Mockito.when(characterClassDAO.getByID(Mockito.anyInt())).thenReturn(
				CHARACTER_CLASS);

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, CHARACTER);

		assertThat(response.getStatus(),
				is(equalTo(Status.CREATED.getStatusCode())));
		assertThat(response.getEntity(Character.class), is(equalTo(CHARACTER)));
	}

	@Test
	public void postError() throws Exception {
		Mockito.when(characterDAO.getByID(Mockito.anyInt())).thenThrow(
				new NullPointerException());

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, CHARACTER);

		assertThat(response.getStatus(),
				is(equalTo(Status.BAD_REQUEST.getStatusCode())));
	}

	@Test
	public void put() throws Exception {
		Mockito.when(characterDAO.getByID(Mockito.anyInt())).thenReturn(
				Sets.newHashSet(CHARACTER));
		Mockito.when(characterClassDAO.getByID(Mockito.anyInt())).thenReturn(
				CHARACTER_CLASS);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, CHARACTER);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
		assertThat(response.getEntity(Character.class), is(equalTo(CHARACTER)));
	}

	@Test
	public void putNotFound() throws Exception {
		Mockito.when(characterDAO.getByID(Mockito.anyInt())).thenReturn(
				new HashSet<Character>());

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, CHARACTER);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void delete() throws Exception {
		Mockito.when(characterDAO.getByID(Mockito.anyInt())).thenReturn(
				Sets.newHashSet(CHARACTER));

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
	}

	@Test
	public void deleteNotFound() throws Exception {
		Mockito.when(characterDAO.getByID(Mockito.anyInt())).thenReturn(
				new HashSet<Character>());

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}
}
