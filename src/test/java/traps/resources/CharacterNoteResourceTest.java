package traps.resources;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static traps.api.CharacterNoteTest.CHARACTER_NOTE;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.mockito.Mockito;

import traps.api.CharacterNote;
import traps.dao.CharacterNoteDAO;

import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientResponse;
import com.yammer.dropwizard.testing.ResourceTest;

public class CharacterNoteResourceTest extends ResourceTest {

	private final CharacterNoteDAO dao = Mockito.mock(CharacterNoteDAO.class);

	private static final String URL = "/notes";

	@Override
	protected void setUpResources() {
		addResource(new CharacterNoteResource(dao));
	}

	@Test
	public void getAll() throws Exception {
		Mockito.when(dao.getAll()).thenReturn(Sets.newHashSet(CHARACTER_NOTE));

		assertThat(client().resource(URL).get(CharacterNote[].class)[0],
				is(equalTo(CHARACTER_NOTE)));
	}

	@Test
	public void getByID() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(CHARACTER_NOTE);

		assertThat(client().resource(URL + "/1").get(CharacterNote.class),
				is(equalTo(CHARACTER_NOTE)));
	}

	@Test
	public void getByIDNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1").get(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void post() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(CHARACTER_NOTE);

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, CHARACTER_NOTE);

		assertThat(response.getStatus(),
				is(equalTo(Status.CREATED.getStatusCode())));
		assertThat(response.getEntity(CharacterNote.class),
				is(equalTo(CHARACTER_NOTE)));
	}

	@Test
	public void postError() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenThrow(
				new NullPointerException());

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, CHARACTER_NOTE);

		assertThat(response.getStatus(),
				is(equalTo(Status.BAD_REQUEST.getStatusCode())));
	}

	@Test
	public void put() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(CHARACTER_NOTE);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, CHARACTER_NOTE);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
		assertThat(response.getEntity(CharacterNote.class),
				is(equalTo(CHARACTER_NOTE)));
	}

	@Test
	public void putNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, CHARACTER_NOTE);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void delete() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(CHARACTER_NOTE);

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
	}

	@Test
	public void deleteNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}
}
