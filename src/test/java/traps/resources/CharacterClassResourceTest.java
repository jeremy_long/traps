package traps.resources;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static traps.api.CharacterClassTest.CHARACTER_CLASS;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.mockito.Mockito;

import traps.api.CharacterClass;
import traps.dao.CharacterClassDAO;
import traps.dao.CharacterDAO;

import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientResponse;
import com.yammer.dropwizard.testing.ResourceTest;

public class CharacterClassResourceTest extends ResourceTest {

	private final CharacterClassDAO characterClassDAO = Mockito
			.mock(CharacterClassDAO.class);
	private final CharacterDAO characterDAO = Mockito.mock(CharacterDAO.class);

	private static final String URL = "/classes";

	@Override
	protected void setUpResources() {
		addResource(new CharacterClassResource(characterClassDAO, characterDAO));
	}

	@Test
	public void getAll() throws Exception {
		Mockito.when(characterClassDAO.getAll()).thenReturn(
				Sets.newHashSet(CHARACTER_CLASS));

		assertThat(client().resource(URL).get(CharacterClass[].class)[0],
				is(equalTo(CHARACTER_CLASS)));
	}

	@Test
	public void getByID() throws Exception {
		Mockito.when(characterClassDAO.getByID(Mockito.anyInt())).thenReturn(
				CHARACTER_CLASS);

		assertThat(client().resource(URL + "/1").get(CharacterClass.class),
				is(equalTo(CHARACTER_CLASS)));
	}

	@Test
	public void getByIDNotFound() throws Exception {
		Mockito.when(characterClassDAO.getByID(Mockito.anyInt())).thenReturn(
				null);

		ClientResponse response = client().resource(URL + "/1").get(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void post() throws Exception {
		Mockito.when(characterClassDAO.getByID(Mockito.anyInt())).thenReturn(
				CHARACTER_CLASS);

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, CHARACTER_CLASS);

		assertThat(response.getStatus(),
				is(equalTo(Status.CREATED.getStatusCode())));
		assertThat(response.getEntity(CharacterClass.class),
				is(equalTo(CHARACTER_CLASS)));
	}

	@Test
	public void postError() throws Exception {
		Mockito.when(characterClassDAO.getByID(Mockito.anyInt())).thenThrow(
				new NullPointerException());

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, CHARACTER_CLASS);

		assertThat(response.getStatus(),
				is(equalTo(Status.BAD_REQUEST.getStatusCode())));
	}

	@Test
	public void put() throws Exception {
		Mockito.when(characterClassDAO.getByID(Mockito.anyInt())).thenReturn(
				CHARACTER_CLASS);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, CHARACTER_CLASS);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
		assertThat(response.getEntity(CharacterClass.class),
				is(equalTo(CHARACTER_CLASS)));
	}

	@Test
	public void putNotFound() throws Exception {
		Mockito.when(characterClassDAO.getByID(Mockito.anyInt())).thenReturn(
				null);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, CHARACTER_CLASS);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void delete() throws Exception {
		Mockito.when(characterClassDAO.getByID(Mockito.anyInt())).thenReturn(
				CHARACTER_CLASS);

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
	}

	@Test
	public void deleteNotFound() throws Exception {
		Mockito.when(characterClassDAO.getByID(Mockito.anyInt())).thenReturn(
				null);

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}
}
