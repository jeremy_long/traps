package traps.resources;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static traps.api.CombatSkillTest.COMBAT_SKILL;
import static traps.api.WeaponTest.WEAPON;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.mockito.Mockito;

import traps.api.Weapon;
import traps.dao.CombatSkillDAO;
import traps.dao.WeaponDAO;

import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientResponse;
import com.yammer.dropwizard.testing.ResourceTest;

public class WeaponResourceTest extends ResourceTest {

	private final WeaponDAO weaponDao = Mockito.mock(WeaponDAO.class);
	private final CombatSkillDAO combatSkillDAO = Mockito
			.mock(CombatSkillDAO.class);

	private static final String URL = "/weapons";

	@Override
	protected void setUpResources() {
		addResource(new WeaponResource(weaponDao, combatSkillDAO));
	}

	@Test
	public void getAll() throws Exception {
		Mockito.when(weaponDao.getAll()).thenReturn(Sets.newHashSet(WEAPON));

		assertThat(client().resource(URL).get(Weapon[].class)[0],
				is(equalTo(WEAPON)));
	}

	@Test
	public void getByID() throws Exception {
		Mockito.when(weaponDao.getByID(Mockito.anyInt())).thenReturn(WEAPON);

		assertThat(client().resource(URL + "/1").get(Weapon.class),
				is(equalTo(WEAPON)));
	}

	@Test
	public void getByIDNotFound() throws Exception {
		Mockito.when(weaponDao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1").get(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void post() throws Exception {
		Mockito.when(weaponDao.getByID(Mockito.anyInt())).thenReturn(WEAPON);

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, WEAPON);

		assertThat(response.getStatus(),
				is(equalTo(Status.CREATED.getStatusCode())));
		assertThat(response.getEntity(Weapon.class), is(equalTo(WEAPON)));
	}

	@Test
	public void postError() throws Exception {
		Mockito.when(weaponDao.getByID(Mockito.anyInt())).thenThrow(
				new NullPointerException());

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, WEAPON);

		assertThat(response.getStatus(),
				is(equalTo(Status.BAD_REQUEST.getStatusCode())));
	}

	@Test
	public void put() throws Exception {
		Mockito.when(weaponDao.getByID(Mockito.anyInt())).thenReturn(WEAPON);
		Mockito.when(combatSkillDAO.getByID(Mockito.anyInt())).thenReturn(
				COMBAT_SKILL);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, WEAPON);

		Weapon weapon = response.getEntity(Weapon.class);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
		assertThat(weapon, is(equalTo(WEAPON)));
		assertThat(weapon.getCombatSkill(), is(equalTo(COMBAT_SKILL)));
	}

	@Test
	public void putWeaponNotFound() throws Exception {
		Mockito.when(weaponDao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, WEAPON);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void putCombatSkillNotFound() throws Exception {
		Mockito.when(weaponDao.getByID(Mockito.anyInt())).thenReturn(WEAPON);
		Mockito.when(combatSkillDAO.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, WEAPON);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void delete() throws Exception {
		Mockito.when(weaponDao.getByID(Mockito.anyInt())).thenReturn(WEAPON);

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
	}

	@Test
	public void deleteNotFound() throws Exception {
		Mockito.when(weaponDao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}
}
