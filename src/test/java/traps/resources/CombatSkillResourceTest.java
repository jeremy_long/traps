package traps.resources;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static traps.api.CombatSkillTest.COMBAT_SKILL;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.mockito.Mockito;

import traps.api.CombatSkill;
import traps.dao.CombatSkillDAO;

import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientResponse;
import com.yammer.dropwizard.testing.ResourceTest;

public class CombatSkillResourceTest extends ResourceTest {

	private final CombatSkillDAO dao = Mockito.mock(CombatSkillDAO.class);

	private static final String URL = "/combatSkills";

	@Override
	protected void setUpResources() {
		addResource(new CombatSkillResource(dao));
	}

	@Test
	public void getAll() throws Exception {
		Mockito.when(dao.getAll()).thenReturn(Sets.newHashSet(COMBAT_SKILL));

		assertThat(client().resource(URL).get(CombatSkill[].class)[0],
				is(equalTo(COMBAT_SKILL)));
	}

	@Test
	public void getByID() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(COMBAT_SKILL);

		assertThat(client().resource(URL + "/1").get(CombatSkill.class),
				is(equalTo(COMBAT_SKILL)));
	}

	@Test
	public void getByIDNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1").get(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void post() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(COMBAT_SKILL);

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, COMBAT_SKILL);

		assertThat(response.getStatus(),
				is(equalTo(Status.CREATED.getStatusCode())));
		assertThat(response.getEntity(CombatSkill.class),
				is(equalTo(COMBAT_SKILL)));
	}

	@Test
	public void postError() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenThrow(
				new NullPointerException());

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, COMBAT_SKILL);

		assertThat(response.getStatus(),
				is(equalTo(Status.BAD_REQUEST.getStatusCode())));
	}

	@Test
	public void put() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(COMBAT_SKILL);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, COMBAT_SKILL);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
		assertThat(response.getEntity(CombatSkill.class),
				is(equalTo(COMBAT_SKILL)));
	}

	@Test
	public void putNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, COMBAT_SKILL);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void delete() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(COMBAT_SKILL);

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
	}

	@Test
	public void deleteNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}
}
