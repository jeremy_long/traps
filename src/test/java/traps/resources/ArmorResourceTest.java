package traps.resources;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static traps.api.ArmorTest.ARMOR;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.mockito.Mockito;

import traps.api.Armor;
import traps.dao.ArmorDAO;

import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientResponse;
import com.yammer.dropwizard.testing.ResourceTest;

public class ArmorResourceTest extends ResourceTest {

	private final ArmorDAO dao = Mockito.mock(ArmorDAO.class);

	private static final String URL = "/armor";

	@Override
	protected void setUpResources() {
		addResource(new ArmorResource(dao));
	}

	@Test
	public void getAll() throws Exception {
		Mockito.when(dao.getAll()).thenReturn(Sets.newHashSet(ARMOR));

		assertThat(client().resource(URL).get(Armor[].class)[0],
				is(equalTo(ARMOR)));
	}

	@Test
	public void getByID() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(ARMOR);

		assertThat(client().resource(URL + "/1").get(Armor.class),
				is(equalTo(ARMOR)));
	}

	@Test
	public void getByIDNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1").get(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void post() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(ARMOR);

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, ARMOR);

		assertThat(response.getStatus(),
				is(equalTo(Status.CREATED.getStatusCode())));
		assertThat(response.getEntity(Armor.class), is(equalTo(ARMOR)));
	}

	@Test
	public void postError() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenThrow(
				new NullPointerException());

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, ARMOR);

		assertThat(response.getStatus(),
				is(equalTo(Status.BAD_REQUEST.getStatusCode())));
	}

	@Test
	public void put() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(ARMOR);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, ARMOR);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
		assertThat(response.getEntity(Armor.class), is(equalTo(ARMOR)));
	}

	@Test
	public void putNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, ARMOR);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void delete() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(ARMOR);

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
	}

	@Test
	public void deleteNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(null);

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}
}
