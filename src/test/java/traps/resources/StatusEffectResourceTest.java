package traps.resources;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static traps.api.StatusEffectTest.STATUS_EFFECT;

import java.util.HashSet;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.mockito.Mockito;

import traps.api.StatusEffect;
import traps.dao.StatusEffectDAO;

import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientResponse;
import com.yammer.dropwizard.testing.ResourceTest;

public class StatusEffectResourceTest extends ResourceTest {

	private final StatusEffectDAO dao = Mockito.mock(StatusEffectDAO.class);

	private static final String URL = "/statusEffects";

	@Override
	protected void setUpResources() {
		addResource(new StatusEffectResource(dao));
	}

	@Test
	public void getAll() throws Exception {
		Mockito.when(dao.getAll()).thenReturn(Sets.newHashSet(STATUS_EFFECT));

		assertThat(client().resource(URL).get(StatusEffect[].class)[0],
				is(equalTo(STATUS_EFFECT)));
	}

	@Test
	public void getByID() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(
				Sets.newHashSet(STATUS_EFFECT));

		assertThat(client().resource(URL + "/1").get(StatusEffect.class),
				is(equalTo(STATUS_EFFECT)));
	}

	@Test
	public void getByIDNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(
				new HashSet<StatusEffect>());

		ClientResponse response = client().resource(URL + "/1").get(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void post() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(
				Sets.newHashSet(STATUS_EFFECT));

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, STATUS_EFFECT);

		assertThat(response.getStatus(),
				is(equalTo(Status.CREATED.getStatusCode())));
		assertThat(response.getEntity(StatusEffect.class),
				is(equalTo(STATUS_EFFECT)));
	}

	@Test
	public void postError() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenThrow(
				new NullPointerException());

		ClientResponse response = client().resource(URL)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.post(ClientResponse.class, STATUS_EFFECT);

		assertThat(response.getStatus(),
				is(equalTo(Status.BAD_REQUEST.getStatusCode())));
	}

	@Test
	public void put() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(
				Sets.newHashSet(STATUS_EFFECT));

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, STATUS_EFFECT);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
		assertThat(response.getEntity(StatusEffect.class),
				is(equalTo(STATUS_EFFECT)));
	}

	@Test
	public void putNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(
				new HashSet<StatusEffect>());

		ClientResponse response = client().resource(URL + "/1")
				.type(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, STATUS_EFFECT);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}

	@Test
	public void delete() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(
				Sets.newHashSet(STATUS_EFFECT));

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(), is(equalTo(Status.OK.getStatusCode())));
	}

	@Test
	public void deleteNotFound() throws Exception {
		Mockito.when(dao.getByID(Mockito.anyInt())).thenReturn(
				new HashSet<StatusEffect>());

		ClientResponse response = client().resource(URL + "/1").delete(
				ClientResponse.class);

		assertThat(response.getStatus(),
				is(equalTo(Status.NOT_FOUND.getStatusCode())));
	}
}
