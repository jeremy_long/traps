package traps.api;

import static com.yammer.dropwizard.testing.JsonHelpers.asJson;
import static com.yammer.dropwizard.testing.JsonHelpers.fromJson;
import static com.yammer.dropwizard.testing.JsonHelpers.jsonFixture;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

public class TechniqueTest {

	public static final Technique TECHNIQUE = new Technique(1, "Power Blow", 2,
			1, 1, 1, 2, 1, 2, 1, 2, 3, "None", "None", 1);

	private static String TECHNIQUE_FIXTURE;

	@BeforeClass
	public static void setup() throws IOException {
		TECHNIQUE_FIXTURE = jsonFixture("fixtures/technique.json");
	}

	@Test
	public void serializesToJson() throws IOException {
		assertThat("Technique can be serialized to JSON", asJson(TECHNIQUE),
				is(equalTo(TECHNIQUE_FIXTURE)));
	}

	@Test
	public void deserializesFromJson() throws IOException {
		Technique technique = fromJson(TECHNIQUE_FIXTURE, Technique.class);
		assertThat("Technique can be deserialized from JSON", technique,
				is(equalTo(TECHNIQUE)));
		assertEquals(technique.getTechniqueName(), TECHNIQUE.getTechniqueName());
		assertEquals(technique.getPowerCost(), TECHNIQUE.getPowerCost());
		assertEquals(technique.getAccuracyCost(), TECHNIQUE.getAccuracyCost());
		assertEquals(technique.getFinesseCost(), TECHNIQUE.getFinesseCost());
		assertEquals(technique.getHitBonus(), TECHNIQUE.getHitBonus());
		assertEquals(technique.getDamageBonus(), TECHNIQUE.getDamageBonus());
		assertEquals(technique.getPierceBonus(), TECHNIQUE.getPierceBonus());
		assertEquals(technique.getSpeedBonus(), TECHNIQUE.getSpeedBonus());
		assertEquals(technique.getEvadeBonus(), TECHNIQUE.getEvadeBonus());
		assertEquals(technique.getDamageReductionBonus(),
				TECHNIQUE.getDamageReductionBonus());
		assertEquals(technique.getResistanceBonus(),
				TECHNIQUE.getResistanceBonus());
		assertEquals(technique.getExclusiveSet(), TECHNIQUE.getExclusiveSet());
		assertEquals(technique.getSpecialEffects(),
				TECHNIQUE.getSpecialEffects());
		assertEquals(technique.getTechniqueLevel(),
				TECHNIQUE.getTechniqueLevel());
	}
}
