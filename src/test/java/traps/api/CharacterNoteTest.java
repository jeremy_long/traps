package traps.api;

import static com.yammer.dropwizard.testing.JsonHelpers.asJson;
import static com.yammer.dropwizard.testing.JsonHelpers.fromJson;
import static com.yammer.dropwizard.testing.JsonHelpers.jsonFixture;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

public class CharacterNoteTest {

	public static final CharacterNote CHARACTER_NOTE = new CharacterNote(1, 1,
			"This is a note");

	private static String CHARACTER_NOTE_FIXTURE;

	@BeforeClass
	public static void setup() throws IOException {
		CHARACTER_NOTE_FIXTURE = jsonFixture("fixtures/characterNote.json");
	}

	@Test
	public void serializesToJson() throws IOException {
		assertThat("CharacterNote can be serialized to JSON",
				asJson(CHARACTER_NOTE), is(equalTo(CHARACTER_NOTE_FIXTURE)));
	}

	@Test
	public void deserializesFromJson() throws IOException {
		CharacterNote characterNote = fromJson(CHARACTER_NOTE_FIXTURE,
				CharacterNote.class);
		assertThat("CharacterNote can be deserialized from JSON",
				characterNote, is(equalTo(CHARACTER_NOTE)));
		assertEquals(characterNote.getCharacterID(),
				CHARACTER_NOTE.getCharacterID());
		assertEquals(characterNote.getText(), CHARACTER_NOTE.getText());
	}
}
