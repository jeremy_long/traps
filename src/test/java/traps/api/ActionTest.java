package traps.api;

import static com.yammer.dropwizard.testing.JsonHelpers.asJson;
import static com.yammer.dropwizard.testing.JsonHelpers.fromJson;
import static com.yammer.dropwizard.testing.JsonHelpers.jsonFixture;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static traps.api.TechniqueTest.TECHNIQUE;
import static traps.api.WeaponTest.WEAPON;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

public class ActionTest {

	private static final Action ACTION = new Action(1, 1, 1, 2, 3);

	private static String ACTION_FIXTURE;

	@BeforeClass
	public static void setup() throws IOException {
		ACTION_FIXTURE = jsonFixture("fixtures/action.json");
		ACTION.setWeapon(WEAPON);
		ACTION.addTechnique(TECHNIQUE);
	}

	@Test
	public void serializesToJson() throws IOException {
		assertThat("Action can be serialized to JSON", asJson(ACTION),
				is(equalTo(ACTION_FIXTURE)));
	}

	@Test
	public void deserializesFromJson() throws IOException {
		Action action = fromJson(ACTION_FIXTURE, Action.class);
		assertThat("Action can be deserialized from JSON", action,
				is(equalTo(ACTION)));
		assertEquals(action.getCombatantID(), ACTION.getCombatantID());
		assertEquals(action.getWeaponID(), ACTION.getWeaponID());
		assertEquals(action.getRound(), ACTION.getRound());
		assertEquals(action.getInitiative(), ACTION.getInitiative());
		assertEquals(action.getWeapon(), ACTION.getWeapon());
		assertEquals(action.getPowerCost(), ACTION.getPowerCost());
		assertEquals(action.getAccuracyCost(), ACTION.getAccuracyCost());
		assertEquals(action.getFinesseCost(), ACTION.getFinesseCost());
		assertEquals(action.getHitBonus(), ACTION.getHitBonus());
		assertEquals(action.getDamageBonus(), ACTION.getDamageBonus());
		assertEquals(action.getPierceBonus(), ACTION.getPierceBonus());
		assertEquals(action.getEvadeBonus(), ACTION.getEvadeBonus());
		assertEquals(action.getDamageReductionBonus(),
				ACTION.getDamageReductionBonus());
		assertEquals(action.getSpeed(), ACTION.getSpeed());
		assertEquals(action.getTechniques(), ACTION.getTechniques());
		assertEquals(action.getResistanceBonus(), ACTION.getResistanceBonus());
	}
}
