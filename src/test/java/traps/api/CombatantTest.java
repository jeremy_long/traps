package traps.api;

import static com.yammer.dropwizard.testing.JsonHelpers.asJson;
import static com.yammer.dropwizard.testing.JsonHelpers.fromJson;
import static com.yammer.dropwizard.testing.JsonHelpers.jsonFixture;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

public class CombatantTest {

	private static String COMBATANT_FIXTURE;

	public static final Character CHARACTER = new Character(1, "John Smith", 1,
			12, 4, 6, 3, 3, 3, 2, 3, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 0, 3);

	public static final Combatant COMBATANT = new Combatant(1, CHARACTER, 1,
			10, 4, 3, 2, 1, 1);

	@BeforeClass
	public static void setup() throws IOException {
		COMBATANT_FIXTURE = jsonFixture("fixtures/combatant.json");
	}

	@Test
	public void serializesToJson() throws IOException {
		assertThat("Combatant can be serialized to JSON", asJson(COMBATANT),
				is(equalTo(COMBATANT_FIXTURE)));
	}

	@Test
	public void deserializesFromJson() throws IOException {
		Combatant combatant = fromJson(COMBATANT_FIXTURE, Combatant.class);
		assertThat("Combatant can be deserialized from JSON", combatant,
				is(equalTo(COMBATANT)));

		// Verify non-derived stats
		assertEquals(combatant.getCharacter(), COMBATANT.getCharacter());
		assertEquals(combatant.getEncounterID(), COMBATANT.getEncounterID());
		assertEquals(combatant.getCurrentHp(), COMBATANT.getCurrentHp());
		assertEquals(combatant.getCurrentEp(), COMBATANT.getCurrentEp());
		assertEquals(combatant.getCurrentPower(), COMBATANT.getCurrentPower());
		assertEquals(combatant.getCurrentAccuracy(),
				COMBATANT.getCurrentAccuracy());
		assertEquals(combatant.getCurrentFinesse(),
				COMBATANT.getCurrentFinesse());
		assertEquals(combatant.getCurrentWild(), COMBATANT.getCurrentWild());
	}
}
