package traps.api;

import static com.yammer.dropwizard.testing.JsonHelpers.asJson;
import static com.yammer.dropwizard.testing.JsonHelpers.fromJson;
import static com.yammer.dropwizard.testing.JsonHelpers.jsonFixture;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

public class CharacterClassTest {

	public static final CharacterClass CHARACTER_CLASS = new CharacterClass(1,
			"Warrior", 1, 1, 2, 2, 2, 2, 1, 1, 1, 2, 6, 1, 4, 3);

	private static String CHARACTER_CLASS_FIXTURE;

	@BeforeClass
	public static void setup() throws IOException {
		CHARACTER_CLASS_FIXTURE = jsonFixture("fixtures/characterClass.json");
	}

	@Test
	public void serializesToJson() throws IOException {
		assertThat("CharacterClass can be serialized to JSON",
				asJson(CHARACTER_CLASS), is(equalTo(CHARACTER_CLASS_FIXTURE)));
	}

	@Test
	public void deserializesFromJson() throws IOException {
		CharacterClass characterClass = fromJson(CHARACTER_CLASS_FIXTURE,
				CharacterClass.class);
		assertThat("CharacterClass can be deserialized from JSON",
				characterClass, is(equalTo(CHARACTER_CLASS)));
		assertEquals(characterClass.getCharacterClassName(),
				CHARACTER_CLASS.getCharacterClassName());
		assertEquals(characterClass.getHpLevelBonus(),
				CHARACTER_CLASS.getHpLevelBonus());
		assertEquals(characterClass.getEpLevelBonus(),
				CHARACTER_CLASS.getEpLevelBonus());
		assertEquals(characterClass.getStrengthInitialBonus(),
				CHARACTER_CLASS.getStrengthInitialBonus());
		assertEquals(characterClass.getDexterityInitialBonus(),
				CHARACTER_CLASS.getDexterityInitialBonus());
		assertEquals(characterClass.getAgilityInitialBonus(),
				CHARACTER_CLASS.getAgilityInitialBonus());
		assertEquals(characterClass.getHealthInitialBonus(),
				CHARACTER_CLASS.getHealthInitialBonus());
		assertEquals(characterClass.getCunningInitialBonus(),
				CHARACTER_CLASS.getCunningInitialBonus());
		assertEquals(characterClass.getWillpowerInitialBonus(),
				CHARACTER_CLASS.getWillpowerInitialBonus());
		assertEquals(characterClass.getIntelligenceInitialBonus(),
				CHARACTER_CLASS.getIntelligenceInitialBonus());
		assertEquals(characterClass.getAttributeInitialBonus(),
				CHARACTER_CLASS.getAttributeInitialBonus());
		assertEquals(characterClass.getHpInitialBonus(),
				CHARACTER_CLASS.getHpInitialBonus());
		assertEquals(characterClass.getEpInitialBonus(),
				CHARACTER_CLASS.getEpInitialBonus());
		assertEquals(characterClass.getSkillInitialBonus(),
				CHARACTER_CLASS.getSkillInitialBonus());
		assertEquals(characterClass.getPassiveInitialBonus(),
				CHARACTER_CLASS.getPassiveInitialBonus());
	}
}
