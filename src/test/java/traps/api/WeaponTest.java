package traps.api;

import static com.yammer.dropwizard.testing.JsonHelpers.asJson;
import static com.yammer.dropwizard.testing.JsonHelpers.fromJson;
import static com.yammer.dropwizard.testing.JsonHelpers.jsonFixture;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static traps.api.CombatSkillTest.COMBAT_SKILL;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

public class WeaponTest {

	public static final Weapon WEAPON = new Weapon(1, "Club", 1, 1, 6, 1, 4, 1,
			1, 2, 1, COMBAT_SKILL);

	private static String WEAPON_FIXTURE;

	@BeforeClass
	public static void setup() throws IOException {
		WEAPON_FIXTURE = jsonFixture("fixtures/weapon.json");
	}

	@Test
	public void serializesToJson() throws IOException {
		assertThat("Weapon can be serialized to JSON", asJson(WEAPON),
				is(equalTo(WEAPON_FIXTURE)));
	}

	@Test
	public void deserializesFromJson() throws IOException {
		Weapon weapon = fromJson(WEAPON_FIXTURE, Weapon.class);
		assertThat("Weapon can be deserialized from JSON", weapon,
				is(equalTo(WEAPON)));
		assertEquals(weapon.getWeaponName(), WEAPON.getWeaponName());
		assertEquals(weapon.getHitBonus(), WEAPON.getHitBonus());
		assertEquals(weapon.getDamageDiceNumber(), WEAPON.getDamageDiceNumber());
		assertEquals(weapon.getDamageDiceType(), WEAPON.getDamageDiceType());
		assertEquals(weapon.getPierce(), WEAPON.getPierce());
		assertEquals(weapon.getSpeed(), WEAPON.getSpeed());
		assertEquals(weapon.getRange(), WEAPON.getRange());
		assertEquals(weapon.getHands(), WEAPON.getHands());
		assertEquals(weapon.getCost(), WEAPON.getCost());
		assertEquals(weapon.getEpCost(), WEAPON.getEpCost());
		assertEquals(weapon.getCombatSkill(), WEAPON.getCombatSkill());
	}
}
