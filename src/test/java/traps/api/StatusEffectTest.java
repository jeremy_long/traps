package traps.api;

import static com.yammer.dropwizard.testing.JsonHelpers.asJson;
import static com.yammer.dropwizard.testing.JsonHelpers.fromJson;
import static com.yammer.dropwizard.testing.JsonHelpers.jsonFixture;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isIn;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

public class StatusEffectTest {

	public static final StatusEffect STATUS_EFFECT = new StatusEffect(1,
			"Blind", "Can't see", 3, 1, true);

	private static String STATUS_EFFECT_FIXTURE;

	@BeforeClass
	public static void setup() throws IOException {
		STATUS_EFFECT_FIXTURE = jsonFixture("fixtures/statusEffect.json");
	}

	@Test
	public void serializesToJson() throws IOException {
		assertThat("StatusEffect can be serialized to JSON",
				asJson(STATUS_EFFECT), is(equalTo(STATUS_EFFECT_FIXTURE)));
	}

	@Test
	public void deserializesFromJson() throws IOException {
		StatusEffect statusEffect = fromJson(STATUS_EFFECT_FIXTURE,
				StatusEffect.class);
		assertThat("StatusEffect can be deserialized from JSON", statusEffect,
				is(equalTo(STATUS_EFFECT)));
		assertEquals(statusEffect.getStatusEffectName(),
				STATUS_EFFECT.getStatusEffectName());
		assertEquals(statusEffect.getStatusEffectDescription(),
				STATUS_EFFECT.getStatusEffectDescription());
		assertEquals(statusEffect.getModifier(), STATUS_EFFECT.getModifier());
		assertEquals(statusEffect.getRoundInflicted(),
				STATUS_EFFECT.getRoundInflicted());
		assertEquals(statusEffect.getPersistent(),
				STATUS_EFFECT.getPersistent());
	}

	@Test
	public void convertAttributeEffectsToJson() throws IOException {
		StatusEffect statusEffect = new StatusEffect(1, "Blind", "Can't see",
				3, 1, true);
		statusEffect.addAttributeEffect("Dexterity", -3);
		StatusEffect convertedStatusEffect = fromJson(asJson(statusEffect),
				StatusEffect.class);
		assertThat("Attribute effects can be converted to/from JSON",
				statusEffect.getAttributeEffects().entrySet(),
				everyItem(isIn(convertedStatusEffect.getAttributeEffects()
						.entrySet())));
		assertThat("Attribute effects can be converted to/from JSON",
				convertedStatusEffect.getAttributeEffects().entrySet(),
				everyItem(isIn(statusEffect.getAttributeEffects().entrySet())));
	}
}