package traps.api;

import static com.yammer.dropwizard.testing.JsonHelpers.asJson;
import static com.yammer.dropwizard.testing.JsonHelpers.fromJson;
import static com.yammer.dropwizard.testing.JsonHelpers.jsonFixture;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import com.yammer.dropwizard.validation.InvalidEntityException;

public class CombatSkillTest {

	public static final CombatSkill COMBAT_SKILL = new CombatSkill(1, "Blunt",
			"A", "B", "C");

	private static String COMBAT_SKILL_FIXTURE;

	@BeforeClass
	public static void setup() throws IOException {
		COMBAT_SKILL_FIXTURE = jsonFixture("fixtures/combatSkill.json");
	}

	@Test
	public void serializesToJson() throws IOException {
		assertThat("Combat Skill can be serialized to JSON",
				asJson(COMBAT_SKILL), is(equalTo(COMBAT_SKILL_FIXTURE)));
	}

	@Test
	public void deserializesFromJson() throws IOException {
		CombatSkill combatSkill = fromJson(COMBAT_SKILL_FIXTURE,
				CombatSkill.class);
		assertThat("Combat Skill can be deserialized from JSON", combatSkill,
				is(equalTo(COMBAT_SKILL)));
		assertEquals(combatSkill.getCombatSkillName(),
				COMBAT_SKILL.getCombatSkillName());
		assertEquals(combatSkill.getPowerRank(), COMBAT_SKILL.getPowerRank());
		assertEquals(combatSkill.getAccuracyRank(),
				COMBAT_SKILL.getAccuracyRank());
		assertEquals(combatSkill.getFinesseRank(),
				COMBAT_SKILL.getFinesseRank());
	}

	@Test(expected = InvalidEntityException.class)
	public void invalidPowerRank() {
		new CombatSkill(1, "Blunt", "blah", "B", "C");
	}

	@Test(expected = InvalidEntityException.class)
	public void invalidAccuracyRank() {
		new CombatSkill(1, "Blunt", "A", "blah", "C");
	}

	@Test(expected = InvalidEntityException.class)
	public void invalidFinesseRank() {
		new CombatSkill(1, "Blunt", "A", "B", "blah");
	}
}
