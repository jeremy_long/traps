package traps.api;

import static com.yammer.dropwizard.testing.JsonHelpers.asJson;
import static com.yammer.dropwizard.testing.JsonHelpers.fromJson;
import static com.yammer.dropwizard.testing.JsonHelpers.jsonFixture;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static traps.api.ArmorTest.ARMOR;
import static traps.api.CharacterClassTest.CHARACTER_CLASS;
import static traps.api.WeaponTest.WEAPON;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

public class CharacterTest {

	private static final Character CHARACTER = new Character(1, "John Smith",
			1, 12, 4, 6, 3, 3, 3, 2, 3, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 0, 3);

	private static String CHARACTER_FIXTURE;

	@BeforeClass
	public static void setup() throws IOException {
		CHARACTER_FIXTURE = jsonFixture("fixtures/character.json");
		CHARACTER.setCharacterClass(CHARACTER_CLASS);
		CHARACTER.addWeapon(WEAPON);
		CHARACTER.addArmor(ARMOR);
	}

	@Test
	public void serializesToJson() throws IOException {
		assertThat("Character can be serialized to JSON", asJson(CHARACTER),
				is(equalTo(CHARACTER_FIXTURE)));
	}

	@Test
	public void deserializesFromJson() throws IOException {
		Character character = fromJson(CHARACTER_FIXTURE, Character.class);
		assertThat("Character can be deserialized from JSON", character,
				is(equalTo(CHARACTER)));

		// Verify non-derived stats.
		assertEquals(character.getCharacterName(), CHARACTER.getCharacterName());
		assertEquals(character.getCharacterClass(),
				CHARACTER.getCharacterClass());
		assertEquals(character.getWeapons(), CHARACTER.getWeapons());
		assertEquals(character.getLevel(), CHARACTER.getLevel());
		assertEquals(character.getHp(), CHARACTER.getHp());
		assertEquals(character.getHpInitial(), CHARACTER.getHpInitial());
		assertEquals(character.getEp(), CHARACTER.getEp());
		assertEquals(character.getEpInitial(), CHARACTER.getEpInitial());
		assertEquals(character.getStrength(), CHARACTER.getStrength());
		assertEquals(character.getDexterity(), CHARACTER.getDexterity());
		assertEquals(character.getAgility(), CHARACTER.getAgility());
		assertEquals(character.getHealth(), CHARACTER.getHealth());
		assertEquals(character.getCunning(), CHARACTER.getCunning());
		assertEquals(character.getWillpower(), CHARACTER.getWillpower());
		assertEquals(character.getIntelligence(), CHARACTER.getIntelligence());
		assertEquals(character.getAttribute(), CHARACTER.getAttribute());
		assertEquals(character.getStrengthAttribute(),
				CHARACTER.getStrengthAttribute());
		assertEquals(character.getDexterityAttribute(),
				CHARACTER.getDexterityAttribute());
		assertEquals(character.getAgilityAttribute(),
				CHARACTER.getAgilityAttribute());
		assertEquals(character.getHealthAttribute(),
				CHARACTER.getHealthAttribute());
		assertEquals(character.getCunningAttribute(),
				CHARACTER.getCunningAttribute());
		assertEquals(character.getWillpowerAttribute(),
				CHARACTER.getWillpowerAttribute());
		assertEquals(character.getIntelligenceAttribute(),
				CHARACTER.getIntelligenceAttribute());
		assertEquals(character.getSkill(), CHARACTER.getSkill());
		assertEquals(character.getPower(), CHARACTER.getPower());
		assertEquals(character.getAccuracy(), CHARACTER.getAccuracy());
		assertEquals(character.getFinesse(), CHARACTER.getFinesse());
		assertEquals(character.getWild(), CHARACTER.getWild());
		assertEquals(character.getPassive(), CHARACTER.getPassive());
	}

	@Test
	public void levelUp() {
		Character levelTwoCharacter = copy(CHARACTER);

		levelTwoCharacter.levelUp();

		assertThat("Character gains appropriate attribute points upon level",
				levelTwoCharacter.getAttribute(),
				is(equalTo(CHARACTER.getAttribute()
						+ Character.ATTRIBUTE_PER_LEVEL)));
		assertThat("Character gains appropriate skill points upon level",
				levelTwoCharacter.getSkill(), is(equalTo(CHARACTER.getSkill()
						+ Character.SKILL_PER_LEVEL)));
		assertThat(
				"Character gains appropriate passive points upon level",
				levelTwoCharacter.getPassive(),
				is(equalTo(CHARACTER.getPassive() + Character.PASSIVE_PER_LEVEL)));
	}

	@Test
	public void makeLevelOne() {
		Character testCharacter = copy(CHARACTER);
		testCharacter.levelUp();

		testCharacter.makeLevelOne();

		assertThat("Resetting a character to level one sets appropriate HP",
				testCharacter.getHp(), is(equalTo(CHARACTER.getHp())));
		assertThat("Resetting a character to level one sets appropriate EP",
				testCharacter.getEp(), is(equalTo(CHARACTER.getEp())));
		assertThat(
				"Resetting a character to level one sets appropriate attribute points",
				testCharacter.getAttribute(), is(equalTo(CHARACTER
						.getCharacterClass().getAttributeInitialBonus())));
		assertThat(
				"Resetting a character to level one sets appropriate skill points",
				testCharacter.getSkill(), is(equalTo(CHARACTER
						.getCharacterClass().getSkillInitialBonus())));
		assertThat(
				"Resetting a character to level one sets appropriate passive points",
				testCharacter.getPassive(), is(equalTo(CHARACTER
						.getCharacterClass().getPassiveInitialBonus())));
	}

	@Test
	public void resetPoints() {
		Character testCharacter = copy(CHARACTER);

		testCharacter.resetPoints();

		assertThat(
				"Resetting a character deallocates all attribute points",
				testCharacter.getAttribute(),
				is(equalTo(CHARACTER.getAttribute()
						+ CHARACTER.getStrengthAttribute()
						+ CHARACTER.getDexterityAttribute()
						+ CHARACTER.getAgilityAttribute()
						+ CHARACTER.getHealthAttribute()
						+ CHARACTER.getCunningAttribute()
						+ CHARACTER.getWillpowerAttribute()
						+ CHARACTER.getIntelligenceAttribute())));
		assertThat("Resetting a character deallocates all skill points",
				testCharacter.getSkill(), is(equalTo(CHARACTER.getSkill()
						+ CHARACTER.getPower() + CHARACTER.getAccuracy()
						+ CHARACTER.getFinesse() + CHARACTER.getWild() * 2)));
	}

	@Test
	public void changeCharacterClass() {
		Character testCharacter = copy(CHARACTER);
		CharacterClass newCharacterClass = new CharacterClass(1, "Test", 1, 2,
				2, 2, 2, 2, 2, 2, 2, 3, 8, 5, 3, 4);

		testCharacter.setCharacterClass(newCharacterClass);

		assertThat("Changing a character's class revokes the old HP bonus",
				testCharacter.getHp(), is(equalTo(CHARACTER.getHp()
						+ newCharacterClass.getHpInitialBonus()
						- CHARACTER.getCharacterClass().getHpInitialBonus())));
		assertThat("Changing a character's class revokes the old EP bonus",
				testCharacter.getEp(), is(equalTo(CHARACTER.getEp()
						+ newCharacterClass.getEpInitialBonus()
						- CHARACTER.getCharacterClass().getEpInitialBonus())));
	}

	private Character copy(Character original) {
		Character copy = new Character(original.getCharacterID(),
				original.getCharacterName(), original.getLevel(),
				original.getHp(), original.getEp(), original.getHpInitial(),
				original.getEpInitial(), original.getStrength(),
				original.getDexterity(), original.getAgility(),
				original.getHealth(), original.getCunning(),
				original.getWillpower(), original.getIntelligence(),
				original.getAttribute(), original.getStrengthAttribute(),
				original.getDexterityAttribute(),
				original.getAgilityAttribute(), original.getHealthAttribute(),
				original.getCunningAttribute(),
				original.getWillpowerAttribute(),
				original.getIntelligenceAttribute(), original.getSkill(),
				original.getPower(), original.getAccuracy(),
				original.getFinesse(), original.getWild(),
				original.getPassive());
		copy.setCharacterClass(original.getCharacterClass());
		return copy;
	}
}
