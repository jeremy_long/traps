package traps.api;

import static com.yammer.dropwizard.testing.JsonHelpers.asJson;
import static com.yammer.dropwizard.testing.JsonHelpers.fromJson;
import static com.yammer.dropwizard.testing.JsonHelpers.jsonFixture;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static traps.api.CombatantTest.COMBATANT;

import java.io.IOException;
import java.util.Iterator;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

public class EncounterTest {

	private static final Encounter ENCOUNTER = new Encounter(1, "Encounter", 1);

	private static String ENCOUNTER_FIXTURE;

	@BeforeClass
	public static void setup() throws IOException {
		ENCOUNTER_FIXTURE = jsonFixture("fixtures/encounter.json");
		ENCOUNTER.addCombatant(COMBATANT);
	}

	@Test
	public void serializesToJson() throws IOException {
		assertThat("Encounter can be serialized to JSON", asJson(ENCOUNTER),
				is(equalTo(ENCOUNTER_FIXTURE)));
	}

	@Test
	public void deserializesFromJson() throws IOException {
		Encounter encounter = fromJson(ENCOUNTER_FIXTURE, Encounter.class);
		assertThat("Encounter can be deserialized from JSON", encounter,
				is(equalTo(ENCOUNTER)));
		assertEquals(encounter.getEncounterName(), ENCOUNTER.getEncounterName());
		assertEquals(encounter.getCurrentRound(), ENCOUNTER.getCurrentRound());
		assertEquals(encounter.getCombatants(), ENCOUNTER.getCombatants());
	}

	@Test
	public void sortCombatants() {
		Combatant combatant1 = Mockito.mock(Combatant.class);
		Combatant combatant2 = Mockito.mock(Combatant.class);

		Mockito.when(combatant1.getCombatantID()).thenReturn(1);
		Mockito.when(combatant2.getCombatantID()).thenReturn(2);

		Mockito.when(combatant1.compareTo(combatant2)).thenReturn(1);
		Mockito.when(combatant2.compareTo(combatant1)).thenReturn(-1);

		Encounter testEncounter = new Encounter(1, "Encounter", 1);
		testEncounter.addCombatant(combatant1);
		testEncounter.addCombatant(combatant2);

		testEncounter.sortCombatants();

		Iterator<Combatant> combatantIterator = testEncounter.getCombatants()
				.iterator();

		assertThat("Sorting combatants respects combatant's comparator",
				combatantIterator.next(), is(equalTo(combatant2)));
		assertThat("Sorting combatants respects combatant's comparator",
				combatantIterator.next(), is(equalTo(combatant1)));
	}
}
