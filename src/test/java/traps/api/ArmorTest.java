package traps.api;

import static com.yammer.dropwizard.testing.JsonHelpers.asJson;
import static com.yammer.dropwizard.testing.JsonHelpers.fromJson;
import static com.yammer.dropwizard.testing.JsonHelpers.jsonFixture;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import com.yammer.dropwizard.validation.InvalidEntityException;

public class ArmorTest {

	public static final Armor ARMOR = new Armor(1, "Helmet", 1, 2, 1, "HEAD",
			0, 10);

	private static String ARMOR_FIXTURE;

	@BeforeClass
	public static void setup() throws IOException {
		ARMOR_FIXTURE = jsonFixture("fixtures/armor.json");
	}

	@Test
	public void serializesToJson() throws IOException {
		assertThat("Armor can be serialized to JSON", asJson(ARMOR),
				is(equalTo(ARMOR_FIXTURE)));
	}

	@Test
	public void deserializesFromJson() throws IOException {
		Armor armor = fromJson(ARMOR_FIXTURE, Armor.class);
		assertThat("Armor can be deserialized from JSON", armor,
				is(equalTo(ARMOR)));
		assertEquals(armor.getCost(), ARMOR.getCost());
		assertEquals(armor.getDamageReduction(), ARMOR.getDamageReduction());
		assertEquals(armor.getEvade(), ARMOR.getEvade());
		assertEquals(armor.getWeight(), ARMOR.getWeight());
		assertEquals(armor.getType(), ARMOR.getType());
		assertEquals(armor.getHands(), ARMOR.getHands());
	}

	@Test(expected = InvalidEntityException.class)
	public void invalidArmorType() {
		new Armor(1, "Helmet", 1, 2, 1, "blah", 0, 10);
	}
}
