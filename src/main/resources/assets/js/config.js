// set the require.js configuration for your application
require.config({
    baseUrl:'assets',

    paths:{
        // libraries
        'jquery':'js/libs/jquery-1.8.2.min',
        //'jquery':'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min',
        'jquery.cookie':'js/libs/jquery.cookie',
        'underscore':'js/libs/underscore-min',
        'backbone':'js/libs/backbone-min',
        'bootstrap':'js/libs/bootstrap.min',
        // require.js text plugin
        'text':'js/libs/text',
        // paths for our code:
        'router':'js/router',
        'model':'js/model',
        'view':'js/view',
        'util':'js/util',
        'tpl':'tpl',
        'mustache':'js/libs/mustache',
        'viewmaster': 'js/libs/backbone.viewmaster.min',
        'handlebars':'js/libs/handlebars',
        'deepmodel':'js/libs/deep-model.min',
        'bootbox':'js/libs/bootbox.min'
    },
    // 'shim' js libs not friendly with AMD specification
    shim:{
        underscore:{
            exports:'_'
        },
        backbone:{
            deps:[ 'underscore', 'jquery'],
            exports:'Backbone'
        },
        mustache:{
        	exports:'Mustache'
        },
        viewmaster: {
        	deps:[ 'underscore', 'backbone'],
        	exports:'ViewMaster'
        },
        cookie: {
        	deps:['jquery'],
        	exports: 'Cookie'
        },
        handlebars: {
            exports:'Handlebars'
        },
        bootbox: {
            exports:'bootbox'
        }
    }
})

// this will bootstrap our application
require([
    'jquery', 'backbone', 'router'
], function ($, Backbone, Router) {
	// see http://backbonejs.org/#Sync-emulateHTTP
    // Must be false to enable CORS requests through the Jetty filter.
	Backbone.emulateHTTP = false;
    var app = new Router();
    Backbone.history.start({
        // routes without hashes
        // pushState:true
    })
})

