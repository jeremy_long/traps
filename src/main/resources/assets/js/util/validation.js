define([], function () {
	var ValidationUtils = {
		requiredFieldValidation: function(value) {
            if($.trim(value) == '') {
                return 'This field is required.';
            }
        },
        requiredNumberFieldValidation: function(value) {
            if($.trim(value) == '') {
                return 'This field is required.';
            }
            var numberValue = Number(value);
            if (Math.floor(numberValue) != value) {
                return 'Please enter a number.';
            }
        },
        requiredPositiveNumberFieldValidation: function(value) {
            if($.trim(value) == '') {
                return 'This field is required.';
            }
            var numberValue = Number(value);
            if (Math.floor(numberValue) != value || numberValue < 0) {
                return 'Please enter a positive number.';
            }
        },
        requiredNonZeroNumberFieldValidation: function(value) {
            if($.trim(value) == '') {
                return 'This field is required.';
            }
            var numberValue = Number(value);
            if (Math.floor(numberValue) != value || numberValue < 1) {
                return 'Please enter a number > 0.';
            }
        }
	};
	return ValidationUtils;
})