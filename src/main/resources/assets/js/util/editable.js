define([
    'util/validation', 'util/error'
], function (ValidationUtils, ErrorUtils) {
	var EditableUtils = {
		
		// Uses jQuery to select all DOM elements that belong to a variety of 'editable' classes, and then uses
		// the bootstrap-editable script to set up editing in popups. Each class implies a different level of validation,
		// and some are specific to certain types of data (eg. editableCombatSkill).
        makeEditable: function (view) {
            $.ajax({
            	// For development purposes, when working without an internet connection.
                //url: 'http://cdnjs.cloudflare.com/ajax/libs/x-editable/1.4.5/bootstrap-editable/js/bootstrap-editable.min.js',
                url: 'assets/js/libs/bootstrap-editable.min.js',
                dataType: "script",
                cache: true,          
                success: function () {
                
                	// Validate this editable field as required
                    $('.editable').editable({
                        placement: 'bottom',
                        validate: ValidationUtils.requiredFieldValidation,
                        success: function(response, newValue) {
                            if (newValue === 'None') {
                                newValue = null;
                            }
                            var map = {};
                            map[$(this).attr('name')] = $.trim(newValue);
                            view.collection.get($(this).attr('value')).save(map, {
                                error: ErrorUtils.fetch(view.collection),
                            });
                        },
                        error: ErrorUtils.log
                    });
                    
                    // Makes elements with the editableOptional class optional editable fields.
                    $('.editableOptional').editable({
                        placement: 'bottom',
                        success: function(response, newValue) {
                            if (newValue === 'None') {
                                newValue = null;
                            }
                            var map = {};
                            map[$(this).attr('name')] = $.trim(newValue);
                            view.collection.get($(this).attr('value')).save(map, {
                                error: ErrorUtils.fetch(view.collection),
                            });
                        },
                        error: ErrorUtils.log
                    });
                    
                    // Applies number validation to an editable field
                    $('.editableNumber').editable({
                        placement: 'bottom',
                        validate: ValidationUtils.requiredNumberFieldValidation,
                        success: function(response, newValue) {
                            var map = {};
                            map[$(this).attr('name')] = $.trim(newValue);
                            view.collection.get($(this).attr('value')).save(map, {
                                error: ErrorUtils.fetch(view.collection),
                            });
                        },
                        error: ErrorUtils.log
                    });
                    
                    // Applies positive number validation to an editable field
                    $('.editablePositiveNumber').editable({
                        placement: 'bottom',
                        validate: ValidationUtils.requiredPositiveNumberFieldValidation,
                        success: function(response, newValue) {
                            var map = {};
                            map[$(this).attr('name')] = $.trim(newValue);
                            view.collection.get($(this).attr('value')).save(map, {
                                error: ErrorUtils.fetch(view.collection),
                            });
                        },
                        error: ErrorUtils.log
                    });
                    
                    // Applies positive, non-zero validation to an editable field
                    $('.editablePositiveNonZero').editable({
                        placement: 'bottom',
                        validate: ValidationUtils.requiredNonZeroNumberFieldValidation,
                        success: function(response, newValue) {
                            var map = {};
                            map[$(this).attr('name')] = _.escape($.trim(newValue));
                            view.collection.get($(this).attr('value')).save(map, {
                                success: ErrorUtils.fetch(view.collection),
                                error: ErrorUtils.fetch(view.collection),
                            });
                        },
                        error: ErrorUtils.log
                    });
                    
                    // Edit the CombatSkill associated with a weapon
                    $('.editableCombatSkill').editable({
                        placement: 'bottom',
                        success: function(response, newValue) {
                            var weaponModel = view.collection.get($(this).attr('value'));
                            weaponModel.set({'combatSkill.combatSkillID':newValue});
                            weaponModel.save({'weaponName':weaponModel.attributes.weaponName}, {
                                success: function() {
                                    view.collection.fetch()
                                },
                                error: ErrorUtils.fetch(view.collection)
                            });
                        },
                        error: ErrorUtils.log
                    });
                    
                    // Edit the character class associated with a character
                    $('.editableCharacterClass').editable({
                        placement: 'bottom',
                        success: function(response, newValue) {
                            var characterModel = view.collection.get($(this).attr('value'));
                            characterModel.set({'characterClass.characterClassID':newValue});
                            characterModel.save({'characterName':characterModel.attributes.characterName}, {
                                success: ErrorUtils.fetch(view.collection),
                                error: ErrorUtils.fetch(view.collection)
                            });
                        },
                        error: ErrorUtils.log
                    });
                },
                error: ErrorUtils.log
            });
        },
	};
	return EditableUtils;
})