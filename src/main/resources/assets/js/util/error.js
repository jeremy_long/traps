define([], function () {
	var ErrorUtils = {
        log: function(jqXHR, textStatus, errorThrown) {
            console.log('Error: ' + errorThrown);
        },
        fetch: function(collection) {
            return function() {
                collection.fetch();
            }
        }
	}
	return ErrorUtils;
})