define([
    'handlebars', 'jquery', 'backbone', 'text!tpl/energy-modal.html', 'bootstrap'
], function (Handlebars, $, Backbone, EnergyModalHtml) {

    var EnergyModal = Backbone.View.extend({
        events:{
            'click button.submitModal':'submit'
        },
        initialize: function () {            
            var view = this, rendered,
                model = view.options.combatant.toJSON();
   
            var combatantModel = {
                combatant: model,
            };
            
            rendered = Handlebars.compile(EnergyModalHtml);
            $(view.el).html(rendered(combatantModel));
            
            $('.modal').modal().css({
                'margin-top': function () {
                    return window.pageYOffset-($(this).height() / 2 );
                }
            });
            view.$('.modal').modal('show');
        },
        submit: function (eventName) {
            var energyCost = $.trim(this.$('#energyCost').val()),
                that = this,
                combatant = that.options.combatant;

            var errors = new Object();

            if (energyCost.length < 1) {
                errors.energyCost = 'Please enter a number.';
            }
             
            if (this.hasProperties(errors)) {
                this.hideErrors();
                this.showErrors(errors);
                return;
            }

            that.options.combatant.save({'currentEp':(combatant.get('currentEp') - energyCost)}, {
                success: function() {
                    that.model.fetch({
                        success: function(energy) {
                            that.$('.modal').modal('hide');
                            that.undelegateEvents();
                        },
                    });
                },
            });
        },
    });

    return EnergyModal;
})