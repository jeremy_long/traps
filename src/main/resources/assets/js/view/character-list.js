define([
	'handlebars', 'jquery', 'backbone', 'view/character-modal', 'view/spendPoints-modal', 'text!tpl/characters.html', 'util/validation', 'util/error', 'util/editable'
], function (Handlebars, $, Backbone, CharacterModal, SpendPointsModal, CharacterTemplate, ValidationUtils, ErrorUtils, EditableUtils) {

    var CharacterListView = Backbone.View.extend({
        events:{
            'click button.spendPoints':'spendPoints',
            'click button.delete':'delete',
            'click button.create':'add', 
        },
        initialize: function () {
        	this.template = CharacterTemplate;
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.render, this);
            this.collection.on('remove', this.render, this);
            this.collection.on('set', this.render, this);
        },
        render: function () {
        	var view = this, rendered;
        	var characterCollection = {
        		characters: view.collection.toJSON(),
                characterClasses: view.options.characterClasses.toJSON()
        	}
            rendered = Handlebars.compile(view.template);
        	$(view.el).html(rendered(characterCollection));
            EditableUtils.makeEditable(view);
            return view;
        },
        delete: function(eventName) {
            var character = this.collection.get(eventName.target.value),
                that = this;
            character.destroy({
                success: function() {
                    that.collection.remove(character)
                },
                error: ErrorUtils.log
            });
        },
        add: function (event) {
            this.modal = new CharacterModal({
                el:'#modal-container',
                collection:this.collection,
                characterClasses:this.options.characterClasses,
                model:new CharacterModel
            });
        },
        spendPoints: function (event) {
            var character = this.collection.get(event.target.value);

            this.modal = new SpendPointsModal({
                el:'#modal-container',
                collection:this.collection,
                model:character
            });
        },
    });

    return CharacterListView;
})