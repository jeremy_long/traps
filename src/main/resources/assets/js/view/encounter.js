define([
    'handlebars', 'jquery', 'backbone', 'jquery.cookie', 'text!tpl/encounter.html', 'view/damage-modal', 'view/energy-modal', 'view/changePoints-modal', 'view/inflictStatus-modal', 'util/validation', 'util/error', 'util/editable'
], function (Handlebars, $, Backbone, Cookie, EncounterTemplate, DamageModal, EnergyModal, ChangePointsModal, InflictStatusModal, ValidationUtils, ErrorUtils, EditableUtils) {

    var EncounterView = Backbone.View.extend({
    	events:{
            'click button.addCombatant':'addCombatant',
            'click button.removeCombatant':'removeCombatant',
            'click button.nextRound':'nextRound',
            'click a.reduceHp':'reduceHp',
            'click a.reduceEp':'reduceEp',
            'click button.inflictStatus':'inflictStatus',
            'click button.removeStatus':'removeStatus',
            'click .technique':'submitAction',
            'click .weapon':'submitAction',
            'click a.changePoints':'changePoints',
        },
        initialize: function () {
            this.template = EncounterTemplate;
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.render, this);
            this.collection.on('remove', this.render, this);
            this.collection.on('set', this.render, this);

            this.model.on("change", this.render, this);
            this.model.on("fetch", this.render, this);
        },
        render: function () {
        	var view = this, rendered,
                model = view.model.toJSON();
   
        	var encounterModel = {
        		encounter: model,
                characters: view.options.characters.toJSON(),
                techniques: view.options.techniques.toJSON()
        	};
            
            rendered = Handlebars.compile(view.template);

			// Register Handlebars helpers for some client-side display logic
            Handlebars.registerHelper('addition', function(number1, number2) {
                return number1 + number2;
            });

            Handlebars.registerHelper('subtraction', function(number1, number2) {
                return number1 - number2;
            });

            Handlebars.registerHelper('techniqueColor', function(powerCost, accuracyCost, finesseCost) {
                if (powerCost > 0) {
                    return 'class="error"';
                } else if (accuracyCost > 0) {
                    return 'class="success"';
                } else {
                    return 'class="info"';
                }
            });

            Handlebars.registerHelper('usingWeapon', function(combatantID, weaponID) {
                var combatant;
                for (var i = 0; i < view.model.get('combatants').length; i++) {
                    if (view.model.get('combatants')[i].combatantID == combatantID) {
                        combatant = view.model.get('combatants')[i];
                        break;
                    }
                }
                if (combatant.actions[0].weaponID == weaponID) {
                    return 'checked';
                }
            });

            Handlebars.registerHelper('usingTechnique', function(combatantID, techniqueID) {
                var techniques,
                    combatant;
                for (var i = 0; i < view.model.get('combatants').length; i++) {
                    if (view.model.get('combatants')[i].combatantID == combatantID) {
                        combatant = view.model.get('combatants')[i];
                        techniques = view.model.get('combatants')[i].actions[0].techniques;
                        break;
                    }
                }
                for (var i = 0; i < techniques.length; i++) {
                    if (techniques[i].techniqueID == techniqueID) {
                        return 'checked="true"';
                    }
                }

                var technique = view.options.techniques.get(techniqueID),
                    currentPower = combatant.currentPower,
                    currentAccuracy = combatant.currentAccuracy,
                    currentFinesse = combatant.currentFinesse,
                    currentWild = combatant.currentWild,
                    powerCost = technique.get('powerCost'),
                    accuracyCost = technique.get('accuracyCost'),
                    finesseCost = technique.get('finesseCost');


                if (currentPower < powerCost) {
                    currentWild -= (powerCost - currentPower);
                }
                if (currentAccuracy < accuracyCost) {
                    currentWild -= (accuracyCost - currentAccuracy);
                }
                if (currentFinesse < finesseCost) {
                    currentWild -= (finesseCost - currentFinesse);
                }
                if (currentWild < 0) {
                    return 'style="display:none;"';
                }

                if (technique.get('exclusiveSet') !== null && technique.get('exclusiveSet').length > 0) {
                    for (var i = 0; i < techniques.length; i++) {
                        if (techniques[i].exclusiveSet === technique.get('exclusiveSet')) {
                            return 'style="display:none;"';
                        }
                    }
                }
            });

            $(view.el).html(rendered(encounterModel));         
            EditableUtils.makeEditable(view);

            //show the last visible group
            var last=$.cookie('activeAccordionGroup');
            if (last!=null) {
                view.$("#"+last).addClass('in');
            }

            //when a group is shown, save it as the active accordion group
            view.$("#accordion2").bind('shown', function() {
                var active=view.$("#accordion2 .in").attr('id');
                $.cookie('activeAccordionGroup', active);
            });
            view.$("#accordion2").bind('hidden', function() {
                var active=view.$("#accordion2 .in").attr('id');
                if (active == null) {
                    $.cookie('activeAccordionGroup', null);
                }
            });

        	return view;
        },
        addCombatant: function() {
            var view = this,
                characterID = this.$('#newCombatant').val(),
                combatant = new CombatantModel();

            combatant.set({'character.characterID':characterID});
            combatant.save({'encounterID':view.model.get('encounterID')}, {
                success: function() {
                    view.model.fetch()
                },
            });
        },
        removeCombatant: function(event) {
            var view = this,
                combatantID = this.$('#removeCombatant').val(),
                combatant = new CombatantModel();

            combatant.set({'combatantID':combatantID});
            combatant.destroy({
                success: function() {
                    view.model.fetch();
                },
                error: ErrorUtils.log,
            });
        },
        submitAction: function(event) {
            var view = this,
                action = new ActionModel(),
                newTechniques = [],
                techniques = this.options.techniques.toJSON(),
                combatant,
                weapons,
                weaponID;

            for (var i = 0; i < view.model.get('combatants').length; i++) {
                if (view.model.get('combatants')[i].combatantID == event.target.value) {
                    combatant = view.model.get('combatants')[i];
                    action.set({'actionID':view.model.get('combatants')[i].actions[0].actionID});
                    break;
                }
            }

            for (var i = 0; i < techniques.length; i++) {
                if (this.$('#combatant'+event.target.value+'technique'+techniques[i].techniqueID).is(":checked")) {
                    newTechniques.push(techniques[i]);
                }
            }

            weapons = combatant.character.weapons;
            for (var i = 0; i < weapons.length; i++) {
                if (this.$('#combatant'+event.target.value+'weapon'+weapons[i].weaponID).is(":checked")) {
                    weaponID = weapons[i].weaponID;
                    break;
                }
            }

            action.save({'combatantID':event.target.value, 'weaponID':weaponID, 'techniques':newTechniques}, {
                success: function() {
                    view.model.fetch();
                },
                error: function() {
                    view.model.fetch();
                },
            });
        },
        nextRound: function(event) {
            var view = this,
                nextRound = view.model.get('currentRound') + 1;

            view.model.save({'currentRound':nextRound}, {
                success: function() {
                    view.model.fetch();
                },
                error: function() {
                    view.model.fetch();
                },
            });
        },
        reduceHp: function (event) {
            var combatant = new CombatantModel(),
                that = this;

            combatant.set({'combatantID':$(event.target).attr('value')});
            combatant.fetch({
                success: function() {
                    that.modal = new DamageModal({
                        el:'#modal-container',
                        collection:that.collection,
                        model:that.model,
                        combatant:combatant
                    });  
                },
            });       
        },
        reduceEp: function (event) {
            var combatant = new CombatantModel(),
                that = this;

            combatant.set({'combatantID':$(event.target).attr('value')});
            combatant.fetch({
                success: function() {
                    that.modal = new EnergyModal({
                        el:'#modal-container',
                        collection:that.collection,
                        model:that.model,
                        combatant:combatant
                    });  
                },
            });     
        },
        changePoints: function (event) {
            var combatant = new CombatantModel(),
                that = this;

            combatant.set({'combatantID':$(event.target).attr('value')});
            combatant.fetch({
                success: function() {
                    that.modal = new ChangePointsModal({
                        el:'#modal-container',
                        collection:that.collection,
                        model:that.model,
                        combatant:combatant
                    });  
                },
            });         
        },
        inflictStatus: function (event) {
            var combatant = new CombatantModel(),
                that = this;

            combatant.set({'combatantID':event.target.value});
            combatant.fetch({
                success: function() {
                    that.modal = new InflictStatusModal({
                        el:'#modal-container',
                        collection:that.collection,
                        model:that.model,
                        combatant:combatant,
                        statusEffects:that.options.statusEffects
                    });  
                },
            });         
        },
        removeStatus: function (event) {
            var combatant = new CombatantModel(),
                that = this;

            combatant.set({'combatantID':event.target.value});
            combatant.fetch({
                success: function() {
                    var statusEffects = combatant.get('statusEffects');
                    statusEffects.splice(event.target.name, 1);
                    combatant.save({'statusEffects':statusEffects}, {
                        success: function() {
                            that.model.fetch();
                        },
                    });
                },
            });         
        },
    });

    return EncounterView;
})