define([
	'handlebars', 'jquery', 'backbone', 'text!tpl/techniques.html', 'util/validation', 'util/error', 'view/technique-modal', 'util/editable'
], function (Handlebars, $, Backbone,  TechniqueTemplate, ValidationUtils, ErrorUtils, TechniqueModal, EditableUtils) {

    var TechniqueListView = Backbone.View.extend({
        events:{
            'click button.delete':'delete',
            'click button.create':'add',
            'change #csvFile':'uploadCSV',    
        },
        initialize: function () {
        	this.template = TechniqueTemplate;
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.render, this);
            this.collection.on('remove', this.render, this);
            this.collection.on('set', this.render, this);
        },
        render: function () {
        	var view = this, rendered;
        	var techniqueCollection = {
        		techniques: view.collection.toJSON(),
        	};

            rendered = Handlebars.compile(view.template);
            $(view.el).html(rendered(techniqueCollection));
            EditableUtils.makeEditable(view);
            return view;
        },
        delete: function(eventName) {
            var element = this.collection.get(eventName.target.value),
                that = this;
            element.destroy({
                success: function() {
                    that.collection.remove(element);
                },
            });
        },
        add: function (event) {
            this.modal = new TechniqueModal({
                el:'#modal-container',
                collection:this.collection,
                model:new TechniqueModel
            });
        },
        uploadCSV: function(event) {
            var reader = new FileReader(),
                file = event.target.files[0],
                that = this;
           
            reader.onload = (function(theFile, model, that) {
                return function(e) {
                    var text = e.target.result;

                    // tolerate both Windows and Unix linebreaks
                    var lines = text.split(/[\r\n]+/g); 

                    text = '';
                    for(var i = 0; i < lines.length; i++) {
                        lines[i] += '\n';
                        text += lines[i];
                    }
                    model.save({'csv':text + '\n'}, {
                        success: function() {
                            that.collection.fetch();
                        },
                    })    
                };
            })(file, new CSVModel(), that);
            
            reader.readAsText(file);
        },

    });
    return TechniqueListView;
})