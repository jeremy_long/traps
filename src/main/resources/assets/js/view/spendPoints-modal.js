define([
    'mustache', 'jquery', 'backbone', 'text!tpl/spendPoints-modal.html', 'bootstrap'
], function (Mustache, $, Backbone, SpendPointsModalHtml) {

    var SpendPointsModal = Backbone.View.extend({
        events:{
            'click button.spendPointsModal':'submit'
        },
        initialize: function () {
          	//create and show the modal
            var character = {
                character: this.model.toJSON()
            };
            
            rendered = Mustache.to_html(SpendPointsModalHtml, character);
            this.$el.html(rendered);

            $('.modal').modal().css(
            {
                'margin-top': function () {
                    return window.pageYOffset-($(this).height() / 2 );
                }
            });
            this.$('.modal').modal('show');
        },
        submit: function (eventName) {
            var that = this,
                attribute = this.$('#attribute').val(),
                strengthAttribute = this.$('#strengthTotal').val() - this.model.get('strength'),
                dexterityAttribute = this.$('#dexterityTotal').val() - this.model.get('dexterity'),
                agilityAttribute = this.$('#agilityTotal').val() - this.model.get('agility'),
                healthAttribute = this.$('#healthTotal').val() - this.model.get('health'),
                cunningAttribute = this.$('#cunningTotal').val() - this.model.get('cunning'),
                willpowerAttribute = this.$('#willpowerTotal').val() - this.model.get('willpower'),
                intelligenceAttribute = this.$('#intelligenceTotal').val() - this.model.get('intelligence'),
                skill = this.$('#skill').val(),
                power = this.$('#power').val(),
                accuracy = this.$('#accuracy').val(),
                finesse = this.$('#finesse').val(),
                wild = this.$('#wild').val();

            that.model.save({'attribute':attribute, 'strengthAttribute':strengthAttribute,'dexterityAttribute':dexterityAttribute, 'agilityAttribute':agilityAttribute, 'healthAttribute':healthAttribute, 'cunningAttribute':cunningAttribute, 'willpowerAttribute':willpowerAttribute, 'intelligenceAttribute':intelligenceAttribute, 'skill':skill, 'power':power, 'accuracy':accuracy, 'finesse':finesse, 'wild':wild}, {
                success: function(character) {
                    that.collection.fetch();
                    that.$('.modal').modal('hide');
                    that.undelegateEvents();
                },
                error: function(model, response) {
                }
            });
        },
    });

    return SpendPointsModal;
})