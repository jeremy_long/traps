define([
    'mustache', 'jquery', 'backbone', 'text!tpl/weapon-modal.html', 'bootstrap'
], function (Mustache, $, Backbone, WeaponModalHtml) {

    var WeaponModal = Backbone.View.extend({
        events:{
            'click button.submitModal':'submit'
        },
        initialize: function () {
            var combatSkillCollection = {
                combatSkills: this.options.combatSkills.toJSON()
            };
            
            rendered = Mustache.to_html(WeaponModalHtml, combatSkillCollection);
            this.$el.html(rendered);

            $('.modal').modal().css(
            {
                'margin-top': function () {
                    return window.pageYOffset-($(this).height() / 2 );
                }
            });
            this.$('.modal').modal('show');
        },
        submit: function (eventName) {
        	var weaponName = $.trim(this.$('#weaponName').val()),
                hitBonus = this.$('#hitBonus').val(),
                damageDiceNumber = this.$('#damageDiceNumber').val(),
                damageDiceType = this.$('#damageDiceType').val(),
                pierce = this.$('#pierce').val(),
                speed = this.$('#speed').val(),
                range = this.$('#range').val(),
                epCost = this.$('#epCost').val(),
                hands = this.$('#hands').val(),
                cost = this.$('#cost').val(),
                combatSkill = this.$('#combatSkill').val(),
                that = this;

            var errors = new Object();

            if (weaponName.length < 1) {
            	errors.weaponName = 'Weapon name must not be empty.';
            }
            var errorMessage = 'Please enter a positive integer.';
            if (hitBonus.length < 1) { errors.hitBonus = errorMessage; }
            if (damageDiceNumber.length < 1) { errors.damageDiceNumber = errorMessage; }
            if (damageDiceType.length < 1) { errors.damageDiceType = errorMessage; }
            if (pierce.length < 1) { errors.pierce = errorMessage; }
            if (speed.length < 1) { errors.speed = errorMessage; }
            if (range.length < 1) { errors.range = errorMessage; }
            if (epCost.length < 1) { errors.epCost = errorMessage; }
            if (hands.length < 1) { errors.hands = errorMessage; }
            if (cost.length < 1) { errors.cost = errorMessage; }
            
            if (this.hasProperties(errors)) {
                this.hideErrors();
                this.showErrors(errors);
                return;
            }

            var newWeaponModel = new WeaponModel();
            newWeaponModel.set({'combatSkill':that.options.combatSkills.get(combatSkill).toJSON()})//.combatSkillID':combatSkill});
            newWeaponModel.save({weaponName:_.escape(weaponName), 'hitBonus':hitBonus, 'damageDiceType':damageDiceType, 'damageDiceNumber':damageDiceNumber, 'pierce':pierce, 'speed':speed, 'range':range, 'epCost':epCost, 'hands':hands, 'cost':cost}, {
                success: function(weapon) {
                    that.collection.push(weapon);
                    that.collection.fetch();
                    that.$('.modal').modal('hide');
                    that.undelegateEvents();
                },
            });
        },
    });

    return WeaponModal;
})