define([
    'mustache', 'jquery', 'backbone', 'text!tpl/technique-modal.html', 'bootstrap'
], function (Mustache, $, Backbone, TechniqueModalHtml) {

    var TechniqueModal = Backbone.View.extend({
        events:{
            'click button.submitModal':'submit'
        },
        initialize: function () {            
            this.$el.html(TechniqueModalHtml);
            
            $('.modal').modal().css(
            {
                'margin-top': function () {
                    return window.pageYOffset-($(this).height() / 2 );
                }
            });
            this.$('.modal').modal('show');
        },
        submit: function (eventName) {
            var techniqueName = $.trim(this.$('#techniqueName').val()),
                powerCost = this.$('#powerCost').val(),
                accuracyCost = this.$('#accuracyCost').val(),
                finesseCost = this.$('#finesseCost').val(),
                hitBonus = this.$('#hitBonus').val(),
                damageBonus = this.$('#damageBonus').val(),
                pierceBonus = this.$('#pierceBonus').val(),
                speedBonus = this.$('#speedBonus').val(),
                evadeBonus = this.$('#evadeBonus').val(),
                damageReductionBonus = this.$('#damageReductionBonus').val(),
                resistanceBonus = this.$('#resistanceBonus').val(),
                specialEffects = this.$('#specialEffects').val(),
                type = this.$('#type').val(),
                techniqueLevel = this.$('#techniqueLevel').val(),
                that = this;

            var errors = new Object();

            if (techniqueName.length < 1) {
                errors.techniqueName = 'Technique name must not be empty.';
            }
            var errorMessage = 'Please enter a positive integer.';
            if (powerCost.length < 1) { errors.powerCost = errorMessage; }
            if (accuracyCost.length < 1) { errors.accuracyCost = errorMessage; }
            if (finesseCost.length < 1) { errors.finesseCost = errorMessage; }
            if (damageBonus.length < 1) { errors.damageBonus = errorMessage; }
            if (pierceBonus.length < 1) { errors.pierceBonus = errorMessage; }
            if (speedBonus.length < 1) { errors.speedBonus = errorMessage; }
            if (evadeBonus.length < 1) { errors.evadeBonus = errorMessage; }
            if (damageReductionBonus.length < 1) { errors.damageReductionBonus = errorMessage; }
            if (techniqueLevel.length < 1) { errors.techniqueLevel = errorMessage;}
            
            if (this.hasProperties(errors)) {
                this.hideErrors();
                this.showErrors(errors);
                return;
            }

            if (type === 'None') {
                type = null;
            }

            var newTechniqueModel = new TechniqueModel();
            newTechniqueModel.save({'techniqueName':techniqueName, 'powerCost':powerCost, 'accuracyCost':accuracyCost, 'finesseCost':finesseCost, 'hitBonus':hitBonus, 'damageBonus':damageBonus, 'pierceBonus':pierceBonus, 'speedBonus':speedBonus, 'evadeBonus':evadeBonus, 'damageReductionBonus':damageReductionBonus, 'exclusiveSet':type, 'specialEffects':specialEffects, 'techniqueLevel':techniqueLevel, 'resistanceBonus':resistanceBonus}, {
                success: function(technique) {
                    that.collection.push(technique);
                    that.collection.fetch();
                    that.$('.modal').modal('hide');
                    that.undelegateEvents();
                },
            });
        },
    });

    return TechniqueModal;
})