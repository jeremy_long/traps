define([
    'mustache', 'jquery', 'backbone', 'view/encounter-modal', 'text!tpl/encounters.html', 'viewmaster', 'text!tpl/encounter-layout.html', 'view/encounter-menu', 'view/encounter'
], function (Mustache, $, Backbone, EncounterModal, EncountersTemplate, ViewMaster, EncounterLayoutTemplate, EncounterMenuView, EncounterView) {

    var EncounterLayoutView = Backbone.ViewMaster.extend({
    	constructor: function() {
    		Backbone.ViewMaster.prototype.constructor.apply(this, arguments);
    		
    		var encounterMenuView = new EncounterMenuView({
    			    collection: this.collection,
    		});
    		this.setView('.encounter-list', encounterMenuView);
    		if(this.model) {
    			this.showEncounter();
    			encounterMenuView.highlight('#' + this.model.id);
    		} 
    		else {
    			this.addEncounter();
    		}
    	},
        template: function(context) {
        	return EncounterLayoutTemplate;
        },
        initialize: function () {
            this.collection.on('reset', this.refreshViews, this);
            this.collection.on('add', this.refreshViews, this);
            this.collection.on('remove', this.refreshViews, this);
            this.collection.on('set', this.refreshViews, this);
        },
        events:{
            'click button.create':'addEncounter'
        },
        addEncounter: function(event) {
            this.modal = new EncounterModal({
                el:'#modal-container',
                collection:this.collection,
                model:new EncounterModel
            });
           this.refreshViews();
        },
        showEncounter: function() {
        	this.setView('.encounter-content', new EncounterView({
                model:this.model,
                collection:this.collection,
                characters:this.options.characters,
                techniques:this.options.techniques
           }));
        }
    });

    return EncounterLayoutView;
})