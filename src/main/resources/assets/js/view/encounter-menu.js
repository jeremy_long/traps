define([
    'mustache', 'jquery', 'backbone', 'view/encounter-modal', 'text!tpl/encounter-menu.html'
], function (Mustache, $, Backbone, EncounterModal, EncounterMenuTemplate) {

    var EncounterMenuView = Backbone.View.extend({
        initialize: function () {
        	this.template = EncounterMenuTemplate;
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.render, this);
        },
        render: function () {
        	var view = this, rendered;
        	var encounterCollection = {
        		encounters: view.collection.toJSON()
        	}
        	
        	rendered = Mustache.to_html(view.template, encounterCollection);
        	$(view.el).html(rendered);
        	return view;
        },
        highlight: function(selected) {
        	// highlight the selected item
        	$('.nav li', '.encounter-list').removeClass('active');
        	$(selected).addClass('active');
        }
    });

    return EncounterMenuView;
})