define([
    'handlebars', 'jquery', 'backbone', 'text!tpl/changePoints-modal.html', 'bootstrap'
], function (Handlebars, $, Backbone, ChangePointsModalHtml) {

    var ChangePointsModal = Backbone.View.extend({
        events:{
            'click button.submitModal':'submit'
        },
        initialize: function () {            
            var view = this, rendered,
                model = view.options.combatant.toJSON();
   
            var combatantModel = {
                combatant: model,
            };
            
            rendered = Handlebars.compile(ChangePointsModalHtml);
            $(view.el).html(rendered(combatantModel));
            
            $('.modal').modal().css({
                'margin-top': function () {
                    return window.pageYOffset-($(this).height() / 2 );
                }
            });
            view.$('.modal').modal('show');
        },
        submit: function (eventName) {
            var power = $.trim(this.$('#power').val()),
                accuracy = $.trim(this.$('#accuracy').val()),
                finesse = $.trim(this.$('#finesse').val()),
                wild = $.trim(this.$('#wild').val()),
                that = this,
                combatant = that.options.combatant;

            var errors = new Object();

            if (power.length < 1) { errors.power = 'Please enter a number.'; }
            if (accuracy.length < 1) { errors.accuracy = 'Please enter a number.'; }
            if (finesse.length < 1) { errors.finesse = 'Please enter a number.'; }
            if (wild.length < 1) { errors.wild = 'Please enter a number.'; }

            if (this.hasProperties(errors)) {
                this.hideErrors();
                this.showErrors(errors);
                return;
            }

            that.options.combatant.save({'currentPower':power, 'currentAccuracy':accuracy, 'currentFinesse':finesse, 'currentWild':wild}, {
                success: function() {
                    that.model.fetch({
                        success: function(energy) {
                            that.$('.modal').modal('hide')
                            that.undelegateEvents()
                        },
                    });
                },
            });
        },
    });

    return ChangePointsModal;
})