define([
	'handlebars', 'jquery', 'backbone', 'view/encounter-modal', 'text!tpl/encounters.html', 'util/editable', 'util/error'
], function (Handlebars, $, Backbone, EncounterModal, EncounterTemplate, EditableUtils, ErrorUtils) {

    var EncounterListView = Backbone.View.extend({
        events:{
            'click button.delete':'delete',
            'click button.create':'add', 
        },
        initialize: function () {
        	this.template = EncounterTemplate;
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.render, this);
            this.collection.on('remove', this.render, this);
            this.collection.on('set', this.render, this);
        },
        render: function () {
        	var view = this, rendered;
        	var encounterCollection = {
        		encounters: view.collection.toJSON(),
        	};

            rendered = Handlebars.compile(view.template);
        	$(view.el).html(rendered(encounterCollection));
            EditableUtils.makeEditable(view);
            return view;
        },
        delete: function(eventName) {
            var encounter = this.collection.get(eventName.target.value),
                that = this;
            encounter.destroy({
                success: function() {
                    that.collection.remove(encounter);
                },
                error: ErrorUtils.log
            });
        },
        add: function (event) {
            this.modal = new EncounterModal({
                el:'#modal-container',
                collection:this.collection,
                model:new EncounterModel
            });
        },
    });

    return EncounterListView;
})