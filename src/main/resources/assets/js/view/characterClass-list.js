define([
	'mustache', 'jquery', 'backbone', 'view/characterClass-modal', 'text!tpl/classes.html', 'util/editable'
], function (Mustache, $, Backbone, CharacterClassModal, CharacterClassesTemplate, EditableUtils) {

    var CharacterClassListView = Backbone.View.extend({
        events:{
            'click button.delete':'deleteClass',
            'click button.create':'addClass', 
        },
        initialize: function () {
        	this.template = CharacterClassesTemplate;
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.render, this);
            this.collection.on('remove', this.render, this);
            this.collection.on('set', this.render, this);
        },
        render: function () {
        	var view = this, rendered;
        	var characterClassCollection = {
        		characterClasses: view.collection.toJSON()
        	}
        	
        	rendered = Mustache.to_html(view.template, characterClassCollection);
        	$(view.el).html(rendered);
            EditableUtils.makeEditable(view);
            return view;
        },
        deleteClass: function(eventName) {
            var characterClass = this.collection.get(eventName.target.value),
                that = this;
            characterClass.destroy({
                success: function() {
                    that.collection.remove(characterClass);
                },
            });
        },
        addClass: function (event) {
            this.modal = new CharacterClassModal({
                el:'#modal-container',
                collection:this.collection,
                model:new CharacterClassModel
            });
        },
    });

    return CharacterClassListView;
})