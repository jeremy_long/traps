define([
    'handlebars', 'jquery', 'backbone', 'text!tpl/damage-modal.html', 'bootstrap'
], function (Handlebars, $, Backbone, DamageModalHtml) {

    var DamageModal = Backbone.View.extend({
        events:{
            'click button.submitModal':'submit'
        },
        initialize: function () {            
            var view = this, rendered,
                model = view.options.combatant.toJSON();
   
            var combatantModel = {
                combatant: model,
            };
            
            rendered = Handlebars.compile(DamageModalHtml);

            Handlebars.registerHelper('addition', function(number1, number2) {
                return number1 + number2;
            })

            $(view.el).html(rendered(combatantModel));
            
            $('.modal').modal().css({
                'margin-top': function () {
                    return window.pageYOffset-($(this).height() / 2 );
                }
            });
            view.$('.modal').modal('show');
        },
        submit: function (eventName) {
            var damage = $.trim(this.$('#damage').val()),
                pierce = $.trim(this.$('#pierce').val()),
                that = this,
                combatant = that.options.combatant,
                damageReduction = combatant.get('actions')[1].damageReductionBonus + combatant.get('currentDamageReduction');

            var errors = new Object();

            if (damage.length < 1) {
                errors.damage = 'Please enter a number.';
            }
            if (pierce.length < 1) {
                errors.pierce = 'Please enter a number.';
            }
             
            if (this.hasProperties(errors)) {
                this.hideErrors();
                this.showErrors(errors);
                return;
            }

            damageReduction -= pierce
            if (damageReduction < 0) {
                damageReduction = 0;
            }
            damage -= damageReduction
            if (damage < 0) {
                damage = 0;
            }

            that.options.combatant.save({'currentHp':(combatant.get('currentHp') - damage)}, {
                success: function() {
                    that.model.fetch({
                        success: function(damage) {
                            that.$('.modal').modal('hide')
                            that.undelegateEvents()
                        },
                    });
                },
            });
        },
    });

    return DamageModal;
})