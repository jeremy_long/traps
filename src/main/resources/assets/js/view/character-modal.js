define([
    'mustache', 'jquery', 'backbone', 'text!tpl/character-modal.html', 'bootstrap'
], function (Mustache, $, Backbone, CharacterModalHtml) {

    var CharacterModal = Backbone.View.extend({
        events:{
            'click button.submitCharacterModal':'submit'
        },
        initialize: function () {
			var characterClassCollection = {
                characterClasses: this.options.characterClasses.toJSON()
            };
            
            rendered = Mustache.to_html(CharacterModalHtml, characterClassCollection);
            this.$el.html(rendered);

            $('.modal').modal().css(
            {
                'margin-top': function () {
                    return window.pageYOffset-($(this).height() / 2 );
                }
            });
            this.$('.modal').modal('show');
        },
        submit: function (eventName) {
        	var characterName = this.$('#characterName').val(),
                characterClassID = this.$('#characterClass').val(),
                level = this.$('#level').val(),
                hp = this.$('#hp').val(),
                ep = this.$('#ep').val(),
                strength = this.$('#strength').val(),
                dexterity = this.$('#dexterity').val(),
                agility = this.$('#agility').val(),
                health = this.$('#health').val(),
                cunning = this.$('#cunning').val(),
                willpower = this.$('#willpower').val(),
                intelligence = this.$('#intelligence').val(),
                that = this;

            if (characterName.length < 1) {
            	this.hideErrors();
            	var errors = new Object();
            	errors.characterName = 'Character name must not be empty.';
            	this.showErrors(errors);
            	return;
            }

            var newCharacterModel = new CharacterModel();
            newCharacterModel.set({'characterClass.characterClassID':characterClassID});
            newCharacterModel.save({characterName:_.escape(characterName), 'level':level, 'hp':hp, 'ep':ep, 'hpInitial':hp, 'epInitial':ep, 'strength':strength, 'dexterity':dexterity, 'agility':agility, 'health':health, 'cunning':cunning, 'willpower':willpower, 'intelligence':intelligence}, {
            	success: function(character) {
                    that.collection.push(character)
                    that.collection.fetch()
	                that.$('.modal').modal('hide')
                    that.undelegateEvents()
	    		},
	    		error: function(model, response) {
                }
            });
        },
    });

    return CharacterModal;
})