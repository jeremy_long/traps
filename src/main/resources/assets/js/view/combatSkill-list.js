define([
	'mustache', 'jquery', 'backbone', 'text!tpl/combatSkills.html', 'view/combatSkill-modal', 'util/editable'
], function (Mustache, $, Backbone,  CombatSkillsTemplate, CombatSkillModal, EditableUtils) {

    var CombatSkillListView = Backbone.View.extend({
        events:{
            'click button.delete':'delete',
            'click button.create':'add', 
        },
        initialize: function () {
        	this.template = CombatSkillsTemplate;
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.render, this);
            this.collection.on('remove', this.render, this);
            this.collection.on('set', this.render, this);
        },
        render: function () {
        	var view = this, rendered;
        	var combatSkillCollection = {
        		combatSkills: view.collection.toJSON()
        	}
        	
        	rendered = Mustache.to_html(view.template, combatSkillCollection);
        	$(view.el).html(rendered);
            EditableUtils.makeEditable(view);
            return view;
        },
        delete: function(eventName) {
            var element = this.collection.get(eventName.target.value),
                that = this;
            element.destroy({
                success: function() {
                    that.collection.remove(element);
                },
            });
        },
        add: function (event) {
            this.modal = new CombatSkillModal({
                el:'#modal-container',
                collection:this.collection,
                model:new CombatSkillModel
            });
        },
    });

    return CombatSkillListView;
})