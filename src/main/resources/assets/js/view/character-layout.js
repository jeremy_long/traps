define([
    'mustache', 'jquery', 'backbone', 'view/character-modal', 'text!tpl/characters.html', 'viewmaster', 'text!tpl/character-layout.html', 'view/character-menu', 'view/character'
], function (Mustache, $, Backbone, CharacterModal, CharactersTemplate, ViewMaster, CharacterLayoutTemplate, CharacterMenuView, CharacterView) {

    var CharacterLayoutView = Backbone.ViewMaster.extend({
    	constructor: function() {
    		Backbone.ViewMaster.prototype.constructor.apply(this, arguments);
    		
    		var characterMenuView = new CharacterMenuView({
    			    collection: this.collection,
    		})
    		this.setView('.character-list', characterMenuView);
    		if(this.model) {
    			this.showCharacter();
    			characterMenuView.highlight('#' + this.model.id);
    		} 
    		else {
    			this.addCharacter();
    		}
    	},
        template: function(context) {
        	return CharacterLayoutTemplate;
        },
        initialize: function () {
            this.collection.on('reset', this.refreshViews, this);
            this.collection.on('add', this.refreshViews, this);
            this.collection.on('remove', this.refreshViews, this);
            this.collection.on('set', this.refreshViews, this);
        },
        events:{
            'click button.create':'addCharacter'
        },
        addCharacter: function(event) {
            this.modal = new CharacterModal({
                el:'#modal-container',
                collection:this.collection,
                characterClasses:this.options.characterClasses,
                model:new CharacterModel
            });
           this.refreshViews();
        },
        showCharacter: function() {
        	this.setView('.character-content', new CharacterView({
                model:this.model,
                collection:this.collection,
                characterClasses:this.options.characterClasses,
                weapons:this.options.weapons,
                armor:this.options.armor
           }));
        }
    });

    return CharacterLayoutView;
})