define([
    'mustache', 'jquery', 'backbone', 'text!tpl/statusEffect-modal.html', 'bootstrap'
], function (Mustache, $, Backbone, StatusEffectModalHtml) {

    var StatusEffectModal = Backbone.View.extend({
        events:{
            'click button.submitModal':'submit'
        },
        initialize: function () {            
            this.$el.html(StatusEffectModalHtml);
            
            $('.modal').modal().css(
            {
                'margin-top': function () {
                    return window.pageYOffset-($(this).height() / 2 );
                }
            });
            this.$('.modal').modal('show');
        },
        submit: function (eventName) {
            var statusEffectName = $.trim(this.$('#statusEffectName').val()),
                statusEffectDescription = $.trim(this.$('#statusEffectDescription').val()),
                persistent = $.trim(this.$('#persistent').val()),
                that = this;

            var errors = new Object();

            if (statusEffectName.length < 1) {
                errors.statusEffectName = 'StatusEffect name must not be empty.';
            }
            
            if (this.hasProperties(errors)) {
                this.hideErrors();
                this.showErrors(errors);
                return;
            }

            var newStatusEffectModel = new StatusEffectModel();
            newStatusEffectModel.save({'statusEffectName':statusEffectName, 'statusEffectDescription':statusEffectDescription, 'persistent':persistent}, {
                success: function(statusEffect) {
                    that.collection.push(statusEffect);
                    that.collection.fetch();
                    that.$('.modal').modal('hide');
                    that.undelegateEvents();
                },
            });
        },
    });

    return StatusEffectModal;
})