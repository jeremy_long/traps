define([
	'handlebars', 'jquery', 'backbone', 'text!tpl/statusEffects.html', 'view/statusEffect-modal', 'util/validation', 'util/error', 'util/editable' 
], function (Handlebars, $, Backbone,  StatusEffectsTemplate, StatusEffectModal, ValidationUtils, ErrorUtils, EditableUtils) {

    var StatusEffectListView = Backbone.View.extend({
        events:{
            'click button.delete':'delete',
            'click button.create':'add',
            'click button.addAttributeEffect':'addAttributeEffect'
        },
        initialize: function () {
        	this.template = StatusEffectsTemplate;
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.render, this);
            this.collection.on('remove', this.render, this);
            this.collection.on('set', this.render, this);
        },
        render: function () {
        	var view = this, rendered;
        	var statusEffectCollection = {
        		statusEffects: view.collection.toJSON()
        	};
            rendered = Handlebars.compile(view.template);
            $(view.el).html(rendered(statusEffectCollection));
            EditableUtils.makeEditable(view);
            view.makeEditable();
            return view;
        },
        // TODO: Refactor to take advantage of editable utility.
        makeEditable: function() { 
            var view = this;
            $.ajax({
                //url: 'http://cdnjs.cloudflare.com/ajax/libs/x-editable/1.4.5/bootstrap-editable/js/bootstrap-editable.min.js',
                url: 'assets/js/libs/bootstrap-editable.min.js',
                dataType: 'script',
                cache: true,          
                success: function () {
                    $('.editableAttributeEffect').editable({
                        placement: 'bottom',
                        validate: ValidationUtils.requiredNumberFieldValidation,
                        success: function(response, newValue) {
                            var currentModel = view.collection.get($(this).attr('value'));
                            var map = currentModel.get('attributeEffects');
                            map[$(this).attr('name')] = $.trim(newValue);
                            currentModel.save({'attributeEffects':map}, {
                                error: ErrorUtils.fetch(view.collection),
                            });
                        },
                        error: ErrorUtils.log
                    });
                    $('.editableAffectedAttribute').editable({
                        placement: 'bottom',
                        success: function(response, newValue) {
                            var currentModel = view.collection.get($(this).attr('value'));
                            var map = currentModel.get('attributeEffects');
                            if ($.trim(newValue).length > 0) {
                                map[$.trim(newValue)] = map[$(this).attr('name')];
                            }
                            delete map[$(this).attr('name')];
                            currentModel.save({'attributeEffects':map}, {
                                success: function() {
                                    view.collection.fetch();
                                },
                                error: ErrorUtils.fetch(view.collection),
                            });
                        },
                        error: ErrorUtils.log
                    });
                },
                error: ErrorUtils.log
            });
        },
        delete: function(eventName) {
            var element = this.collection.get(eventName.target.value),
                that = this;
            element.destroy({
                success: function() {
                    that.collection.remove(element);
                },
            });
        },
        add: function (event) {
            this.modal = new StatusEffectModal({
                el:'#modal-container',
                collection:this.collection,
                model:new StatusEffectModel
            });
        },
        addAttributeEffect: function (event) {
            var model = this.collection.get(event.target.value),
                that = this;

            var map = model.get('attributeEffects');
            map['Undefined'] = 0;

            model.save({'attributeEffects':map}, {
                success: function() {
                    that.collection.fetch();
                },
                error: ErrorUtils.fetch(that.collection)
            });
        },
    });

    return StatusEffectListView;
})