define([
    'mustache', 'jquery', 'backbone', 'text!tpl/armor-modal.html', 'bootstrap'
], function (Mustache, $, Backbone, ArmorModalHtml) {

    var ArmorModal = Backbone.View.extend({
        events:{
            'click button.submitModal':'submit'
        },
        initialize: function () {            
        	//create and show the modal
            this.$el.html(ArmorModalHtml);
            
            $('.modal').modal().css(
            {
                'margin-top': function () {
                    return window.pageYOffset-($(this).height() / 2 );
                }
            });
            this.$('.modal').modal('show');
        },
        submit: function (eventName) {
            var armorName = $.trim(this.$('#armorName').val()),
                damageReduction = this.$('#damageReduction').val(),
                evade = this.$('#evade').val(),
                weight = this.$('#weight').val(),
                type = this.$('#type').val(),
                hands = this.$('#hands').val(),
                cost = this.$('#cost').val(),
                that = this;

            var errors = new Object();

            if (armorName.length < 1) {
                errors.armorName = 'Armor name must not be empty.';
            }
            var errorMessage = 'Please enter a positive integer.';
            if (damageReduction.length < 1) { errors.damageReduction = errorMessage; }
            if (evade.length < 1) { errors.evade = errorMessage; }
            if (weight.length < 1) { errors.weight = errorMessage; }
            if (hands.length < 1) { errors.hands = errorMessage; }
            if (cost.length < 1) { errors.cost = errorMessage; }
            
            if (this.hasProperties(errors)) {
                this.hideErrors();
                this.showErrors(errors);
                return;
            }

            var newArmorModel = new ArmorModel();
            newArmorModel.save({'armorName':_.escape(armorName), 'damageReduction':damageReduction, 'evade':evade, 'weight':weight, 'type':type, 'hands':hands, 'cost':cost}, {
                success: function(armor) {
                    that.collection.push(armor)
                    that.collection.fetch()
                    that.$('.modal').modal('hide')
                    that.undelegateEvents()
                },
            });
        },
    });

    return ArmorModal;
})