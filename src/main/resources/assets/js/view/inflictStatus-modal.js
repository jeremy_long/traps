define([
    'handlebars', 'jquery', 'backbone', 'text!tpl/inflictStatus-modal.html', 'bootstrap'
], function (Handlebars, $, Backbone, InflictStatusModalHtml) {

    var InflictStatusModal = Backbone.View.extend({
        events:{
            'click button.submitModal':'submit'
        },
        initialize: function () {            
            var view = this, rendered,
                model = view.options.combatant.toJSON();
   
            var combatantModel = {
                combatant: model,
                statusEffects: view.options.statusEffects.toJSON()
            };
            
            rendered = Handlebars.compile(InflictStatusModalHtml);
            $(view.el).html(rendered(combatantModel));
            
            $('.modal').modal().css({
                'margin-top': function () {
                    return window.pageYOffset-($(this).height() / 2 );
                }
            });
            view.$('.modal').modal('show');
        },
        submit: function (eventName) {
            var statusEffectID = $.trim(this.$('#statusEffectID').val()),
                checkModifier = $.trim(this.$('#checkModifier').val()),
                that = this,
                combatant = that.options.combatant,
                combatantStatusEffects = combatant.get("statusEffects"),
                statusEffect = that.options.statusEffects.get(statusEffectID);

            var errors = new Object();

            if (checkModifier.length < 1) { errors.checkModifier = 'Please enter a number.'; }
            
            if (this.hasProperties(errors)) {
                this.hideErrors();
                this.showErrors(errors);
                return;
            }

            statusEffect.set({'modifier':checkModifier});
            combatantStatusEffects.push(statusEffect);

            that.options.combatant.save({'statusEffects':combatantStatusEffects}, {
                success: function() {
                    that.model.fetch({
                        success: function(energy) {
                            that.$('.modal').modal('hide');
                            that.undelegateEvents();
                        },
                    });
                },
            });
        },
    });

    return InflictStatusModal;
})