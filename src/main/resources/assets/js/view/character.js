define([
    'handlebars', 'jquery', 'backbone', 'text!tpl/character.html', 'view/spendPoints-modal', 'util/editable'
], function (Handlebars, $, Backbone, CharacterTemplate, SpendPointsModal, EditableUtils) {

    var CharacterView = Backbone.View.extend({
    	events:{
            'click button.add':'addNote',
            'click button.delete':'deleteNote',
            'click button.save':'saveNote',
            'click button.spendPoints':'spendPoints',
            'click button.download':'downloadCharacter',
            'click .characterNotes':'editCharacterNote',
        },
        initialize: function () {
            this.template = CharacterTemplate;
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.render, this);
            this.collection.on('remove', this.render, this);
            this.collection.on('set', this.render, this);
        },
        render: function () {
        	var view = this, rendered,
                model = view.model.toJSON(),
                armor = view.options.armor,
                armorHead = new ArmorList(),
                armorHands = new ArmorList(),
                armorTorso = new ArmorList(),
                armorLegs = new ArmorList(),
                armorShield = new ArmorList();

            for (var i = 0; i < armor.length; i++) {
                if (armor.at(i).get('type') === 'HEAD') {
                    armorHead.add(armor.at(i));
                } else if (armor.at(i).get('type') === 'HANDS') {
                    armorHands.add(armor.at(i));
                } else if (armor.at(i).get('type') === 'TORSO') {
                    armorTorso.add(armor.at(i));
                } else if (armor.at(i).get('type') === 'LEGS') {
                    armorLegs.add(armor.at(i));
                } else if (armor.at(i).get('type') === 'SHIELD') {
                    armorShield.add(armor.at(i));
                }
            }

        	var characterModel = {
        		character: model,
                characterClasses: view.options.characterClasses.toJSON(),
                weapons: view.options.weapons.toJSON(),
                armorHead: armorHead.toJSON(),
                armorHands: armorHands.toJSON(),
                armorTorso: armorTorso.toJSON(),
                armorLegs: armorLegs.toJSON(),
                armorShield: armorShield.toJSON()
        	};
            
            rendered = Handlebars.compile(view.template);
            $(view.el).html(rendered(characterModel));

            view.makeEditable();

        	return view;
        },
        addNote: function(eventName) {
            var text = this.$('#characterNote').val(),
                newCharacterNoteModel = new CharacterNoteModel(),
                that = this;
            
            newCharacterNoteModel.save({'text':_.escape(text), 'characterID':that.model.get('characterID')}, {
                success: function(character) {
                    that.model.fetch()
                    that.collection.fetch()
                    that.render()
                },
                error: function(model, response) {
                }
            });
        },
        deleteNote: function(eventName) {
            var that = this,
                characterNoteModel = new CharacterNoteModel({'characterNoteID':eventName.target.value});

            characterNoteModel.destroy({
                success: function(character) {
                    that.model.fetch()
                    that.collection.fetch()
                    that.render()
                },
                error: function(model, response) {
                }
            });
        },
        saveNote: function(eventName) {
            var that = this,
                characterNoteModel = new CharacterNoteModel(),
                text = this.$('#characterNote'+eventName.target.name).val();

            characterNoteModel.save({'characterNoteID':eventName.target.name, 'text':_.escape(text), 'characterID':that.model.get('characterID')}, {
                success: function(characterNote) {
                    that.model.fetch()
                    that.collection.fetch()
                    that.render()
                },
            });
        },
        // Changes a note into an editable textarea, saving the contents of any previous textareas in the notes section.
        // TODO: Refactor code to take advantage of bootstrap-editable.
        editCharacterNote: function(eventName) {
            var div = this.$('.characterNotes');
            var target = div.find('#' + eventName.target.id);
            if (target.is('span')) {
                var textarea = div.find('textarea');
                if (textarea.is('textarea')) {
                     var that = this,
                        characterNoteModel = new CharacterNoteModel(),
                        text = textarea.val();

                        characterNoteModel.save({'characterNoteID':textarea.attr('name'), 'text':_.escape(text), 'characterID':that.model.get('characterID')}, {async: false});
                        that.model.fetch({async: false}, {});
                        that.render();
                        var target = that.$('.characterNotes').find('#'+eventName.target.id);
                        target.html('<textarea id="characterNote'+eventName.target.id+'" name="'+eventName.target.id+'">'+target.text()+'</textarea>');                                 
                        div.find('#save' + eventName.target.id).addAttr('disabled');
                } else {
                    target.html('<textarea id="characterNote'+eventName.target.id+'" name="'+eventName.target.id+'">'+target.text()+'</textarea>');
                    div.find('#save' + eventName.target.id).removeAttr('disabled');
                }
            }
        },
        downloadCharacter: function(eventName) {
            var that = this,
                characterSheet = new CharacterSheetModel({'characterID':this.model.get('characterID')});

            characterSheet.fetch({
                success: function(result) {
                    window.location = 'assets/txt/' + that.model.get('characterName').replace(' ', '') + '.txt'
                },
                error: function(result, error) {
                    alert('An error occurred while generating the character sheet: ' + error)
                },
            });
        },
        // TODO: Convert to using the edtiable utility file
        makeEditable: function() { 
            var view = this
            $.ajax({
                //url: 'http://cdnjs.cloudflare.com/ajax/libs/x-editable/1.4.5/bootstrap-editable/js/bootstrap-editable.min.js',
                url: 'assets/js/libs/bootstrap-editable.min.js',
                dataType: 'script',
                cache: true,          
                success: function () {
                    $('.editableCharacterName').editable({
                        placement: 'bottom',
                        validate: function(value) {
                            if($.trim(value) == '') {
                                return 'This field is required.';
                            }
                        },
                        success: function(response, newValue) {
                            var characterModel = view.model;
                            characterModel.save({characterName:$.trim(_.escape(newValue))}, {
                                success: function() {
                                    view.collection.fetch();
                                },
                                error: function() {
                                    view.model.fetch();
                                    view.render();
                                },
                            });
                        },
                        error: function() {
                            alert('error');
                        }
                    });
                    $('.editableCharacterClass').editable({
                        placement: 'bottom',
                        validate: function(value) {
                            if($.trim(value) == '') {
                                return 'This field is required.';
                            }
                            var numberValue = Number(value);
                            if (Math.floor(numberValue) != value) {
                                return 'Please enter a number.';
                            }
                        },
                        success: function(response, newValue) {
                            var characterModel = view.model;
                            characterModel.set({'characterClass.characterClassID':newValue});
                            characterModel.save({'characterName':characterModel.attributes.characterName}, {
                                success: function() {
                                    characterModel.fetch({
                                        success: function() {
                                            view.render();
                                        },
                                    });
                                },
                                error: function() {
                                    alert('Error');
                                    view.collection.fetch();
                                },
                            });
                        },
                        error: function() {
                            alert('error');
                        }
                    });
                    var validate = function(value) {
                        if($.trim(value) == '') {
                            return 'This field is required.';
                        }
                        var numberValue = Number(value);
                        if (Math.floor(numberValue) != value || numberValue < 1) {
                            return 'Please enter a positive number.';
                        }
                    }
                    $('.editableLevel').editable({
                        placement: 'bottom',
                        validate: validate,
                        success: function(response, newValue) {
                            var characterModel = view.model;
                            characterModel.save({'level':newValue}, {
                                success: function() {
                                    characterModel.fetch({
                                        success: function() {
                                            view.render();
                                        },
                                    });
                                },
                                error: function() {
                                    alert('Error');
                                    view.collection.fetch();
                                },
                            });
                        },
                        error: function() {
                            alert('error');
                        }
                    });
                    $('.editableWeapon').editable({
                        placement: 'bottom',
                        success: function(response, newValue) {
                            var characterModel = view.model,
                                characterWeapons = characterModel.get("weapons"),
                                weaponIndex = $(this).attr('name');
                            if (newValue > 0) {
                                characterWeapons[weaponIndex] = view.options.weapons.get(newValue);
                            } else {
                                characterWeapons.splice(weaponIndex, 1);
                            }
                            characterModel.save({'weapons':characterWeapons}, {
                                success: function() {
                                    characterModel.fetch({
                                        success: function() {
                                            view.render();
                                        },
                                    })
                                },
                                error: function() {
                                    characterModel.fetch({
                                        success: function() {
                                            view.render();
                                        },
                                    });
                                },
                            });
                        },
                        error: function() {
                            alert('error');
                        }
                    });
                    $('.editableArmor').editable({
                        placement: 'bottom',
                        success: function(response, newValue) {
                            var characterModel = view.model,
                                characterArmor = characterModel.get("armor"),
                                armorType = $(this).attr('name');
                            if (newValue > 0) {
                                characterArmor[armorType] = view.options.armor.get(newValue);
                            } else {
                                delete characterArmor[armorType];
                            }
                            characterModel.save({'armor':characterArmor}, {
                                success: function() {
                                    characterModel.fetch({
                                        success: function() {
                                            view.render();
                                        },
                                    })
                                },
                                error: function() {
                                    characterModel.fetch({
                                        success: function() {
                                            view.render();
                                        },
                                    });
                                },
                            });
                        },
                        error: function() {
                            alert('error');
                        }
                    });

                },
                error: function () {
                    console.log('Cannot load script');
                    return;
                },
            });
        },
        spendPoints: function (event) {
            this.modal = new SpendPointsModal({
                el:'#modal-container',
                collection:this.collection,
                model:this.model
            });
        }, 
    });

    return CharacterView;
})