define([
    'mustache', 'jquery', 'backbone', 'text!tpl/encounter-modal.html', 'bootstrap'
], function (Mustache, $, Backbone, EncounterModalHtml) {

    var EncounterModal = Backbone.View.extend({
        events:{
            'click button.submitModal':'submit'
        },
        initialize: function () {            
            this.$el.html(EncounterModalHtml);
            
            $('.modal').modal().css(
            {
                'margin-top': function () {
                    return window.pageYOffset-($(this).height() / 2 );
                }
            });
            this.$('.modal').modal('show');
        },
        submit: function (eventName) {
            var encounterName = $.trim(this.$('#encounterName').val()),
                that = this;

            var errors = new Object();

            if (encounterName.length < 1) {
                errors.encounterName = 'Encounter name must not be empty.';
            }
             
            if (this.hasProperties(errors)) {
                this.hideErrors();
                this.showErrors(errors);
                return;
            }

            var newEncounterModel = new EncounterModel();
            newEncounterModel.save({'encounterName':_.escape(encounterName)}, {
                success: function(encounter) {
                    that.collection.push(encounter);
                    that.collection.fetch();
                    that.$('.modal').modal('hide');
                    that.undelegateEvents();
                },
            });
        },
    });

    return EncounterModal;
})