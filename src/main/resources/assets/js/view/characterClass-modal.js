define([
    'mustache', 'jquery', 'backbone', 'text!tpl/characterClass-modal.html', 'bootstrap'
], function (Mustache, $, Backbone, CharacterClassModalHtml) {

    var CharacterClassModal = Backbone.View.extend({
        events:{
            'click button.submitClassModal':'submit'
        },
        initialize: function () {            
        	//create and show the modal
            this.$el.html(CharacterClassModalHtml);
            
            $('.modal').modal().css(
            {
                'margin-top': function () {
                    return window.pageYOffset-($(this).height() / 2 );
                }
            });
            this.$('.modal').modal('show');
        },
        submit: function (eventName) {
        	var characterClassName = this.$('#characterClassName').val(),
                hpInitialBonus = this.$('#hpInitialBonus').val().valueOf(),
                epInitialBonus = this.$('#epInitialBonus').val().valueOf(),
                skillInitialBonus = this.$('#skillInitialBonus').val().valueOf(),
                passiveInitialBonus = this.$('#passiveInitialBonus').val().valueOf(),
                hpLevelBonus = this.$('#hpLevelBonus').val().valueOf(),
                epLevelBonus = this.$('#epLevelBonus').val().valueOf(),
                strengthInitialBonus = this.$('#strengthInitialBonus').val().valueOf(),
                dexterityInitialBonus = this.$('#dexterityInitialBonus').val().valueOf(),
                agilityInitialBonus = this.$('#agilityInitialBonus').val().valueOf(),
                healthInitialBonus = this.$('#healthInitialBonus').val().valueOf(),
                cunningInitialBonus = this.$('#cunningInitialBonus').val().valueOf(),
                willpowerInitialBonus = this.$('#willpowerInitialBonus').val().valueOf(),
                intelligenceInitialBonus = this.$('#intelligenceInitialBonus').val().valueOf(),
                attributeInitialBonus = this.$('#attributeInitialBonus').val().valueOf(),
                that = this;

            if (characterClassName.length < 1) {
            	this.hideErrors();
            	var errors = new Object();
            	errors.characterClassName = 'Character class name must not be empty.';
            	this.showErrors(errors);
            	return;
            }

            var newCharacterClassModel = new CharacterClassModel();
            newCharacterClassModel.save({characterClassName:_.escape(characterClassName),'hpInitialBonus':hpInitialBonus, 'epInitialBonus':epInitialBonus, 'skillInitialBonus': skillInitialBonus, 'passiveInitialBonus':passiveInitialBonus, 'hpLevelBonus':hpLevelBonus, 'epLevelBonus':epLevelBonus, 'strengthInitialBonus':strengthInitialBonus, 'dexterityInitialBonus':dexterityInitialBonus, 'agilityInitialBonus':agilityInitialBonus, 'healthInitialBonus':healthInitialBonus, 'cunningInitialBonus':cunningInitialBonus, 'willpowerInitialBonus':willpowerInitialBonus, 'intelligenceInitialBonus':intelligenceInitialBonus, 'attributeInitialBonus':attributeInitialBonus}, {
            	success: function(characterClass) {
            		that.collection.push(characterClass)
	            	that.$('.modal').modal('hide')
                    that.undelegateEvents()
	    		},
            });        
        },
    });

    return CharacterClassModal;
})