define([
    'mustache', 'jquery', 'backbone', 'text!tpl/combatSkill-modal.html', 'bootstrap'
], function (Mustache, $, Backbone, CombatSkillModalHtml) {

    var CombatSkillModal = Backbone.View.extend({
        events:{
            'click button.submitClassModal':'submit'
        },
        initialize: function () {            
        	//create and show the modal
            this.$el.html(CombatSkillModalHtml);
            
            $('.modal').modal().css(
            {
                'margin-top': function () {
                    return window.pageYOffset-($(this).height() / 2 );
                }
            });
            this.$('.modal').modal('show');
        },
        submit: function (eventName) {
        	var combatSkillName = $.trim(this.$('#combatSkillName').val()),
                powerRank = this.$('#powerRank').val(),
                accuracyRank = this.$('#accuracyRank').val(),
                finesseRank = this.$('#finesseRank').val(),
                that = this;

            if (combatSkillName.length < 1) {
            	this.hideErrors();
            	var errors = new Object();
            	errors.combatSkillName = 'Combat skill name must not be empty.';
            	this.showErrors(errors);
            	return;
            }

            var newCombatSkillModel = new CombatSkillModel();
            newCombatSkillModel.save({'combatSkillName':_.escape(combatSkillName),'powerRank':powerRank, 'accuracyRank':accuracyRank, 'finesseRank': finesseRank}, {
            	success: function(combatSkill) {
            		that.collection.push(combatSkill);
	            	that.$('.modal').modal('hide');
                    that.undelegateEvents();
	    		},
	    		error: function(model, response) {
                  
	            }
            });        
        },
    });

    return CombatSkillModal;
})