define([
	'handlebars', 'jquery', 'backbone', 'text!tpl/weapons.html', 'view/weapon-modal', 'util/editable'
], function (Handlebars, $, Backbone,  WeaponsTemplate, WeaponModal, EditableUtils) {

    var WeaponListView = Backbone.View.extend({
        events:{
            'click button.delete':'delete',
            'click button.create':'add', 
        },
        initialize: function () {
        	this.template = WeaponsTemplate;
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.render, this);
            this.collection.on('remove', this.render, this);
            this.collection.on('set', this.render, this);
        },
        render: function () {
        	var view = this, rendered;
        	var weaponCollection = {
        		weapons: view.collection.toJSON(),
                combatSkills: view.options.combatSkills.toJSON()
        	}

            rendered = Handlebars.compile(view.template);
            $(view.el).html(rendered(weaponCollection));
            EditableUtils.makeEditable(view);
            return view;
        },
        delete: function(eventName) {
            var element = this.collection.get(eventName.target.value),
                that = this;
            element.destroy({
                success: function() {
                    that.collection.remove(element);
                },
            });
        },
        add: function (event) {
            this.modal = new WeaponModal({
                el:'#modal-container',
                collection:this.collection,
                combatSkills:this.options.combatSkills,
                model:new WeaponModel
            });
        },
    });

    return WeaponListView;
})