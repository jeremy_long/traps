define([
	'handlebars', 'jquery', 'backbone', 'text!tpl/armor.html', 'view/armor-modal', 'util/editable'
], function (Handlebars, $, Backbone,  ArmorTemplate, ArmorModal, EditableUtils) {

    var ArmorListView = Backbone.View.extend({
        events:{
            'click button.delete':'delete',
            'click button.create':'add', 
        },
        initialize: function () {
        	this.template = ArmorTemplate;
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.render, this);
            this.collection.on('remove', this.render, this);
            this.collection.on('set', this.render, this);
        },
        render: function () {
        	var view = this, rendered;
        	var armorCollection = {
        		armor: view.collection.toJSON(),
        	};
            rendered = Handlebars.compile(view.template);
            $(view.el).html(rendered(armorCollection))   ;         
            EditableUtils.makeEditable(this);
            return view;
        },
        delete: function(eventName) {
            var element = this.collection.get(eventName.target.value),
                that = this;
            element.destroy({
                success: function() {
                    that.collection.remove(element)
                },
            });
        },
        add: function (event) {
            this.modal = new ArmorModal({
                el:'#modal-container',
                collection:this.collection,
                model:new ArmorModel
            });
        },
    });
    return ArmorListView;
})