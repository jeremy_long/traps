define([
    'mustache', 'jquery', 'backbone', 'view/character-modal', 'text!tpl/character-menu.html'
], function (Mustache, $, Backbone, CharacterModal, CharacterMenuTemplate) {

    var CharacterMenuView = Backbone.View.extend({
        initialize: function () {
        	this.template = CharacterMenuTemplate;
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.render, this);
        },
        render: function () {
        	var view = this, rendered;
        	var characterCollection = {
        		characters: view.collection.toJSON()
        	};
        	
        	rendered = Mustache.to_html(view.template, characterCollection);
        	$(view.el).html(rendered);
        	return view;
        },
        highlight: function(selected) {
        	$('.nav li', '.character-list').removeClass('active');
        	$(selected).addClass('active');
        }
    });

    return CharacterMenuView;
})