define([
    'underscore', 'jquery', 'backbone', 'jquery.cookie',
    'model/characterClass', 'view/characterClass-list',
    'model/character', 'view/character-list', 'view/character-layout',
    'model/characterNote', 'model/characterSheet',
    'model/combatSkill', 'view/combatSkill-list',
    'model/weapon', 'view/weapon-list',
    'model/armor', 'view/armor-list',
    'model/technique', 'view/technique-list',
    'model/encounter', 'view/encounter-list', 'view/encounter-layout', 'view/encounter',
    'model/combatant',
    'model/action',
    'model/statusEffect', 'view/statusEffect-list',
    'model/csv'
], function (_, $, Backbone, Cookie, CharacterClass, CharacterClassListView, 
    Character, CharacterListView, CharacterLayoutView, CharacterNote, CharacterSheet, 
    CombatSkill, CombatSkillListView, Weapon, WeaponListView, Armor, ArmorListView,
    Technique, TechniqueListView, Encounter, EncounterListView, EncounterLayoutView, EncounterView,
    Combatant, Action, StatusEffect, StatusEffectListView, CSV) {
    
    // add helper methods for all views
    _.extend(Backbone.View.prototype, {
        close: function () {
            this.remove();
             _.each(this.bindings, function(binding) {
                binding.model.unbind(binding.ev, binding.callback);
            })
            this.unbind();
        },
        showErrors: function(errors) {
            _.each(errors, function (error, key) {
                var field = this.$('.' + key);
                field.addClass('error');
                field.find('.help-inline').text(error.replace(/\(.+\)/, ''));
            }, this);
        },
        hideErrors: function () {
            this.$('.control-group').removeClass('error');
            this.$('.help-inline').text('');
        },
        hideThisError: function(event) {
            var target = $(event.target).parent('div');
            target.removeClass('error');
            target.find('.help-inline').text('');
        },
        hasProperties: function(object) {
            for(var prop in object) {
                if (object.hasOwnProperty(prop)) {
                    return true;
                }
            }
            return false;
        }
    });

    var AppRouter = Backbone.Router.extend({
        routes:{
            'characters/:id':'viewCharacter',
            'characters':   'viewCharacters',
            'classes':      'viewClasses',
            'combatSkills': 'viewCombatSkills',
            'weapons':      'viewWeapons',
            'weapons/:id':  'viewWeapons',
            'armor':        'viewArmor',
            'techniques':   'viewTechniques',
            'statusEffects':'viewStatusEffects', 
            'statusEffects/:id':'viewStatusEffects', 
            'encounters':   'viewEncounters',
            'encounters/:id':'viewEncounter',    
            '*path':		'defaultRoute'
        },
        showView: function(menuItem, view) {
        	$('.nav li').removeClass('active');
        	if (menuItem) {
        		$(menuItem).addClass('active');
        	}
            // close the previous view - does binding clean-up and avoids memory leaks
            if (this.currentView) this.currentView.close();
            $('#container').html(view.render().el);
            this.currentView = view;
            return view;
        },
        defaultRoute: function() {
            this.viewCharacters();
        },
        viewClasses: function() {
            var that = this,
                classes = new CharacterClassList;
            
            classes.fetch({success:function () {
                that.showView('#nav-classes', new CharacterClassListView({
                    collection:classes
                }));
            }});
        },
        viewCombatSkills: function() {
            var that = this,
                combatSkills = new CombatSkillList;
            
            combatSkills.fetch({success:function () {
                that.showView('#nav-combat-skills', new CombatSkillListView({
                    collection:combatSkills
                }));
            }});
        },
        viewStatusEffects: function() {
            var that = this,
                statusEffects = new StatusEffectList;
            
            statusEffects.fetch({success:function () {
                that.showView('#nav-status-effects', new StatusEffectListView({
                    collection:statusEffects
                }));
            }});
        },
        viewArmor: function() {
            var that = this,
                armor = new ArmorList;

            armor.fetch({success:function () {
                that.showView('#nav-armor', new ArmorListView({
                    collection:armor
                }));
            }});
        },
        viewTechniques: function() {
            var that = this,
                techniques = new TechniqueList;

            techniques.fetch({success:function () {
                that.showView('#nav-techniques', new TechniqueListView({
                    collection:techniques
                }));
            }});
        },
        viewEncounter: function(id) {
            var that = this,
                encounters = new EncounterList,
                characters = new CharacterList,
                techniques = new TechniqueList,
                statusEffects = new StatusEffectList;

            statusEffects.fetch({success: function() { 
                techniques.fetch({success: function() {
                    characters.fetch({success: function() {
                        encounters.fetch({success:function() {
                            that.showView('#nav-encounters', new EncounterView({
                                collection: encounters,
                                model: encounters.get(id),
                                characters: characters,
                                techniques: techniques,
                                statusEffects: statusEffects
                            }));
                        }});
                    }});
                }});
            }});
        },
        viewEncounters: function() {
            var that = this,
                encounters = new EncounterList;

            encounters.fetch({success:function () {
                that.showView('#nav-encounters', new EncounterListView({
                    collection:encounters
                }));
            }});
        },
        viewWeapons: function() {
            var that = this,
                weapons = new WeaponList,
                combatSkills = new CombatSkillList;

            combatSkills.fetch({success:function() {
                weapons.fetch({success:function() {
                    that.showView('#nav-weapons', new WeaponListView({
                        collection:weapons,
                        combatSkills:combatSkills
                    }));
                }});
            }});
        },
        viewCharacters: function() {
            var that = this,
                characters = new CharacterList,
                classes = new CharacterClassList;

            classes.fetch({success:function() {
                characters.fetch({success:function() {
                    that.showView('#nav-characters', new CharacterListView({
                        collection:characters,
                        characterClasses:classes,
                    }));
                }});
            }});
        },
        viewCharacter: function(id) {
            var that = this,
                characters = new CharacterList,
                characterClasses = new CharacterClassList,
                weapons = new WeaponList,
                armor = new ArmorList;

            characterClasses.fetch({success:function() {            
                weapons.fetch({success:function() {
                    armor.fetch({success:function() {
                        characters.fetch({success:function() {
                            that.showView('#nav-characters', new CharacterLayoutView({
                                collection: characters,
                                model: characters.get(id),
                                weapons: weapons,
                                armor: armor,
                                characterClasses: characterClasses,
                            }));
                        }});
                    }});
                }});
            }});
        },
    })

    return AppRouter;
})
