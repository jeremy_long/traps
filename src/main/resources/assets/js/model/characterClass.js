define([
    'backbone'
], function (Backbone) {

	var CharacterClass = {};
	
    CharacterClassModel = Backbone.Model.extend({
    	idAttribute:'characterClassID',
        urlRoot: 'classes'
    });

    CharacterClassList = Backbone.Collection.extend({
        model:CharacterClassModel,
    	url: 'classes'
    });
    
    return CharacterClass;
})