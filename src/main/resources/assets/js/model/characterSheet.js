define([
    'backbone',
], function (Backbone) {

	var CharacterSheet = {};
	
    CharacterSheetModel = Backbone.Model.extend({
    	idAttribute:'characterID',
        urlRoot: 'characters/sheets',
    });
    
    return CharacterSheet;
})