define([
    'backbone'
], function (Backbone) {

	var CharacterNote = {};
	
    CharacterNoteModel = Backbone.Model.extend({
    	idAttribute:'characterNoteID',
        urlRoot: 'notes'
    });

    CharacterNoteList = Backbone.Collection.extend({
        model:CharacterNoteModel,
    	url: 'notes'
    });
    
    return CharacterNote;
})