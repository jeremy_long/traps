define([
    'backbone', 'deepmodel'
], function (Backbone) {

	var Weapon = {};
	
    WeaponModel = Backbone.DeepModel.extend({
    	idAttribute:'weaponID',
        urlRoot: 'weapons',
    });

    WeaponList = Backbone.Collection.extend({
        model:WeaponModel,
    	url: 'weapons'
    });
    
    return Weapon;
})