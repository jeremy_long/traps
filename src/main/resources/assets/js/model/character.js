define([
    'backbone', 'deepmodel'
], function (Backbone) {

	var Character = {};
	
    CharacterModel = Backbone.DeepModel.extend({
    	idAttribute:'characterID',
        urlRoot: 'characters',
    });

    CharacterList = Backbone.Collection.extend({
        model:CharacterModel,
    	url: 'characters'
    });
    
    return Character;
})