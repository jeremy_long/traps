define([
    'backbone', 'deepmodel'
], function (Backbone) {

	var Combatant = {};
	
    CombatantModel = Backbone.DeepModel.extend({
    	idAttribute:'combatantID',
        urlRoot: 'combatants'
    });

    CombatantList = Backbone.Collection.extend({
        model:CombatantModel,
    	url: 'combatants'
    });
    
    return Combatant;
})