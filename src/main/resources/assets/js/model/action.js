define([
    'backbone', 'deepmodel'
], function (Backbone) {

	var Action = {};
	
    ActionModel = Backbone.DeepModel.extend({
    	idAttribute:'actionID',
        urlRoot: 'actions'
    });

    ActionList = Backbone.Collection.extend({
        model:ActionModel,
    	url: 'actions'
    });
    
    return Action;
})