define([
    'backbone',
], function (Backbone) {

	var CSV = {};
	
    CSVModel = Backbone.Model.extend({
    	idAttribute:'csvID',
        urlRoot: 'techniques/csv',
    });
    
    return CSV;
})