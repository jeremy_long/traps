define([
    'backbone', 'deepmodel'
], function (Backbone) {

	var Encounter = {};
	
    EncounterModel = Backbone.DeepModel.extend({
    	idAttribute:'encounterID',
        urlRoot: 'encounters'
    });

    EncounterList = Backbone.Collection.extend({
        model:EncounterModel,
    	url: 'encounters'
    });
    
    return Encounter;
})