define([
    'backbone'
], function (Backbone) {

	var CombatSkill = {};
	
    CombatSkillModel = Backbone.Model.extend({
    	idAttribute:'combatSkillID',
        urlRoot: 'combatSkills'
    });

    CombatSkillList = Backbone.Collection.extend({
        model:CombatSkillModel,
    	url: 'combatSkills'
    });
    
    return CombatSkill;
})