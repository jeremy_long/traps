define([
    'backbone'
], function (Backbone) {

	var Technique = {};
	
    TechniqueModel = Backbone.Model.extend({
    	idAttribute:'techniqueID',
        urlRoot: 'techniques'
    });

    TechniqueList = Backbone.Collection.extend({
        model:TechniqueModel,
    	url: 'techniques'
    });
    
    return Technique;
})