define([
    'backbone'
], function (Backbone) {

	var Armor = {};
	
    ArmorModel = Backbone.Model.extend({
    	idAttribute:'armorID',
        urlRoot: 'armor'
    });

    ArmorList = Backbone.Collection.extend({
        model:ArmorModel,
    	url: 'armor'
    });
    
    return Armor;
})