define([
    'backbone', 'deepmodel'
], function (Backbone) {

	var StatusEffect = {};
	
    StatusEffectModel = Backbone.DeepModel.extend({
    	idAttribute:'statusEffectID',
        urlRoot: 'statusEffects'
    });

    StatusEffectList = Backbone.Collection.extend({
        model:StatusEffectModel,
    	url: 'statusEffects'
    });
    
    return StatusEffect;
})