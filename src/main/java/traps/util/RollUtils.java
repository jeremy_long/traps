package traps.util;

import java.util.Random;

public class RollUtils {

	private static final Random RANDOM = new Random();

	private static final int MAX_INITIATIVE = 4;

	private RollUtils() {
	}

	public static int rollInitiative() {
		return RANDOM.nextInt(MAX_INITIATIVE) + 1;
	}
}
