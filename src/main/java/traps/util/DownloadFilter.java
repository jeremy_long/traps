package traps.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DownloadFilter implements Filter {

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpReq = (HttpServletRequest) req;
		String url = httpReq.getRequestURL().toString();
		String filename = url.substring(url.lastIndexOf("/") + 1, url.length());

		HttpServletResponse httpResp = (HttpServletResponse) resp;
		httpResp.setContentType("text/plain");
		httpResp.addHeader("Content-Disposition", "attachment; filename=\""
				+ filename + "\"");

		chain.doFilter(req, resp);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}
}
