package traps.api;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Data object that represents a note attached to a specific character.
 * 
 * @author jeremy.long
 * 
 */
@JsonPropertyOrder({ "characterNoteID", "characterID", "text", "updated" })
public class CharacterNote {

	private final int characterNoteID;

	private final int characterID;

	private String text;
	private DateTime updated;

	@JsonCreator
	public CharacterNote(@JsonProperty("characterNoteID") int characterNoteID,
			@JsonProperty("characterID") int characterID,
			@JsonProperty("text") String text) {
		this.characterNoteID = characterNoteID;
		this.characterID = characterID;
		this.text = text;
	}

	/**
	 * @return the characterNoteID
	 */
	public int getCharacterNoteID() {
		return characterNoteID;
	}

	/**
	 * @return the characterID
	 */
	public int getCharacterID() {
		return characterID;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the updated
	 */
	public DateTime getUpdated() {
		return updated;
	}

	public void setUpdated(DateTime dateTime) {
		this.updated = dateTime;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public final boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof CharacterNote)) {
			return false;
		}
		CharacterNote rhs = (CharacterNote) obj;
		return new EqualsBuilder().append(characterNoteID, rhs.characterNoteID)
				.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		// pick hard-coded, randomly chosen prime numbers, ideally different for
		// each class
		return new HashCodeBuilder(23, 61).append(characterNoteID).toHashCode();
	}
}