package traps.api;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Data object that represents a Technique, which carries a cost and bonuses.
 * 
 * @author jeremy.long
 * 
 */
@JsonPropertyOrder({ "techniqueID", "techniqueName", "powerCost",
		"accuracyCost", "finesseCost", "hitBonus", "damageBonus",
		"pierceBonus", "speedBonus", "evadeBonus", "damageReductionBonus",
		"resistanceBonus", "exclusiveSet", "techniqueLevel", "updated" })
public class Technique {

	private final int techniqueID;

	@NotBlank
	private final String techniqueName;
	private final int powerCost, accuracyCost, finesseCost, hitBonus,
			damageBonus, pierceBonus, speedBonus, evadeBonus,
			damageReductionBonus, resistanceBonus, techniqueLevel;
	private final String exclusiveSet, specialEffects;

	private DateTime updated;

	@JsonCreator
	public Technique(@JsonProperty("techniqueID") int techniqueID,
			@JsonProperty("techniqueName") String techniqueName,
			@JsonProperty("powerCost") int powerCost,
			@JsonProperty("accuracyCost") int accuracyCost,
			@JsonProperty("finesseCost") int finesseCost,
			@JsonProperty("hitBonus") int hitBonus,
			@JsonProperty("damageBonus") int damageBonus,
			@JsonProperty("pierceBonus") int pierceBonus,
			@JsonProperty("speedBonus") int speedBonus,
			@JsonProperty("evadeBonus") int evadeBonus,
			@JsonProperty("damageReductionBonus") int damageReductionBonus,
			@JsonProperty("resistanceBonus") int resistanceBonus,
			@JsonProperty("exclusiveSet") String exclusiveSet,
			@JsonProperty("specialEffects") String specialEffects,
			@JsonProperty("techniqueLevel") int techniqueLevel) {
		this.techniqueID = techniqueID;
		this.techniqueName = techniqueName;
		this.powerCost = powerCost;
		this.accuracyCost = accuracyCost;
		this.finesseCost = finesseCost;
		this.hitBonus = hitBonus;
		this.damageBonus = damageBonus;
		this.pierceBonus = pierceBonus;
		this.speedBonus = speedBonus;
		this.evadeBonus = evadeBonus;
		this.damageReductionBonus = damageReductionBonus;
		this.resistanceBonus = resistanceBonus;
		this.exclusiveSet = exclusiveSet;
		this.specialEffects = specialEffects;
		this.techniqueLevel = techniqueLevel;
	}

	/**
	 * @return the techniqueID
	 */
	public int getTechniqueID() {
		return techniqueID;
	}

	/**
	 * @return the techniqueName
	 */
	public String getTechniqueName() {
		return techniqueName;
	}

	/**
	 * @return the updated
	 */
	public DateTime getUpdated() {
		return updated;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public final boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Technique)) {
			return false;
		}
		Technique rhs = (Technique) obj;
		return new EqualsBuilder().append(techniqueID, rhs.techniqueID)
				.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		// pick hard-coded, randomly chosen prime numbers, ideally different for
		// each class
		return new HashCodeBuilder(303, 313).append(techniqueID).toHashCode();
	}

	@Override
	public String toString() {
		return String.format("%s", getTechniqueName());
	}

	/**
	 * @return the powerCost
	 */
	public int getPowerCost() {
		return powerCost;
	}

	/**
	 * @return the accuracyCost
	 */
	public int getAccuracyCost() {
		return accuracyCost;
	}

	/**
	 * @return the finesseCost
	 */
	public int getFinesseCost() {
		return finesseCost;
	}

	/**
	 * @return the hitBonus
	 */
	public int getHitBonus() {
		return hitBonus;
	}

	/**
	 * @return the damageBonus
	 */
	public int getDamageBonus() {
		return damageBonus;
	}

	/**
	 * @return the pierceBonus
	 */
	public int getPierceBonus() {
		return pierceBonus;
	}

	/**
	 * @return the speedBonus
	 */
	public int getSpeedBonus() {
		return speedBonus;
	}

	/**
	 * @return the evadeBonus
	 */
	public int getEvadeBonus() {
		return evadeBonus;
	}

	/**
	 * @return the damageReductionBonus
	 */
	public int getDamageReductionBonus() {
		return damageReductionBonus;
	}

	/**
	 * @return the exclusiveSet
	 */
	public String getExclusiveSet() {
		return exclusiveSet;
	}

	/**
	 * @return the specialEffects
	 */
	public String getSpecialEffects() {
		return specialEffects;
	}

	/**
	 * @return the techniqueLevel
	 */
	public int getTechniqueLevel() {
		return techniqueLevel;
	}

	/**
	 * @return the resistanceBonus
	 */
	public int getResistanceBonus() {
		return resistanceBonus;
	}
}