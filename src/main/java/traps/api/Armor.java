package traps.api;

import java.util.Arrays;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.yammer.dropwizard.validation.InvalidEntityException;

/**
 * Armor model, representing a piece of armor that a character can equip.
 * Includes an enunmerator to categorize different types of armor, and help
 * ensure a {@code Character} can only equip one of each type.
 * 
 * @author jeremy.long
 * 
 */
@JsonPropertyOrder({ "armorID", "armorName", "damageReduction", "evade",
		"weight", "type", "hands", "cost", "updated" })
public class Armor {

	public enum ArmorType {
		HEAD, TORSO, LEGS, HANDS, SHIELD;
	}

	private final int armorID;

	@NotBlank
	private final String armorName;
	private final ArmorType type;
	private final int damageReduction, evade, weight, hands, cost;

	private DateTime updated;

	@JsonCreator
	public Armor(@JsonProperty("armorID") int armorID,
			@JsonProperty("armorName") String armorName,
			@JsonProperty("damageReduction") int damageReduction,
			@JsonProperty("evade") int evade,
			@JsonProperty("weight") int weight,
			@JsonProperty("type") String type,
			@JsonProperty("hands") int hands, @JsonProperty("cost") int cost) {
		this.armorID = armorID;
		this.armorName = armorName;
		this.damageReduction = damageReduction;
		this.evade = evade;
		this.weight = weight;
		this.hands = hands;
		this.cost = cost;

		try {
			this.type = ArmorType.valueOf(type);
		} catch (IllegalArgumentException e) {
			throw new InvalidEntityException(
					"The request entity had the following errors:",
					Arrays.asList(String.format("Armor type must be valid.")));
		}
	}

	/**
	 * @return the armorID
	 */
	public int getArmorID() {
		return armorID;
	}

	/**
	 * @return the armorName
	 */
	public String getArmorName() {
		return armorName;
	}

	/**
	 * @return the type
	 */
	public ArmorType getType() {
		return type;
	}

	/**
	 * @return the damageReduction
	 */
	public int getDamageReduction() {
		return damageReduction;
	}

	/**
	 * @return the evade
	 */
	public int getEvade() {
		return evade;
	}

	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 * @return the hands
	 */
	public int getHands() {
		return hands;
	}

	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	/**
	 * @return the updated
	 */
	public DateTime getUpdated() {
		return updated;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public final boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Armor)) {
			return false;
		}
		Armor rhs = (Armor) obj;
		return new EqualsBuilder().append(armorID, rhs.armorID).isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		// pick hard-coded, randomly chosen prime numbers, ideally different for
		// each class
		return new HashCodeBuilder(103, 13).append(armorID).toHashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String
				.format("%s (Damage Reduction: %d, Evade: %d, Weight: %d, Type: %s, Hands: %d)",
						getArmorName(), getDamageReduction(), getEvade(),
						getWeight(), getType().toString(), getHands());
	}
}