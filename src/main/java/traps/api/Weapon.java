package traps.api;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Data object that represents a weapon, which is linked to a specific
 * {@code CombatSkill}.
 * 
 * @author jeremy.long
 * 
 */
@JsonPropertyOrder({ "weaponID", "weaponName", "hitBonus", "damageDiceNumber",
		"damageDiceType", "pierce", "speed", "range", "hands", "cost",
		"epCost", "combatSkill", "updated" })
public class Weapon {

	private final int weaponID;

	@NotBlank
	private final String weaponName;
	private final int hitBonus, damageDiceNumber, damageDiceType, pierce,
			speed, range, hands, cost, epCost;
	private CombatSkill combatSkill;

	private DateTime updated;

	@JsonCreator
	public Weapon(@JsonProperty("weaponID") int weaponID,
			@JsonProperty("weaponName") String weaponName,
			@JsonProperty("hitBonus") int hitBonus,
			@JsonProperty("damageDiceNumber") int damageDiceNumber,
			@JsonProperty("damageDiceType") int damageDiceType,
			@JsonProperty("pierce") int pierce,
			@JsonProperty("speed") int speed, @JsonProperty("range") int range,
			@JsonProperty("hands") int hands, @JsonProperty("cost") int cost,
			@JsonProperty("epCost") int epCost,
			@JsonProperty("combatSkill") CombatSkill combatSkill) {
		this.weaponID = weaponID;
		this.weaponName = weaponName;
		this.combatSkill = combatSkill;
		this.hitBonus = hitBonus;
		this.damageDiceNumber = damageDiceNumber;
		this.damageDiceType = damageDiceType;
		this.pierce = pierce;
		this.speed = speed;
		this.range = range;
		this.hands = hands;
		this.cost = cost;
		this.epCost = epCost;
	}

	/**
	 * @return the weaponID
	 */
	public int getWeaponID() {
		return weaponID;
	}

	/**
	 * @return the weaponName
	 */
	public String getWeaponName() {
		return weaponName;
	}

	/**
	 * @return the hitBonus
	 */
	public int getHitBonus() {
		return hitBonus;
	}

	/**
	 * @return the damageDiceNumber
	 */
	public int getDamageDiceNumber() {
		return damageDiceNumber;
	}

	/**
	 * @return the damageDiceType
	 */
	public int getDamageDiceType() {
		return damageDiceType;
	}

	/**
	 * @return the pierce
	 */
	public int getPierce() {
		return pierce;
	}

	/**
	 * @return the speed
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * @return the range
	 */
	public int getRange() {
		return range;
	}

	/**
	 * @return the hands
	 */
	public int getHands() {
		return hands;
	}

	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	/**
	 * @return the updated
	 */
	public DateTime getUpdated() {
		return updated;
	}

	/**
	 * @return the combatSkill
	 */
	public CombatSkill getCombatSkill() {
		return combatSkill;
	}

	/**
	 * @param combatSkill
	 *            the combatSkill to set
	 */
	public void setCombatSkill(CombatSkill combatSkill) {
		this.combatSkill = combatSkill;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public final boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Weapon)) {
			return false;
		}
		Weapon rhs = (Weapon) obj;
		return new EqualsBuilder().append(weaponID, rhs.weaponID).isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		// pick hard-coded, randomly chosen prime numbers, ideally different for
		// each class
		return new HashCodeBuilder(17, 37).append(weaponID).toHashCode();
	}

	@Override
	public String toString() {
		return String
				.format("%s (Hit Bonus: %d, Damage: %dd%d, Pierce: %d, Speed: %d, EP: %d, Hands: %d, Range: %d, %s)",
						getWeaponName(), getHitBonus(), getDamageDiceNumber(),
						getDamageDiceType(), getPierce(), getSpeed(),
						getEpCost(), getHands(), getRange(), getCombatSkill());
	}

	public int getEpCost() {
		return epCost;
	}
}