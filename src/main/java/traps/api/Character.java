package traps.api;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;

import traps.api.Armor.ArmorType;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The Character model is a complete representation of a character, including
 * all vital attributes, equipment, notes, and current status effects. It
 * encapsulates logic for performing compound operations like increasing level,
 * or selecting a character class.
 * 
 * @author jeremy.long
 * 
 */
@JsonPropertyOrder({ "characterID", "characterName", "characterClass", "level",
		"hp", "hpTotal", "hpInitial", "ep", "epTotal", "epInitial", "strength",
		"dexterity", "agility", "health", "cunning", "willpower",
		"intelligence", "attribute", "strengthAttribute", "dexterityAttribute",
		"agilityAttribute", "healthAttribute", "cunningAttribute",
		"willpowerAttribute", "intelligenceAttribute", "strengthTotal",
		"dexterityTotal", "agilityTotal", "healthTotal", "cunningTotal",
		"willpowerTotal", "intelligenceTotal", "skill", "power", "accuracy",
		"finesse", "wild", "passive", "hitBonus", "damageBonus", "evadeBonus",
		"hpBonus", "epBonus", "sabotageBonus", "acceptanceBonus",
		"synergyBonus", "resistanceBonus", "carry", "damageReduction", "evade",
		"weight", "weightCapacity", "notes", "weapons", "armor", "updated" })
public class Character {

	public static final int ATTRIBUTE_PER_LEVEL = 2, SKILL_PER_LEVEL = 1,
			PASSIVE_PER_LEVEL = 1;

	private static final int MAX_HP_PER_LEVEL = 4, HP_PER_LEVEL_CONSTANT = 2,
			MAX_EP_PER_LEVEL = 4, EP_PER_LEVEL_CONSTANT = 0;

	private final int characterID;

	@NotBlank
	private final String characterName;
	private int level, hp, ep, hpInitial, epInitial, strength, dexterity,
			agility, health, cunning, willpower, intelligence, attribute,
			strengthAttribute, dexterityAttribute, agilityAttribute,
			healthAttribute, cunningAttribute, willpowerAttribute,
			intelligenceAttribute, skill, power, accuracy, finesse, wild,
			passive;

	private CharacterClass characterClass;
	private DateTime updated;

	private final Set<Weapon> weapons;
	private final Map<ArmorType, Armor> armor;
	private final Set<CharacterNote> notes;

	@JsonIgnore
	private Set<StatusEffect> statusEffects;

	@JsonCreator
	public Character(@JsonProperty("characterID") int characterID,
			@JsonProperty("characterName") String characterName,
			@JsonProperty("level") int level, @JsonProperty("hp") int hp,
			@JsonProperty("ep") int ep,
			@JsonProperty("hpInitial") int hpInitial,
			@JsonProperty("epInitial") int epInitial,
			@JsonProperty("strength") int strength,
			@JsonProperty("dexterity") int dexterity,
			@JsonProperty("agility") int agility,
			@JsonProperty("health") int health,
			@JsonProperty("cunning") int cunning,
			@JsonProperty("willpower") int willpower,
			@JsonProperty("intelligence") int intelligence,
			@JsonProperty("attribute") int attribute,
			@JsonProperty("strengthAttribute") int strengthAttribute,
			@JsonProperty("dexterityAttribute") int dexterityAttribute,
			@JsonProperty("agilityAttribute") int agilityAttribute,
			@JsonProperty("healthAttribute") int healthAttribute,
			@JsonProperty("cunningAttribute") int cunningAttribute,
			@JsonProperty("willpowerAttribute") int willpowerAttribute,
			@JsonProperty("intelligenceAttribute") int intelligenceAttribute,
			@JsonProperty("skill") int skill, @JsonProperty("power") int power,
			@JsonProperty("accuracy") int accuracy,
			@JsonProperty("finesse") int finesse,
			@JsonProperty("wild") int wild, @JsonProperty("passive") int passive) {
		this.characterID = characterID;
		this.characterName = characterName;
		this.level = level;
		this.characterClass = null;
		this.hp = hp;
		this.ep = ep;
		this.hpInitial = hpInitial;
		this.epInitial = epInitial;
		this.strength = strength;
		this.dexterity = dexterity;
		this.agility = agility;
		this.health = health;
		this.cunning = cunning;
		this.willpower = willpower;
		this.health = health;
		this.intelligence = intelligence;
		this.strengthAttribute = strengthAttribute;
		this.dexterityAttribute = dexterityAttribute;
		this.agilityAttribute = agilityAttribute;
		this.healthAttribute = healthAttribute;
		this.cunningAttribute = cunningAttribute;
		this.willpowerAttribute = willpowerAttribute;
		this.intelligenceAttribute = intelligenceAttribute;
		this.attribute = attribute;
		this.skill = skill;
		this.power = power;
		this.accuracy = accuracy;
		this.finesse = finesse;
		this.wild = wild;
		this.passive = passive;

		this.notes = new LinkedHashSet<CharacterNote>();
		this.weapons = new LinkedHashSet<Weapon>();
		this.armor = new HashMap<ArmorType, Armor>();
		this.statusEffects = new LinkedHashSet<StatusEffect>();
	}

	/**
	 * Grants the bonuses associated with the character's current class
	 */
	private void grantClassBonus() {
		if (characterClass != null) {
			setHp(getHp() + characterClass.getHpInitialBonus()
					+ characterClass.getHpLevelBonus() * (level - 1));
			setEp(getEp() + characterClass.getEpInitialBonus()
					+ characterClass.getEpLevelBonus() * (level - 1));

			setStrength(getStrength()
					+ characterClass.getStrengthInitialBonus());
			setDexterity(getDexterity()
					+ characterClass.getDexterityInitialBonus());
			setAgility(getAgility() + characterClass.getAgilityInitialBonus());
			setHealth(getHealth() + characterClass.getHealthInitialBonus());
			setCunning(getCunning() + characterClass.getCunningInitialBonus());
			setWillpower(getWillpower()
					+ characterClass.getWillpowerInitialBonus());
			setIntelligence(getIntelligence()
					+ characterClass.getIntelligenceInitialBonus());
			setAttribute(getAttribute()
					+ characterClass.getAttributeInitialBonus());

			setSkill(getSkill() + characterClass.getSkillInitialBonus());
			setPassive(getPassive() + characterClass.getPassiveInitialBonus());
		}
	}

	/**
	 * Revokes the bonuses associated with a character's current class
	 */
	private void revokeClassBonus() {
		if (characterClass != null) {
			// Deallocate all bonus points if this class granted any.
			if (characterClass.getAttributeInitialBonus() > 0
					|| characterClass.getSkillInitialBonus() > 0) {
				resetPoints();
			}

			// Remove class bonuses.
			setHp(getHp() - characterClass.getHpInitialBonus()
					- characterClass.getHpLevelBonus() * (level - 1));
			setEp(getEp() - characterClass.getEpInitialBonus()
					- characterClass.getEpLevelBonus() * (level - 1));

			setStrength(getStrength()
					- characterClass.getStrengthInitialBonus());
			setDexterity(getDexterity()
					- characterClass.getDexterityInitialBonus());
			setAgility(getAgility() - characterClass.getAgilityInitialBonus());
			setHealth(getHealth() - characterClass.getHealthInitialBonus());
			setCunning(getCunning() - characterClass.getCunningInitialBonus());
			setWillpower(getWillpower()
					- characterClass.getWillpowerInitialBonus());
			setIntelligence(getIntelligence()
					- characterClass.getIntelligenceInitialBonus());
			setAttribute(getAttribute()
					- characterClass.getAttributeInitialBonus());

			setSkill(getSkill() - characterClass.getSkillInitialBonus());
			setPassive(getPassive() - characterClass.getPassiveInitialBonus());
		}
	}

	/**
	 * Sets all necessary attributes to make the character level one.
	 */
	public void makeLevelOne() {
		this.setHp(getHpInitial() + getCharacterClass().getHpInitialBonus());
		this.setEp(getEpInitial() + getCharacterClass().getEpInitialBonus());

		this.resetPoints();

		this.setAttribute(characterClass.getAttributeInitialBonus());

		this.setSkill(characterClass.getSkillInitialBonus());
		this.setPassive(characterClass.getPassiveInitialBonus());
	}

	/**
	 * Increases the character's level.
	 */
	public void levelUp() {
		Random random = new Random(System.currentTimeMillis());

		this.hp += this.characterClass.getHpLevelBonus()
				+ random.nextInt(MAX_HP_PER_LEVEL - 1) + 1
				+ HP_PER_LEVEL_CONSTANT;
		this.ep += this.characterClass.getEpLevelBonus()
				+ random.nextInt(MAX_EP_PER_LEVEL - 1) + 1
				+ EP_PER_LEVEL_CONSTANT;

		this.attribute += ATTRIBUTE_PER_LEVEL;

		this.skill += SKILL_PER_LEVEL;
		this.passive += PASSIVE_PER_LEVEL;
	}

	/**
	 * Resets all bonus points.
	 */
	public void resetPoints() {
		this.attribute += this.strengthAttribute + this.dexterityAttribute
				+ this.agilityAttribute + this.healthAttribute
				+ this.cunningAttribute + this.willpowerAttribute
				+ this.intelligenceAttribute;

		this.strengthAttribute = 0;
		this.dexterityAttribute = 0;
		this.agilityAttribute = 0;
		this.healthAttribute = 0;
		this.cunningAttribute = 0;
		this.willpowerAttribute = 0;
		this.intelligenceAttribute = 0;

		this.skill += this.power + this.accuracy + this.finesse + 2 * this.wild;

		this.power = 0;
		this.accuracy = 0;
		this.finesse = 0;
		this.wild = 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return characterName;
	}

	/**
	 * @return ID of the character.
	 */
	public int getCharacterID() {
		return characterID;
	}

	/**
	 * @return name of the character class.
	 */
	public String getCharacterName() {
		return characterName;
	}

	/**
	 * @return the characterClass
	 */
	public CharacterClass getCharacterClass() {
		return characterClass;
	}

	/**
	 * @param characterClass
	 *            the characterClass to set
	 */
	public void setCharacterClass(CharacterClass characterClass) {
		if (this.characterClass != null) {
			revokeClassBonus();
			this.characterClass = characterClass;
			grantClassBonus();
		} else {
			this.characterClass = characterClass;
		}
	}

	/**
	 * @param updated
	 */
	public void setUpdated(DateTime updated) {
		this.updated = updated;
	}

	public int getStrengthTotal() {
		return this.strength + this.strengthAttribute;
	}

	public int getDexterityTotal() {
		return this.dexterity + this.dexterityAttribute;
	}

	public int getAgilityTotal() {
		return this.agility + this.agilityAttribute;
	}

	public int getHealthTotal() {
		return this.health + this.healthAttribute;
	}

	public int getCunningTotal() {
		return this.cunning + this.cunningAttribute;
	}

	public int getWillpowerTotal() {
		return this.willpower + this.willpowerAttribute;
	}

	public int getIntelligenceTotal() {
		return this.intelligence + this.intelligenceAttribute;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public int getHp() {
		return hp;
	}

	public int getEp() {
		return ep;
	}

	public int getDexterity() {
		return dexterity;
	}

	public int getAgility() {
		return agility;
	}

	public int getHealth() {
		return health;
	}

	public int getWillpower() {
		return willpower;
	}

	public int getIntelligence() {
		return intelligence;
	}

	/**
	 * @param hp
	 *            the hp to set
	 */
	public void setHp(int hp) {
		this.hp = hp;
	}

	/**
	 * @param ep
	 *            the ep to set
	 */
	public void setEp(int ep) {
		this.ep = ep;
	}

	/**
	 * @param dexterity
	 *            the dexterity to set
	 */
	public void setDexterity(int dexterity) {
		this.dexterity = dexterity;
	}

	/**
	 * @param agility
	 *            the agility to set
	 */
	public void setAgility(int agility) {
		this.agility = agility;
	}

	/**
	 * @param health
	 *            the health to set
	 */
	public void setHealth(int health) {
		this.health = health;
	}

	/**
	 * @param willpower
	 *            the willpower to set
	 */
	public void setWillpower(int willpower) {
		this.willpower = willpower;
	}

	/**
	 * @param intelligence
	 *            the intelligence to set
	 */
	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	}

	/**
	 * @return the attribute
	 */
	public int getAttribute() {
		return attribute;
	}

	/**
	 * @param attribute
	 *            the attribute to set
	 */
	public void setAttribute(int attribute) {
		this.attribute = attribute;
	}

	/**
	 * @return the skill
	 */
	public int getSkill() {
		return skill;
	}

	/**
	 * @param skill
	 *            the skill to set
	 */
	public void setSkill(int skill) {
		this.skill = skill;
	}

	/**
	 * @return the power
	 */
	public int getPower() {
		return power;
	}

	/**
	 * @param power
	 *            the power to set
	 */
	public void setPower(int power) {
		this.power = power;
	}

	/**
	 * @return the accuracy
	 */
	public int getAccuracy() {
		return accuracy;
	}

	/**
	 * @param accuracy
	 *            the accuracy to set
	 */
	public void setAccuracy(int accuracy) {
		this.accuracy = accuracy;
	}

	/**
	 * @return the finesse
	 */
	public int getFinesse() {
		return finesse;
	}

	/**
	 * @param finesse
	 *            the finesse to set
	 */
	public void setFinesse(int finesse) {
		this.finesse = finesse;
	}

	/**
	 * @return the passive
	 */
	public int getPassive() {
		return passive;
	}

	/**
	 * @param passive
	 *            the passive to set
	 */
	public void setPassive(int passive) {
		this.passive = passive;
	}

	/**
	 * @return the cunning
	 */
	public int getCunning() {
		return cunning;
	}

	/**
	 * @param cunning
	 *            the cunning to set
	 */
	public void setCunning(int cunning) {
		this.cunning = cunning;
	}

	/**
	 * @return the strengthAttribute
	 */
	public int getStrengthAttribute() {
		return strengthAttribute;
	}

	/**
	 * @param strengthAttribute
	 *            the strengthAttribute to set
	 */
	public void setStrengthAttribute(int strengthAttribute) {
		this.strengthAttribute = strengthAttribute;
	}

	/**
	 * @return the dexterityAttribute
	 */
	public int getDexterityAttribute() {
		return dexterityAttribute;
	}

	/**
	 * @param dexterityAttribute
	 *            the dexterityAttribute to set
	 */
	public void setDexterityAttribute(int dexterityAttribute) {
		this.dexterityAttribute = dexterityAttribute;
	}

	/**
	 * @return the agilityAttribute
	 */
	public int getAgilityAttribute() {
		return agilityAttribute;
	}

	/**
	 * @param agilityAttribute
	 *            the agilityAttribute to set
	 */
	public void setAgilityAttribute(int agilityAttribute) {
		this.agilityAttribute = agilityAttribute;
	}

	/**
	 * @return the healthAttribute
	 */
	public int getHealthAttribute() {
		return healthAttribute;
	}

	/**
	 * @param healthAttribute
	 *            the healthAttribute to set
	 */
	public void setHealthAttribute(int healthAttribute) {
		this.hp -= 3 * this.healthAttribute;
		this.healthAttribute = healthAttribute;
		this.hp += 3 * this.healthAttribute;
	}

	/**
	 * @return the cunningAttribute
	 */
	public int getCunningAttribute() {
		return cunningAttribute;
	}

	/**
	 * @param cunningAttribute
	 *            the cunningAttribute to set
	 */
	public void setCunningAttribute(int cunningAttribute) {
		this.cunningAttribute = cunningAttribute;
	}

	/**
	 * @return the willpowerAttribute
	 */
	public int getWillpowerAttribute() {
		return willpowerAttribute;
	}

	/**
	 * @param willpowerAttribute
	 *            the willpowerAttribute to set
	 */
	public void setWillpowerAttribute(int willpowerAttribute) {
		this.willpowerAttribute = willpowerAttribute;
	}

	/**
	 * @return the intelligenceAttribute
	 */
	public int getIntelligenceAttribute() {
		return intelligenceAttribute;
	}

	/**
	 * @param intelligenceAttribute
	 *            the intelligenceAttribute to set
	 */
	public void setIntelligenceAttribute(int intelligenceAttribute) {
		this.intelligenceAttribute = intelligenceAttribute;
	}

	/**
	 * @return the wild
	 */
	public int getWild() {
		return wild;
	}

	/**
	 * @param wild
	 *            the wild to set
	 */
	public void setWild(int wild) {
		this.wild = wild;
	}

	/**
	 * @return the updated
	 */
	public String getUpdated() {
		return (updated != null) ? updated.toString() : null;
	}

	public Set<CharacterNote> getNotes() {
		return notes;
	}

	public void addNote(CharacterNote map) {
		this.notes.add(map);
	}

	public int getHpTotal() {
		return this.hp + getHpBonus();
	}

	public int getEpTotal() {
		return this.ep + getEpBonus();
	}

	/**
	 * @return the Character as a single, displayable String
	 */
	public String toCharacterSheet() {
		StringBuilder result = new StringBuilder();

		result.append(String.format("%s - level %d %s\n\n", this.characterName,
				this.level, this.characterClass.getCharacterClassName()));

		result.append(String.format("HP: %3d / %-3d \n\n\n", this.getHpTotal(),
				this.getHpTotal()));
		result.append(String.format("EP: %3d / %-3d \n\n\n", this.getEpTotal(),
				this.getEpTotal()));

		String attributeFormat = "%-12s  \t%4d";
		result.append(String.format(attributeFormat
				+ "\t+%d to damage, +%d to weight capacity\n", "Strength",
				this.getStrengthTotal(), this.getDamageBonus(),
				this.getStrengthTotal()));
		result.append(String.format(attributeFormat + "\t+%d to hit\n",
				"Dexterity", this.getDexterityTotal(), this.getHitBonus()));
		result.append(String.format(attributeFormat + "\t+%d to evade\n",
				"Agility", this.getAgilityTotal(), this.getEvadeBonus()));
		result.append(String.format(attributeFormat
				+ "\t+%d to HP, +%d to weight capacity\n", "Health",
				this.getHealthTotal(), this.getHpBonus(), this.getHealthTotal()));
		result.append(String.format(attributeFormat
				+ "\t+%d to sabotage, +%d to accept synergy\n", "Cunning",
				this.getCunningTotal(), this.getSabotageBonus(),
				this.getAcceptanceBonus()));
		result.append(String.format(attributeFormat
				+ "\t+%d to resistance, +%d to grant synergy\n", "Willpower",
				this.getWillpowerTotal(), this.getSynergyBonus(),
				this.getResistanceBonus()));
		result.append(String.format(attributeFormat
				+ "\t+%d to carryover, +%d to EP\n\n", "Intelligence",
				this.getIntelligenceTotal(), this.getCarry(), this.getEpBonus()));

		result.append(String.format("Damage Reduction:%2d \tEvade: %2d\n",
				this.getDamageReduction(), this.getEvade()));
		result.append(String.format("Weight:          %2d / %-2d\n\n",
				this.getWeight(), this.getWeightCapacity()));

		result.append("Armor:\n");
		for (Armor a : armor.values()) {
			result.append(a + "\n");
		}
		result.append("\n");

		String skillFormat = "%-12s  \t%4d";
		result.append(String.format(skillFormat + "\n", "Power",
				this.getPower()));
		result.append(String.format(skillFormat + "\n", "Accuracy",
				this.getAccuracy()));
		result.append(String.format(skillFormat + "\n", "Finesse",
				this.getFinesse()));
		result.append(String.format(skillFormat + "\n", "Wild", this.getWild()));
		result.append(String.format(skillFormat + "\n\n", "Passive",
				this.getPassive()));

		result.append("Weapons:\n");
		for (Weapon w : weapons) {
			result.append(w + "\n");
		}
		result.append("\n");

		result.append("Notes:\n\n");
		for (CharacterNote note : this.notes) {
			result.append(note.getText());
			result.append("\n\n");
		}

		result.append("Status Effects:\n\n");

		return result.toString();
	}

	public int getResistanceBonus() {
		return Math.max(0, this.getWillpowerTotal()
				+ getStatusEffectImpact("Willpower"))
				* 3 + getStatusEffectImpact("Synergy");
	}

	public int getSynergyBonus() {
		return Math.max(0, this.getWillpowerTotal()
				+ getStatusEffectImpact("Willpower"))
				* 3 + getStatusEffectImpact("Synergy");
	}

	public int getAcceptanceBonus() {
		return Math.max(0, this.getCunningTotal()
				+ getStatusEffectImpact("Cunning"))
				* 3 + getStatusEffectImpact("Sabotage");
	}

	public int getSabotageBonus() {
		return Math.max(0, this.getCunningTotal()
				+ getStatusEffectImpact("Cunning"))
				* 3 + getStatusEffectImpact("Sabotage");
	}

	public int getHpBonus() {
		return this.getHealthTotal() * 3;
	}

	public int getEpBonus() {
		return this.getIntelligenceTotal() * 2;
	}

	public int getEvadeBonus() {
		return Math.max(0, this.getAgilityTotal()
				+ getStatusEffectImpact("Agility")) * 2;
	}

	public int getHitBonus() {
		return Math.max(0, this.getDexterityTotal()
				+ getStatusEffectImpact("Dexterity")) * 2;
	}

	public int getDamageReduction() {
		int damageReduction = 0;
		for (Armor armor : this.armor.values()) {
			damageReduction += armor.getDamageReduction();
		}
		return Math.max(0, damageReduction + getStatusEffectImpact("Armor"))
				+ getStatusEffectImpact("Protection");
	}

	public int getCarry() {
		return Math.max(0, this.getIntelligenceTotal()
				+ getStatusEffectImpact("Carry")
				+ getStatusEffectImpact("Intelligence"));
	}

	public int getEvade() {
		int evade = 10 + this.getEvadeBonus();
		for (Armor armor : this.armor.values()) {
			evade += armor.getEvade();
		}
		return evade;
	}

	public int getWeight() {
		int weight = 0;
		for (Armor armor : this.armor.values()) {
			weight += armor.getWeight();
		}
		return weight;
	}

	public int getWeightCapacity() {
		return Math.max(0, this.getStrengthTotal() + this.getHealthTotal()
				+ getStatusEffectImpact("Strength")
				+ getStatusEffectImpact("Health"));
	}

	/**
	 * @return the hpInitial
	 */
	public int getHpInitial() {
		return hpInitial;
	}

	/**
	 * @param hpInitial
	 *            the hpInitial to set
	 */
	public void setHpInitial(int hpInitial) {
		this.hpInitial = hpInitial;
	}

	/**
	 * @return the epInitial
	 */
	public int getEpInitial() {
		return epInitial;
	}

	/**
	 * @param epInitial
	 *            the epInitial to set
	 */
	public void setEpInitial(int epInitial) {
		this.epInitial = epInitial;
	}

	public void addWeapon(Weapon weapon) {
		this.weapons.add(weapon);
	}

	public void removeWeapon(Weapon weapon) {
		this.weapons.remove(weapon);
	}

	public Set<Weapon> getWeapons() {
		return weapons;
	}

	public void addArmor(Armor armor) {
		this.armor.put(armor.getType(), armor);
	}

	public void removeArmor(Armor armor) {
		if (this.armor.containsValue(armor)) {
			this.armor.remove(armor.getType());
		}
	}

	public Map<ArmorType, Armor> getArmor() {
		return armor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public final boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Character)) {
			return false;
		}
		Character rhs = (Character) obj;
		return new EqualsBuilder().append(characterID, rhs.characterID)
				.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		// pick hard-coded, randomly chosen prime numbers, ideally different for
		// each class
		return new HashCodeBuilder(211, 239).append(characterID).toHashCode();
	}

	public int getDamageBonus() {
		return Math.max(0, this.getStrengthTotal()
				+ getStatusEffectImpact("Strength"))
				+ getStatusEffectImpact("Damage");
	}

	public void setStatusEffects(Set<StatusEffect> statusEffects) {
		this.statusEffects = statusEffects;
	}

	private int getStatusEffectImpact(String attribute) {
		int result = 0;
		for (StatusEffect status : statusEffects) {
			if (status.getAttributeEffects().containsKey(attribute)) {
				result += status.getAttributeEffects().get(attribute);
			}
		}
		return result;
	}
}
