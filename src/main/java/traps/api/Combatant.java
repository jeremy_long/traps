package traps.api;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * A Combatant represents an instance of a {@code Character} within a particular
 * {@code Encounter}. It tracks the current resources that character has
 * throughout the lifecycle of the encounter. The relationship between a
 * Combatant and a Character is referential, so any changes to the Character
 * will be reflected by the Combatant.
 * 
 * @author jeremy.long
 * 
 */
@JsonPropertyOrder({ "combatantID", "character", "encounterID", "currentHp",
		"currentEp", "currentPower", "currentAccuracy", "currentFinesse",
		"currentWild", "currentHitBonus", "previousHitBonus",
		"currentDamageBonus", "previousDamageBonus", "currentEvade",
		"previousEvade", "currentDamageReduction", "previousDamageReduction",
		"currentSabotageBonus", "previousSabotageBonus", "currentSynergyBonus",
		"previousSynergyBonus", "currentSpeedModifier", "currentCarry",
		"actions", "statusEffects", "previousStatusEffects" })
public class Combatant implements Comparable<Combatant> {

	private static final int CARRY_OVER_WILD_CONVERSION_COST = 2;

	private final int combatantID;

	private Character character;
	private int encounterID, currentHp, currentEp, currentPower,
			currentAccuracy, currentFinesse, currentWild;

	private Set<Action> actions;
	private final Set<StatusEffect> statusEffects;

	@JsonCreator
	public Combatant(@JsonProperty("combatantID") int combatantID,
			@JsonProperty("character") Character character,
			@JsonProperty("encounterID") int encounterID,
			@JsonProperty("currentHp") int currentHp,
			@JsonProperty("currentEp") int currentEp,
			@JsonProperty("currentPower") int currentPower,
			@JsonProperty("currentAccuracy") int currentAccuracy,
			@JsonProperty("currentFinesse") int currentFinesse,
			@JsonProperty("currentWild") int currentWild) {
		this.combatantID = combatantID;
		this.character = character;
		this.encounterID = encounterID;
		this.currentHp = currentHp;
		this.currentEp = currentEp;
		this.currentPower = currentPower;
		this.currentAccuracy = currentAccuracy;
		this.currentFinesse = currentFinesse;
		this.currentWild = currentWild;

		this.actions = new LinkedHashSet<Action>();
		this.statusEffects = new LinkedHashSet<StatusEffect>();
	}

	/**
	 * @return the combatantID
	 */
	public int getCombatantID() {
		return combatantID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public final boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Combatant)) {
			return false;
		}
		Combatant rhs = (Combatant) obj;
		return new EqualsBuilder().append(getCombatantID(),
				rhs.getCombatantID()).isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		// pick hard-coded, randomly chosen prime numbers, ideally different for
		// each class
		return new HashCodeBuilder(403, 413).append(getCombatantID())
				.toHashCode();
	}

	@Override
	public String toString() {
		return String.format("%s", getCharacter());
	}

	/**
	 * @return the character
	 */
	public Character getCharacter() {
		return character;
	}

	/**
	 * @return the encounterID
	 */
	public int getEncounterID() {
		return encounterID;
	}

	/**
	 * @return the currentHp
	 */
	public int getCurrentHp() {
		return currentHp;
	}

	/**
	 * @return the currentEp
	 */
	public int getCurrentEp() {
		return currentEp;
	}

	/**
	 * @return the currentPower
	 */
	public int getCurrentPower() {
		return currentPower;
	}

	/**
	 * @return the currentAccuracy
	 */
	public int getCurrentAccuracy() {
		return currentAccuracy;
	}

	/**
	 * @return the currentFinesse
	 */
	public int getCurrentFinesse() {
		return currentFinesse;
	}

	/**
	 * @return the currentWild
	 */
	public int getCurrentWild() {
		return currentWild;
	}

	/**
	 * Initializes the Combatant's characteristics based on the Character it
	 * represents
	 */
	public void initialize() {
		this.currentHp = this.character.getHpTotal();
		this.currentEp = this.character.getEpTotal();
		this.currentPower = 0;
		this.currentAccuracy = 0;
		this.currentFinesse = 0;
		this.currentWild = 0;
		accumulateSkill();
	}

	/**
	 * Accumulates skill points based on the Character it represents
	 */
	public void accumulateSkill() {
		this.currentPower += this.character.getPower();
		this.currentAccuracy += this.character.getAccuracy();
		this.currentFinesse += this.character.getFinesse();
		this.currentWild += this.character.getWild();
	}

	/**
	 * Determine how many skill points should carry over between rounds
	 */
	public void carryOver() {
		if (this.currentPower < 0) {
			this.currentWild += this.currentPower;
			this.currentPower = 0;
		}
		if (this.currentAccuracy < 0) {
			this.currentWild += this.currentAccuracy;
			this.currentAccuracy = 0;
		}
		if (this.currentFinesse < 0) {
			this.currentWild += this.currentFinesse;
			this.currentFinesse = 0;
		}

		int carry = this.currentPower + this.currentAccuracy
				+ this.currentFinesse + this.currentWild;
		while (carry > getCurrentCarry()) {
			if (this.currentWild >= getCurrentCarry()) {
				this.currentPower = 0;
				this.currentAccuracy = 0;
				this.currentFinesse = 0;
				this.currentWild = getCurrentCarry();
			} else {
				convertCarryOverToWild();
			}
			carry = this.currentPower + this.currentAccuracy
					+ this.currentFinesse + this.currentWild;
		}

		applyRoundEndStatusEffects();
	}

	private void convertCarryOverToWild() {
		Random rand = new Random();
		for (int numConverted = 0; numConverted < CARRY_OVER_WILD_CONVERSION_COST;) {
			int selection = rand.nextInt(3);
			if (selection == 0) {
				if (this.currentPower > 0) {
					this.currentPower--;
					numConverted++;
				}
			} else if (selection == 1) {
				if (this.currentAccuracy > 0) {
					this.currentAccuracy--;
					numConverted++;
				}
			} else if (selection == 2) {
				if (this.currentFinesse > 0) {
					this.currentFinesse--;
					numConverted++;
				}
			}
		}
		this.currentWild++;
	}

	private void applyRoundEndStatusEffects() {
		for (StatusEffect status : statusEffects) {
			if (status.getAttributeEffects().containsKey("HP")) {
				this.currentHp += status.getAttributeEffects().get("HP");
			}
		}
	}

	public void setCharacter(Character character) {
		this.character = character;
	}

	/**
	 * @return the actions
	 */
	public Set<Action> getActions() {
		return actions;
	}

	public void addAction(Action action) {
		this.actions.add(action);
	}

	public void removeAction(Action action) {
		this.actions.remove(action);
	}

	public void use(Technique technique) {
		currentPower -= technique.getPowerCost();
		currentAccuracy -= technique.getAccuracyCost();
		currentFinesse -= technique.getFinesseCost();
	}

	public void cancel(Technique technique) {
		currentPower += technique.getPowerCost();
		currentAccuracy += technique.getAccuracyCost();
		currentFinesse += technique.getFinesseCost();
	}

	public int getCurrentHitBonus() {
		if (actions.size() > 1) {
			character.setStatusEffects(getApplicableStatusEffects(actions
					.iterator().next().getRound()));
		}
		int currentHitBonus = character.getHitBonus();
		return currentHitBonus;
	}

	public int getPreviousHitBonus() {
		if (actions.size() > 1) {
			character.setStatusEffects(getApplicableStatusEffects(actions
					.iterator().next().getRound() - 1));
		}
		int previousHitBonus = character.getHitBonus();
		return previousHitBonus;
	}

	private Set<StatusEffect> getApplicableStatusEffects(int round) {
		Set<StatusEffect> applicableEffects = new LinkedHashSet<StatusEffect>();
		for (StatusEffect effect : statusEffects) {
			if (effect.getRoundInflicted() < round
					&& (effect.getPersistent() || effect.getRoundInflicted() >= round - 1)) {
				applicableEffects.add(effect);
			}
		}
		return applicableEffects;
	}

	public int getCurrentDamageBonus() {
		if (actions.size() > 1) {
			character.setStatusEffects(getApplicableStatusEffects(actions
					.iterator().next().getRound()));
		}
		int currentDamageBonus = character.getDamageBonus();
		return currentDamageBonus;
	}

	public int getPreviousDamageBonus() {
		if (actions.size() > 1) {
			character.setStatusEffects(getApplicableStatusEffects(actions
					.iterator().next().getRound() - 1));
		}
		int previousDamageBonus = character.getDamageBonus();
		return previousDamageBonus;
	}

	public int getCurrentEvade() {
		if (actions.size() > 1) {
			character.setStatusEffects(getApplicableStatusEffects(actions
					.iterator().next().getRound()));
		}
		int currentEvadeBonus = character.getEvade();
		return currentEvadeBonus;
	}

	public int getPreviousEvade() {
		if (actions.size() > 1) {
			character.setStatusEffects(getApplicableStatusEffects(actions
					.iterator().next().getRound() - 1));
		}
		int previousEvadeBonus = character.getEvade();
		return previousEvadeBonus;
	}

	public int getCurrentDamageReduction() {
		if (actions.size() > 1) {
			character.setStatusEffects(getApplicableStatusEffects(actions
					.iterator().next().getRound()));
		}
		int currentDamageReduction = character.getDamageReduction();
		return currentDamageReduction;
	}

	public int getPreviousDamageReduction() {
		if (actions.size() > 1) {
			character.setStatusEffects(getApplicableStatusEffects(actions
					.iterator().next().getRound() - 1));
		}
		int previousDamageReduction = character.getDamageReduction();
		return previousDamageReduction;
	}

	public int getCurrentSabotageBonus() {
		if (actions.size() > 1) {
			character.setStatusEffects(getApplicableStatusEffects(actions
					.iterator().next().getRound()));
		}
		int currentSabotageBonus = character.getSabotageBonus();
		return currentSabotageBonus;
	}

	public int getPreviousSabotageBonus() {
		if (actions.size() > 1) {
			character.setStatusEffects(getApplicableStatusEffects(actions
					.iterator().next().getRound() - 1));
		}
		int previousSabotageBonus = character.getSabotageBonus();
		return previousSabotageBonus;
	}

	public int getCurrentSynergyBonus() {
		if (actions.size() > 1) {
			character.setStatusEffects(getApplicableStatusEffects(actions
					.iterator().next().getRound()));
		}
		int currentSynergyBonus = character.getSynergyBonus();
		return currentSynergyBonus;
	}

	public int getPreviousSynergyBonus() {
		if (actions.size() > 1) {
			character.setStatusEffects(getApplicableStatusEffects(actions
					.iterator().next().getRound() - 1));
		}
		int previousSynergyBonus = character.getSynergyBonus();
		return previousSynergyBonus;
	}

	public int getCurrentCarry() {
		if (actions.size() > 1) {
			character.setStatusEffects(getApplicableStatusEffects(actions
					.iterator().next().getRound() - 1));
		}
		int currentCarry = character.getCarry();
		return currentCarry;
	}

	public int getCurrentSpeedModifier() {
		int modifier = 0;
		if (actions.size() > 0) {
			Set<StatusEffect> effects = getApplicableStatusEffects(actions
					.iterator().next().getRound() - 1);
			for (StatusEffect effect : effects) {
				if (effect.getAttributeEffects().containsKey("Speed")) {
					modifier += effect.getAttributeEffects().get("Speed");
				}
			}
		}
		return modifier;
	}

	@Override
	public int compareTo(Combatant otherCombatant) {
		if (getActions().size() > 1 && otherCombatant.getActions().size() > 1) {
			Iterator<Action> actionIterator = getActions().iterator();
			actionIterator.next();
			Integer initiative1 = actionIterator.next().getInitiative();

			actionIterator = otherCombatant.getActions().iterator();
			actionIterator.next();
			Integer initiative2 = actionIterator.next().getInitiative();

			return initiative2.compareTo(initiative1);
		}
		return 0;
	}

	/**
	 * @return the statusEffects
	 */
	public Set<StatusEffect> getStatusEffects() {
		return statusEffects;
	}

	public Set<StatusEffect> getPreviousStatusEffects() {
		Set<StatusEffect> effects = new LinkedHashSet<StatusEffect>();
		if (actions.size() > 0) {
			effects = getApplicableStatusEffects(actions.iterator().next()
					.getRound() - 1);
		}
		return effects;
	}

	public void addStatusEffect(StatusEffect statusEffect) {
		this.statusEffects.add(statusEffect);
	}

	public void switchWeapons(Weapon oldWeapon, Weapon currentWeapon) {
		this.currentEp += oldWeapon.getEpCost() - currentWeapon.getEpCost();
	}

	public void sortActions() {
		Action[] actionsArray = new Action[actions.size()];
		actions.toArray(actionsArray);
		actions = new LinkedHashSet<Action>();

		Arrays.sort(actionsArray);
		for (Action action : actionsArray) {
			actions.add(action);
		}
	}
}