package traps.api;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * An Action represents the action that a {@code Combatant} performs on a given
 * round of an {@code Encounter}. This involves spending some number of their
 * accumulated points in order to perform zero or more {@code Technique}s that
 * result in bonuses of various sorts to the action.
 * 
 * @author jeremy.long
 * 
 */
@JsonPropertyOrder({ "actionID", "combatantID", "weaponID", "round",
		"initiative", "weapon", "powerCost", "accuracyCost", "finesseCost",
		"hitBonus", "damageBonus", "pierceBonus", "evadeBonus",
		"damageReductionBonus", "speed" })
public class Action implements Comparable<Action> {

	private final int actionID;

	private final int combatantID, weaponID, round;
	private final Set<Technique> techniques;

	private int initiative;
	private Weapon weapon;

	@JsonCreator
	public Action(@JsonProperty("actionID") int actionID,
			@JsonProperty("combatantID") int combatantID,
			@JsonProperty("weaponID") int weaponID,
			@JsonProperty("round") int round,
			@JsonProperty("initiative") int initiative) {
		this.actionID = actionID;
		this.combatantID = combatantID;
		this.weaponID = weaponID;
		this.round = round;
		this.initiative = initiative;

		this.techniques = new HashSet<Technique>();
		this.weapon = null;
	}

	/**
	 * @return the actionID
	 */
	public int getActionID() {
		return actionID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public final boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Action)) {
			return false;
		}
		Action rhs = (Action) obj;
		return new EqualsBuilder().append(actionID, rhs.actionID).isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		return new HashCodeBuilder(503, 513).append(actionID).toHashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("Action: %d", getActionID());
	}

	/**
	 * @return the combatantID
	 */
	public int getCombatantID() {
		return combatantID;
	}

	/**
	 * @return the weaponID
	 */
	public int getWeaponID() {
		return weaponID;
	}

	/**
	 * @return the round
	 */
	public int getRound() {
		return round;
	}

	/**
	 * @return the initiative
	 */
	public int getInitiative() {
		return initiative;
	}

	/**
	 * @return the techniques
	 */
	public Set<Technique> getTechniques() {
		return techniques;
	}

	/**
	 * Adds the specified technique to this action
	 * 
	 * @param technique
	 */
	public void addTechnique(Technique technique) {
		this.techniques.add(technique);
	}

	/**
	 * Removes the specified technique from this action. Defers to the
	 * {@code Technique} equals method
	 * 
	 * @param technique
	 */
	public void removeTechnique(Technique technique) {
		this.removeTechnique(technique);
	}

	/**
	 * Sets the weapon that will be used for the action
	 * 
	 * @param weapon
	 */
	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}

	/**
	 * @return the weapon
	 */
	public Weapon getWeapon() {
		return weapon;
	}

	/**
	 * Sets the initiative for this action, which determines when it will occur
	 * 
	 * @param initiative
	 */
	public void setInitiative(int initiative) {
		this.initiative = initiative;
	}

	/**
	 * @return the total number of power points used to perform this action
	 */
	public int getPowerCost() {
		int powerCost = 0;
		for (Technique technique : this.techniques) {
			powerCost += technique.getPowerCost();
		}
		return powerCost;
	}

	/**
	 * @return the total number of accuracy points used to perform this action
	 */
	public int getAccuracyCost() {
		int accuracyCost = 0;
		for (Technique technique : this.techniques) {
			accuracyCost += technique.getAccuracyCost();
		}
		return accuracyCost;
	}

	/**
	 * @return the total number of finesse points used to perform this action
	 */
	public int getFinesseCost() {
		int finesseCost = 0;
		for (Technique technique : this.techniques) {
			finesseCost += technique.getFinesseCost();
		}
		return finesseCost;
	}

	/**
	 * @return the total speed of this action, including all modifications
	 */
	public int getSpeed() {
		int speed = (weapon != null) ? weapon.getSpeed() : 4;
		for (Technique technique : this.techniques) {
			speed += technique.getSpeedBonus();
		}
		return speed;
	}

	/**
	 * @return the total hit bonus of this action, including all modifications
	 */
	public int getHitBonus() {
		int hitBonus = (weapon != null) ? weapon.getHitBonus() : 0;
		for (Technique technique : this.techniques) {
			hitBonus += technique.getHitBonus();
		}
		return hitBonus;
	}

	/**
	 * @return the total damage bonus of this action, including all
	 *         modifications
	 */
	public int getDamageBonus() {
		int damageBonus = 0;
		for (Technique technique : this.techniques) {
			damageBonus += technique.getDamageBonus();
		}
		return damageBonus;
	}

	/**
	 * @return the total pierce of this action, including all modifications
	 */
	public int getPierceBonus() {
		int pierceBonus = (weapon != null) ? weapon.getPierce() : 0;
		for (Technique technique : this.techniques) {
			pierceBonus += technique.getPierceBonus();
		}
		return pierceBonus;
	}

	/**
	 * @return the total evade of this action, including all modifications
	 */
	public int getEvadeBonus() {
		int evadeBonus = 0;
		for (Technique technique : this.techniques) {
			evadeBonus += technique.getEvadeBonus();
		}
		return evadeBonus;
	}

	/**
	 * @return the total speed of this action, including all modifications
	 */
	public int getDamageReductionBonus() {
		int damageReductionBonus = 0;
		for (Technique technique : this.techniques) {
			damageReductionBonus += technique.getDamageReductionBonus();
		}
		return damageReductionBonus;
	}

	/**
	 * @return the total resistance bonus of this action, including all
	 *         modifications
	 */
	public int getResistanceBonus() {
		int resistanceBonus = 0;
		for (Technique technique : this.techniques) {
			resistanceBonus += technique.getResistanceBonus();
		}
		return resistanceBonus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Action otherAction) {
		Integer otherActionRound = otherAction.getRound();
		Integer actionRound = round;
		return otherActionRound.compareTo(actionRound);
	}
}