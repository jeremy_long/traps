package traps.api;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * A data object that represents a character class, including all the bonuses
 * and modifiers associated with it.
 * 
 * @author jeremy.long
 * 
 */
@JsonPropertyOrder({ "characterClassID", "characterClassName", "hpLevelBonus",
		"epLevelBonus", "strengthInitialBonus", "dexterityInitialBonus",
		"agilityInitialBonus", "healthInitialBonus", "cunningInitialBonus",
		"willpowerInitialBonus", "intelligenceInitialBonus",
		"attributeInitialBonus", "hpInitialBonus", "epInitialBonus",
		"skillInitialBonus", "passiveInitialBonus" })
public class CharacterClass {

	private final int characterClassID;

	@NotBlank
	private final String characterClassName;
	private int hpLevelBonus, epLevelBonus, strengthInitialBonus,
			dexterityInitialBonus, agilityInitialBonus, healthInitialBonus,
			cunningInitialBonus, willpowerInitialBonus,
			intelligenceInitialBonus, attributeInitialBonus, hpInitialBonus,
			epInitialBonus, skillInitialBonus, passiveInitialBonus;

	@JsonCreator
	public CharacterClass(
			@JsonProperty("characterClassID") int characterClassID,
			@JsonProperty("characterClassName") String characterClassName,
			@JsonProperty("hpLevelBonus") int hpLevelBonus,
			@JsonProperty("epLevelBonus") int epLevelBonus,
			@JsonProperty("strengthInitialBonus") int strengthInitialBonus,
			@JsonProperty("dexterityInitialBonus") int dexterityInitialBonus,
			@JsonProperty("agilityInitialBonus") int agilityInitialBonus,
			@JsonProperty("healthInitialBonus") int healthInitialBonus,
			@JsonProperty("cunningInitialBonus") int cunningInitialBonus,
			@JsonProperty("willpowerInitialBonus") int willpowerInitialBonus,
			@JsonProperty("intelligenceInitialBonus") int intelligenceInitialBonus,
			@JsonProperty("attributeInitialBonus") int attributeInitialBonus,
			@JsonProperty("hpInitialBonus") int hpInitialBonus,
			@JsonProperty("epInitialBonus") int epInitialBonus,
			@JsonProperty("skillInitialBonus") int skillInitialBonus,
			@JsonProperty("passiveInitialBonus") int passiveInitialBonus) {
		this.characterClassID = characterClassID;
		this.characterClassName = characterClassName;
		this.hpLevelBonus = hpLevelBonus;
		this.epLevelBonus = epLevelBonus;
		this.strengthInitialBonus = strengthInitialBonus;
		this.dexterityInitialBonus = dexterityInitialBonus;
		this.agilityInitialBonus = agilityInitialBonus;
		this.healthInitialBonus = healthInitialBonus;
		this.cunningInitialBonus = cunningInitialBonus;
		this.willpowerInitialBonus = willpowerInitialBonus;
		this.healthInitialBonus = healthInitialBonus;
		this.intelligenceInitialBonus = intelligenceInitialBonus;
		this.attributeInitialBonus = attributeInitialBonus;
		this.hpInitialBonus = hpInitialBonus;
		this.epInitialBonus = epInitialBonus;
		this.skillInitialBonus = skillInitialBonus;
		this.passiveInitialBonus = passiveInitialBonus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return characterClassName;
	}

	/**
	 * @return ID of the character class.
	 */
	public int getCharacterClassID() {
		return characterClassID;
	}

	/**
	 * @return name of the character class.
	 */
	public String getCharacterClassName() {
		return characterClassName;
	}

	/**
	 * @return the hpLevelBonus
	 */
	public int getHpLevelBonus() {
		return hpLevelBonus;
	}

	/**
	 * @param hpLevelBonus
	 *            the hpLevelBonus to set
	 */
	public void setHpLevelBonus(int hpLevelBonus) {
		this.hpLevelBonus = hpLevelBonus;
	}

	/**
	 * @return the epLevelBonus
	 */
	public int getEpLevelBonus() {
		return epLevelBonus;
	}

	/**
	 * @param epLevelBonus
	 *            the epLevelBonus to set
	 */
	public void setEpLevelBonus(int epLevelBonus) {
		this.epLevelBonus = epLevelBonus;
	}

	/**
	 * @return the strengthInitialBonus
	 */
	public int getStrengthInitialBonus() {
		return strengthInitialBonus;
	}

	/**
	 * @param strengthInitialBonus
	 *            the strengthInitialBonus to set
	 */
	public void setStrengthInitialBonus(int strengthInitialBonus) {
		this.strengthInitialBonus = strengthInitialBonus;
	}

	/**
	 * @return the dexterityInitialBonus
	 */
	public int getDexterityInitialBonus() {
		return dexterityInitialBonus;
	}

	/**
	 * @param dexterityInitialBonus
	 *            the dexterityInitialBonus to set
	 */
	public void setDexterityInitialBonus(int dexterityInitialBonus) {
		this.dexterityInitialBonus = dexterityInitialBonus;
	}

	/**
	 * @return the agilityInitialBonus
	 */
	public int getAgilityInitialBonus() {
		return agilityInitialBonus;
	}

	/**
	 * @param agilityInitialBonus
	 *            the agilityInitialBonus to set
	 */
	public void setAgilityInitialBonus(int agilityInitialBonus) {
		this.agilityInitialBonus = agilityInitialBonus;
	}

	/**
	 * @return the healthInitialBonus
	 */
	public int getHealthInitialBonus() {
		return healthInitialBonus;
	}

	/**
	 * @param healthInitialBonus
	 *            the healthInitialBonus to set
	 */
	public void setHealthInitialBonus(int healthInitialBonus) {
		this.healthInitialBonus = healthInitialBonus;
	}

	/**
	 * @return the willpowerInitialBonus
	 */
	public int getWillpowerInitialBonus() {
		return willpowerInitialBonus;
	}

	/**
	 * @param willpowerInitialBonus
	 *            the willpowerInitialBonus to set
	 */
	public void setWillpowerInitialBonus(int willpowerInitialBonus) {
		this.willpowerInitialBonus = willpowerInitialBonus;
	}

	/**
	 * @return the intelligenceInitialBonus
	 */
	public int getIntelligenceInitialBonus() {
		return intelligenceInitialBonus;
	}

	/**
	 * @param intelligenceInitialBonus
	 *            the intelligenceInitialBonus to set
	 */
	public void setIntelligenceInitialBonus(int intelligenceInitialBonus) {
		this.intelligenceInitialBonus = intelligenceInitialBonus;
	}

	/**
	 * @return the attributeInitialBonus
	 */
	public int getAttributeInitialBonus() {
		return attributeInitialBonus;
	}

	/**
	 * @param attributeInitialBonus
	 *            the attributeInitialBonus to set
	 */
	public void setAttributeInitialBonus(int attributeInitialBonus) {
		this.attributeInitialBonus = attributeInitialBonus;
	}

	/**
	 * @return the hpInitialBonus
	 */
	public int getHpInitialBonus() {
		return hpInitialBonus;
	}

	/**
	 * @param hpInitialBonus
	 *            the hpInitialBonus to set
	 */
	public void setHpInitialBonus(int hpInitialBonus) {
		this.hpInitialBonus = hpInitialBonus;
	}

	/**
	 * @return the epInitialBonus
	 */
	public int getEpInitialBonus() {
		return epInitialBonus;
	}

	/**
	 * @param epInitialBonus
	 *            the epInitialBonus to set
	 */
	public void setEpInitialBonus(int epInitialBonus) {
		this.epInitialBonus = epInitialBonus;
	}

	/**
	 * @return the skillInitialBonus
	 */
	public int getSkillInitialBonus() {
		return skillInitialBonus;
	}

	/**
	 * @param skillInitialBonus
	 *            the skillInitialBonus to set
	 */
	public void setSkillInitialBonus(int skillInitialBonus) {
		this.skillInitialBonus = skillInitialBonus;
	}

	/**
	 * @return the passiveInitialBonus
	 */
	public int getPassiveInitialBonus() {
		return passiveInitialBonus;
	}

	/**
	 * @param passiveInitialBonus
	 *            the passiveInitialBonus to set
	 */
	public void setPassiveInitialBonus(int passiveInitialBonus) {
		this.passiveInitialBonus = passiveInitialBonus;
	}

	/**
	 * @return the cunningInitialBonus
	 */
	public int getCunningInitialBonus() {
		return cunningInitialBonus;
	}

	/**
	 * @param cunningInitialBonus
	 *            the cunningInitialBonus to set
	 */
	public void setCunningInitialBonus(int cunningInitialBonus) {
		this.cunningInitialBonus = cunningInitialBonus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public final boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof CharacterClass)) {
			return false;
		}
		CharacterClass rhs = (CharacterClass) obj;
		return new EqualsBuilder().append(characterClassID,
				rhs.characterClassID).isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		// pick hard-coded, randomly chosen prime numbers, ideally different for
		// each class
		return new HashCodeBuilder(111, 179).append(characterClassID)
				.toHashCode();
	}
}
