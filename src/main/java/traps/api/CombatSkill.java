package traps.api;

import java.util.Arrays;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.yammer.dropwizard.validation.InvalidEntityException;

/**
 * Data object that represents a Combat Skill.
 * 
 * @author jeremy.long
 * 
 */
@JsonPropertyOrder({ "combatSkillID", "combatSkillName", "powerRank",
		"accuracyRank", "finesseRank", "updated" })
public class CombatSkill {

	public enum Rank {
		A, B, C;
	}

	private final int combatSkillID;

	@NotBlank
	private final String combatSkillName;
	private Rank powerRank, accuracyRank, finesseRank;

	private DateTime updated;

	@JsonCreator
	public CombatSkill(@JsonProperty("combatSkillID") int combatSkillID,
			@JsonProperty("combatSkillName") String combatSkillName,
			@JsonProperty("powerRank") String powerRank,
			@JsonProperty("accuracyRank") String accuracyRank,
			@JsonProperty("finesseRank") String finesseRank) {
		this.combatSkillID = combatSkillID;
		this.combatSkillName = combatSkillName;

		try {
			this.powerRank = Rank.valueOf(powerRank);
			this.accuracyRank = Rank.valueOf(accuracyRank);
			this.finesseRank = Rank.valueOf(finesseRank);
		} catch (IllegalArgumentException e) {
			throw new InvalidEntityException(
					"The request entity had the following errors:",
					Arrays.asList(String
							.format("Combat skill ranks must be valid.")));
		}
	}

	/**
	 * @return the combatSkillID
	 */
	public int getCombatSkillID() {
		return combatSkillID;
	}

	/**
	 * @return the combatSkillName
	 */
	public String getCombatSkillName() {
		return combatSkillName;
	}

	/**
	 * @return the powerRank
	 */
	public Rank getPowerRank() {
		return powerRank;
	}

	/**
	 * @return the accuracyRank
	 */
	public Rank getAccuracyRank() {
		return accuracyRank;
	}

	/**
	 * @return the finesseRank
	 */
	public Rank getFinesseRank() {
		return finesseRank;
	}

	/**
	 * @return the updated
	 */
	public DateTime getUpdated() {
		return updated;
	}

	@Override
	public String toString() {
		return String.format("%s (Power: %s Accuracy: %s, Finesse: %s)",
				getCombatSkillName(), getPowerRank(), getAccuracyRank(),
				getFinesseRank());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public final boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof CombatSkill)) {
			return false;
		}
		CombatSkill rhs = (CombatSkill) obj;
		return new EqualsBuilder().append(combatSkillID, rhs.combatSkillID)
				.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		// pick hard-coded, randomly chosen prime numbers, ideally different for
		// each class
		return new HashCodeBuilder(117, 137).append(combatSkillID).toHashCode();
	}
}
