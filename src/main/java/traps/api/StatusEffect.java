package traps.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Data object that represents a Status Effect. It encapsulates a map that is
 * keyed by the affected attribute, and contains the values that will be used to
 * modify the corresponding attribute.
 * 
 * @author jeremy.long
 * 
 */
@JsonPropertyOrder({ "statusEffectID", "statusEffectName",
		"statusEffectDescription", "attributeEffects", "modifier",
		"roundInflicted", "persistent" })
public class StatusEffect {

	private final int statusEffectID;

	@NotBlank
	private final String statusEffectName;
	private final String statusEffectDescription;
	private final int modifier, roundInflicted;
	private final boolean persistent;

	@JsonInclude(Include.NON_EMPTY)
	private final Map<String, Integer> attributeEffects;

	@JsonCreator
	public StatusEffect(
			@JsonProperty("statusEffectID") int statusEffectID,
			@JsonProperty("statusEffectName") String statusEffectName,
			@JsonProperty("statusEffectDescription") String statusEffectDescription,
			@JsonProperty("modifier") int modifier,
			@JsonProperty("roundInflicted") int roundInflicted,
			@JsonProperty("persistent") boolean persistent) {
		this.statusEffectID = statusEffectID;
		this.statusEffectName = statusEffectName;
		this.statusEffectDescription = statusEffectDescription;
		this.modifier = modifier;
		this.roundInflicted = roundInflicted;
		this.persistent = persistent;
		this.attributeEffects = new HashMap<String, Integer>();
	}

	/**
	 * @return the statusEffectID
	 */
	public int getStatusEffectID() {
		return statusEffectID;
	}

	/**
	 * @return the statusEffectName
	 */
	public String getStatusEffectName() {
		return statusEffectName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public final boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof StatusEffect)) {
			return false;
		}
		StatusEffect rhs = (StatusEffect) obj;
		return new EqualsBuilder().append(statusEffectID, rhs.statusEffectID)
				.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		// pick hard-coded, randomly chosen prime numbers, ideally different for
		// each class
		return new HashCodeBuilder(703, 713).append(statusEffectID)
				.toHashCode();
	}

	@Override
	public String toString() {
		return String
				.format("%s, Description: %s, Effects: %s, Round inflicted: %d, Modifier: %d",
						getStatusEffectName(), getStatusEffectDescription(),
						getAttributeEffects(), getRoundInflicted(),
						getModifier());
	}

	/**
	 * @return the statusEffectDescription
	 */
	public String getStatusEffectDescription() {
		return statusEffectDescription;
	}

	/**
	 * @return the attributeEffects
	 */
	public Map<String, Integer> getAttributeEffects() {
		return attributeEffects;
	}

	public void addAttributeEffect(String attribute, int effect) {
		attributeEffects.put(attribute, effect);
	}

	/**
	 * @return the modifier
	 */
	public int getModifier() {
		return modifier;
	}

	/**
	 * @return the roundInflicted
	 */
	public int getRoundInflicted() {
		return roundInflicted;
	}

	/**
	 * @return the persistent
	 */
	public boolean getPersistent() {
		return persistent;
	}
}