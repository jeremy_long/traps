package traps.api;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Represents an Encounter, which encapsulates a collection of {@code Combatant}
 * s and their interactions.
 * 
 * @author jeremy.long
 * 
 */
@JsonPropertyOrder({ "encounterID", "encounterName", "currentRound",
		"combatants", "updated" })
public class Encounter {

	private final int encounterID;

	@NotBlank
	private final String encounterName;
	private final Set<Combatant> combatants;
	private int currentRound;

	private DateTime updated;

	@JsonCreator
	public Encounter(@JsonProperty("encounterID") int encounterID,
			@JsonProperty("encounterName") String encounterName,
			@JsonProperty("currentRound") int currentRound) {
		this.encounterID = encounterID;
		this.encounterName = encounterName;
		this.currentRound = currentRound;

		this.combatants = new LinkedHashSet<Combatant>();
	}

	/**
	 * @return the encounterID
	 */
	public int getEncounterID() {
		return encounterID;
	}

	/**
	 * @return the encounterName
	 */
	public String getEncounterName() {
		return encounterName;
	}

	/**
	 * @return the updated
	 */
	public DateTime getUpdated() {
		return updated;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public final boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Encounter)) {
			return false;
		}
		Encounter rhs = (Encounter) obj;
		return new EqualsBuilder().append(encounterID, rhs.encounterID)
				.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		// pick hard-coded, randomly chosen prime numbers, ideally different for
		// each class
		return new HashCodeBuilder(303, 313).append(encounterID).toHashCode();
	}

	@Override
	public String toString() {
		return String.format("%s", getEncounterName());
	}

	/**
	 * @return the currentRound
	 */
	public int getCurrentRound() {
		return currentRound;
	}

	public void setUpdated(DateTime updated) {
		this.updated = updated;
	}

	public void setCurrentRound(int currentRound) {
		this.currentRound = currentRound;
	}

	public Set<Combatant> getCombatants() {
		return this.combatants;
	}

	public void addCombatant(Combatant combatant) {
		this.combatants.add(combatant);
	}

	public void removeCombatant(Combatant combatant) {
		this.combatants.remove(combatant);
	}

	public void sortCombatants() {
		Combatant[] combatantsArray = new Combatant[combatants.size()];
		combatants.toArray(combatantsArray);
		combatants.clear();

		Arrays.sort(combatantsArray);
		for (Combatant combatant : combatantsArray) {
			addCombatant(combatant);
		}
	}
}