package traps.resources;

import java.util.Set;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import traps.api.Action;
import traps.api.Combatant;
import traps.api.Technique;
import traps.api.Weapon;
import traps.dao.ActionDAO;
import traps.dao.CombatantDAO;
import traps.dao.WeaponDAO;
import traps.util.RollUtils;

import com.google.common.collect.Sets;

@Path("/actions")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ActionResource {

	private final ActionDAO actionDAO;
	private final CombatantDAO combatantDAO;
	private final WeaponDAO weaponDAO;

	public ActionResource(ActionDAO actionDAO, CombatantDAO combatantDAO,
			WeaponDAO weaponDAO) {
		this.actionDAO = actionDAO;
		this.combatantDAO = combatantDAO;
		this.weaponDAO = weaponDAO;
	}

	/**
	 * @return a set of all character actions in the system in json form, or an
	 *         empty set if there are none.
	 */
	@GET
	public Set<Action> getAll() {
		return actionDAO.getAll();
	}

	@GET
	@Path("{id}")
	public Response getByID(@PathParam("id") int actionID) {
		Action action = actionDAO.getByID(actionID).iterator().next();
		if (action == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.status(Status.OK).entity(action).build();
	}

	/**
	 * @param newAction
	 *            a new Action.
	 * @return 201 if created, error otherwise
	 */
	@POST
	public Response add(@Valid Action newAction) {
		try {
			int actionID = actionDAO.add(newAction);

			// Add techniques.
			for (Technique newTechnique : newAction.getTechniques()) {
				actionDAO.addTechnique(actionID, newTechnique.getTechniqueID());
			}

			return Response.status(Status.CREATED)
					.entity(actionDAO.getByID(actionID)).build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e).build();
		}
	}

	@PUT
	@Path("{id}")
	public Response update(@Valid Action updatedAction) {
		Action existingAction = actionDAO.getByID(updatedAction.getActionID())
				.iterator().next();
		if (existingAction == null) {
			return Response.status(Status.NOT_FOUND).build();
		}

		Combatant currentCombatant = combatantDAO
				.getByID(updatedAction.getCombatantID()).iterator().next();
		Weapon currentWeapon = weaponDAO.getByID(updatedAction.getWeaponID());
		updatedAction.setWeapon(currentWeapon);
		currentCombatant.switchWeapons(existingAction.getWeapon(),
				currentWeapon);

		updatedAction.setInitiative(RollUtils.rollInitiative()
				+ updatedAction.getSpeed());
		actionDAO.update(updatedAction);

		// Update techniques.
		for (Technique newTechnique : Sets.difference(
				updatedAction.getTechniques(), existingAction.getTechniques())) {
			actionDAO.addTechnique(updatedAction.getActionID(),
					newTechnique.getTechniqueID());
			currentCombatant.use(newTechnique);
		}
		for (Technique removedTechnique : Sets.difference(
				existingAction.getTechniques(), updatedAction.getTechniques())) {
			actionDAO.removeTechnique(updatedAction.getActionID(),
					removedTechnique.getTechniqueID());
			currentCombatant.cancel(removedTechnique);
		}

		combatantDAO.update(currentCombatant);

		return Response.status(Status.OK)
				.entity(actionDAO.getByID(updatedAction.getActionID())).build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int actionID) {
		if (actionDAO.getByID(actionID) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		actionDAO.deleteByID(actionID);
		return Response.status(Status.OK).build();
	}
}