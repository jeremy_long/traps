package traps.resources;

import java.util.Set;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import traps.api.Armor;
import traps.dao.ArmorDAO;

@Path("/armor")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ArmorResource {

	private final ArmorDAO armorDAO;

	public ArmorResource(ArmorDAO armorDAO) {
		this.armorDAO = armorDAO;
	}

	/**
	 * @return a set of all character armors in the system in json form, or an
	 *         empty set if there are none.
	 */
	@GET
	public Set<Armor> getAll() {
		return armorDAO.getAll();
	}

	@GET
	@Path("{id}")
	public Response getByID(@PathParam("id") int armorID) {
		Armor armor = armorDAO.getByID(armorID);
		if (armor == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.status(Status.OK).entity(armor).build();
	}

	/**
	 * @param newArmor
	 *            a new Armor.
	 * @return 201 if created, error otherwise
	 */
	@POST
	public Response add(@Valid Armor newArmor) {
		try {
			int armorID = armorDAO.add(newArmor);
			return Response.status(Status.CREATED)
					.entity(armorDAO.getByID(armorID)).build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e).build();
		}
	}

	@PUT
	@Path("{id}")
	public Response update(@Valid Armor updatedArmor) {
		if (armorDAO.getByID(updatedArmor.getArmorID()) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		armorDAO.update(updatedArmor);
		return Response.status(Status.OK)
				.entity(armorDAO.getByID(updatedArmor.getArmorID())).build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int armorID) {
		if (armorDAO.getByID(armorID) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		armorDAO.deleteByID(armorID);
		return Response.status(Status.OK).build();
	}
}