package traps.resources;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import traps.api.StatusEffect;
import traps.dao.StatusEffectDAO;

import com.google.common.collect.MapDifference;
import com.google.common.collect.MapDifference.ValueDifference;
import com.google.common.collect.Maps;

@Path("/statusEffects")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StatusEffectResource {

	private final StatusEffectDAO statusEffectDAO;

	public StatusEffectResource(StatusEffectDAO statusEffectDAO) {
		this.statusEffectDAO = statusEffectDAO;
	}

	/**
	 * @return a set of all character statusEffects in the system in json form,
	 *         or an empty set if there are none.
	 */
	@GET
	public Set<StatusEffect> getAll() {
		return statusEffectDAO.getAll();
	}

	@GET
	@Path("{id}")
	public Response getByID(@PathParam("id") int statusEffectID) {
		if (!statusEffectDAO.getByID(statusEffectID).iterator().hasNext()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response
				.status(Status.OK)
				.entity(statusEffectDAO.getByID(statusEffectID).iterator()
						.next()).build();
	}

	/**
	 * @param newStatusEffect
	 *            a new StatusEffect.
	 * @return 201 if created, error otherwise
	 */
	@POST
	public Response add(@Valid StatusEffect newStatusEffect) {
		try {
			int statusEffectID = statusEffectDAO.add(newStatusEffect);
			return Response
					.status(Status.CREATED)
					.entity(statusEffectDAO.getByID(statusEffectID).iterator()
							.next()).build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e).build();
		}
	}

	@PUT
	@Path("{id}")
	public Response update(@Valid StatusEffect updatedStatusEffect) {
		if (!statusEffectDAO.getByID(updatedStatusEffect.getStatusEffectID())
				.iterator().hasNext()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		StatusEffect existingStatusEffect = statusEffectDAO
				.getByID(updatedStatusEffect.getStatusEffectID()).iterator()
				.next();
		statusEffectDAO.update(updatedStatusEffect);

		MapDifference<String, Integer> differenceMap = Maps.difference(
				updatedStatusEffect.getAttributeEffects(),
				existingStatusEffect.getAttributeEffects());
		Collection<String> effectsToRemove = differenceMap.entriesOnlyOnRight()
				.keySet();
		Collection<Entry<String, Integer>> effectsToAdd = differenceMap
				.entriesOnlyOnLeft().entrySet();
		Map<String, ValueDifference<Integer>> changedEffects = differenceMap
				.entriesDiffering();

		for (Entry<String, ValueDifference<Integer>> differentEffect : changedEffects
				.entrySet()) {
			statusEffectDAO.removeEffectModifier(
					updatedStatusEffect.getStatusEffectID(),
					differentEffect.getKey());
			statusEffectDAO.addEffectModifier(updatedStatusEffect
					.getStatusEffectID(), differentEffect.getKey(),
					differentEffect.getValue().leftValue());
		}
		for (String statusEffect : effectsToRemove) {
			statusEffectDAO.removeEffectModifier(
					updatedStatusEffect.getStatusEffectID(), statusEffect);
		}
		for (Entry<String, Integer> statusEffect : effectsToAdd) {
			statusEffectDAO.addEffectModifier(
					updatedStatusEffect.getStatusEffectID(),
					statusEffect.getKey(), statusEffect.getValue());
		}

		return Response
				.status(Status.OK)
				.entity(statusEffectDAO
						.getByID(updatedStatusEffect.getStatusEffectID())
						.iterator().next()).build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int statusEffectID) {
		if (!statusEffectDAO.getByID(statusEffectID).iterator().hasNext()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		statusEffectDAO.deleteByID(statusEffectID);
		return Response.status(Status.OK).build();
	}
}