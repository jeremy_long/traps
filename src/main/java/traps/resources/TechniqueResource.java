package traps.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import traps.api.Technique;
import traps.dao.TechniqueDAO;

@Path("/techniques")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TechniqueResource {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	private final TechniqueDAO techniqueDAO;

	public TechniqueResource(TechniqueDAO techniqueDAO) {
		this.techniqueDAO = techniqueDAO;
	}

	/**
	 * @return a set of all character techniques in the system in json form, or
	 *         an empty set if there are none.
	 */
	@GET
	public Set<Technique> getAll() {
		return techniqueDAO.getAll();
	}

	@GET
	@Path("{id}")
	public Response getByID(@PathParam("id") int techniqueID) {
		Technique technique = techniqueDAO.getByID(techniqueID);
		if (technique == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.status(Status.OK).entity(technique).build();
	}

	/**
	 * @param newTechnique
	 *            a new Technique.
	 * @return 201 if created, error otherwise
	 */
	@POST
	public Response add(@Valid Technique newTechnique) {
		try {
			int techniqueID = techniqueDAO.add(newTechnique);
			return Response.status(Status.CREATED)
					.entity(techniqueDAO.getByID(techniqueID)).build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e).build();
		}
	}

	@PUT
	@Path("{id}")
	public Response update(@Valid Technique updatedTechnique) {
		if (techniqueDAO.getByID(updatedTechnique.getTechniqueID()) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		techniqueDAO.update(updatedTechnique);
		return Response
				.status(Status.OK)
				.entity(techniqueDAO.getByID(updatedTechnique.getTechniqueID()))
				.build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int techniqueID) {
		if (techniqueDAO.getByID(techniqueID) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		techniqueDAO.deleteByID(techniqueID);
		return Response.status(Status.OK).build();
	}

	@POST
	@Path("csv")
	public Response postCSV(@Valid CSVFile csvFile) {
		List<Technique> techniquesToAdd = new ArrayList<Technique>();
		Scanner scanner = null;
		try {
			scanner = new Scanner(csvFile.getCSV()).useDelimiter("\n");
			while (scanner.hasNext()) {
				String value = scanner.next().trim();
				try {
					if (value.contains(",")) {
						techniquesToAdd.add(parseLine(value));
					}
				} catch (Exception e) {
					LOGGER.error("Error parsing technique CSV for ", value);
				}
			}
			for (Technique technique : techniquesToAdd) {
				if (techniqueDAO.getByName(technique.getTechniqueName()) == null) {
					techniqueDAO.add(technique);
				}
			}
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
		return (techniquesToAdd.size() > 0) ? Response.status(Status.CREATED)
				.build() : Response.status(Status.BAD_REQUEST).build();
	}

	private Technique parseLine(String csvLine) {
		String[] tokens = csvLine.split(",");

		List<String> tokenList = new ArrayList<String>();
		for (int i = 0; i < tokens.length; i++) {
			String currentCell = tokens[i];
			while (currentCell.startsWith("\"")
					&& currentCell.indexOf("\"", 1) == -1) {
				currentCell += ", " + tokens[++i];
			}
			currentCell.replaceAll("\"", "");
			tokenList.add(currentCell.trim());
		}

		int i = 0;
		return new Technique(0, tokenList.get(i++),
				getCSVIntValue(tokenList.get(i++)),
				getCSVIntValue(tokenList.get(i++)),
				getCSVIntValue(tokenList.get(i++)),
				getCSVIntValue(tokenList.get(i++)),
				getCSVIntValue(tokenList.get(i++)),
				getCSVIntValue(tokenList.get(i++)),
				getCSVIntValue(tokenList.get(i++)),
				getCSVIntValue(tokenList.get(i++)),
				getCSVIntValue(tokenList.get(i++)),
				getCSVIntValue(tokenList.get(i++)), tokenList.get(i++),
				tokenList.get(i++), getCSVIntValue(tokenList.get(i++)));
	}

	private int getCSVIntValue(String token) {
		return (token.length() > 0) ? Integer.parseInt(token) : 0;
	}

	private static class CSVFile {
		@NotBlank
		String csv;

		public String getCSV() {
			return this.csv;
		}
	}

}