package traps.resources;

import java.util.Iterator;
import java.util.Set;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import traps.api.Character;
import traps.api.CharacterClass;
import traps.dao.CharacterClassDAO;
import traps.dao.CharacterDAO;

@Path("/classes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CharacterClassResource {

	private final CharacterClassDAO classDAO;
	private final CharacterDAO characterDAO;

	public CharacterClassResource(CharacterClassDAO classDAO,
			CharacterDAO characterDAO) {
		this.classDAO = classDAO;
		this.characterDAO = characterDAO;
	}

	/**
	 * @return a set of all character classes in the system in json form, or an
	 *         empty set if there are none.
	 */
	@GET
	public Set<CharacterClass> getAll() {
		return classDAO.getAll();
	}

	@GET
	@Path("{id}")
	public Response getByID(@PathParam("id") int classID) {
		CharacterClass characterClass = classDAO.getByID(classID);
		if (characterClass == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.status(Status.OK).entity(characterClass).build();
	}

	/**
	 * @param newClass
	 *            a new CharacterClass.
	 * @return 201 if created, error otherwise
	 */
	@POST
	public Response add(@Valid CharacterClass newClass) {
		try {
			int classID = classDAO.add(newClass);
			return Response.status(Status.CREATED)
					.entity(classDAO.getByID(classID)).build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e).build();
		}
	}

	@PUT
	@Path("{id}")
	public Response update(@Valid CharacterClass updatedClass) {
		if (classDAO.getByID(updatedClass.getCharacterClassID()) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}

		// Grab all characters with this class.
		Set<Character> characters = characterDAO.getAllByClassID(updatedClass
				.getCharacterClassID());
		Iterator<Character> iterator = characters.iterator();
		while (iterator.hasNext()) {
			Character currentCharacter = iterator.next();
			currentCharacter.setCharacterClass(updatedClass);
			characterDAO.update(currentCharacter);
		}

		classDAO.update(updatedClass);
		return Response.status(Status.OK)
				.entity(classDAO.getByID(updatedClass.getCharacterClassID()))
				.build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int characterClassID) {
		if (classDAO.getByID(characterClassID) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		classDAO.deleteByID(characterClassID);
		return Response.status(Status.OK).build();
	}
}