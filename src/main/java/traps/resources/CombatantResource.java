package traps.resources;

import java.util.Iterator;
import java.util.Set;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import traps.api.Action;
import traps.api.Character;
import traps.api.Combatant;
import traps.api.StatusEffect;
import traps.api.Weapon;
import traps.dao.ActionDAO;
import traps.dao.CharacterDAO;
import traps.dao.CombatantDAO;
import traps.dao.EncounterDAO;
import traps.util.RollUtils;

import com.google.common.collect.Sets;

@Path("/combatants")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CombatantResource {

	private final CombatantDAO combatantDAO;
	private final CharacterDAO characterDAO;
	private final ActionDAO actionDAO;
	private final EncounterDAO encounterDAO;

	public CombatantResource(CombatantDAO combatantDAO,
			CharacterDAO characterDAO, ActionDAO actionDAO,
			EncounterDAO encounterDAO) {
		this.combatantDAO = combatantDAO;
		this.characterDAO = characterDAO;
		this.actionDAO = actionDAO;
		this.encounterDAO = encounterDAO;
	}

	/**
	 * @return a set of all character combatants in the system in json form, or
	 *         an empty set if there are none.
	 */
	@GET
	public Set<Combatant> getAll() {
		Set<Combatant> combatants = combatantDAO.getAll();
		for (Combatant combatant : combatants) {
			Set<Action> actions = actionDAO.getByCombatantID(combatant
					.getCombatantID());
			for (Action action : actions) {
				combatant.addAction(action);
			}
		}
		return combatants;
	}

	@GET
	@Path("{id}")
	public Response getByID(@PathParam("id") int combatantID) {
		Iterator<Combatant> combatantIterator = combatantDAO.getByID(
				combatantID).iterator();
		if (combatantIterator == null || !combatantIterator.hasNext()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		Combatant combatant = combatantIterator.next();
		Set<Action> actions = actionDAO.getByCombatantID(combatant
				.getCombatantID());
		for (Action action : actions) {
			combatant.addAction(action);
		}
		return Response.status(Status.OK).entity(combatant).build();
	}

	/**
	 * @param newCombatant
	 *            a new Combatant.
	 * @return 201 if created, error otherwise
	 */
	@POST
	public Response add(@Valid Combatant newCombatant) {
		try {
			Character character = characterDAO
					.getByID(newCombatant.getCharacter().getCharacterID())
					.iterator().next();
			if (character == null) {
				return Response.status(Status.NOT_FOUND).build();
			}
			newCombatant.setCharacter(character);
			newCombatant.initialize();
			int combatantID = combatantDAO.add(newCombatant);

			// Create a new action for this character when they join the
			// encounter -- note that their action
			// will be deleted if they are removed by the Cascade on the
			// relevant tables.
			int currentRound = encounterDAO.getCurrentRoundByID(newCombatant
					.getEncounterID());
			Weapon defaultWeapon = newCombatant.getCharacter().getWeapons()
					.iterator().next();
			int speed = RollUtils.rollInitiative() + defaultWeapon.getSpeed();
			Action action = new Action(0, combatantID,
					defaultWeapon.getWeaponID(), currentRound, speed);
			int actionID = actionDAO.add(action);

			newCombatant.addAction(actionDAO.getByID(actionID).iterator()
					.next());

			return Response.status(Status.CREATED)
					.entity(combatantDAO.getByID(combatantID)).build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e).build();
		}
	}

	@PUT
	@Path("{id}")
	public Response update(@Valid Combatant updatedCombatant) {
		if (combatantDAO.getByID(updatedCombatant.getCombatantID()) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		Combatant existingCombatant = combatantDAO
				.getByID(updatedCombatant.getCombatantID()).iterator().next();
		combatantDAO.update(updatedCombatant);

		updatedCombatant.sortActions();
		int currentRound = 1;
		if (updatedCombatant.getActions().size() > 1) {
			currentRound = updatedCombatant.getActions().iterator().next()
					.getRound() - 1;
		}

		// Update status effects.
		for (StatusEffect newStatusEffect : Sets.difference(
				updatedCombatant.getStatusEffects(),
				existingCombatant.getStatusEffects())) {
			combatantDAO.addStatusEffect(updatedCombatant.getCombatantID(),
					newStatusEffect.getStatusEffectID(), currentRound,
					newStatusEffect.getModifier());

			// TODO: Better winded treatment
			if (newStatusEffect.getAttributeEffects().containsKey("Speed")) {
				Action currentAction = updatedCombatant.getActions().iterator()
						.next();
				currentAction.setInitiative(currentAction.getInitiative()
						+ newStatusEffect.getAttributeEffects().get("Speed"));
				actionDAO.update(currentAction);
			}
		}
		for (StatusEffect removedStatusEffect : Sets.difference(
				existingCombatant.getStatusEffects(),
				updatedCombatant.getStatusEffects())) {
			combatantDAO.removeStatusEffect(updatedCombatant.getCombatantID(),
					removedStatusEffect.getStatusEffectID());

			// TODO: Better winded treatment
			if (removedStatusEffect.getAttributeEffects().containsKey("Speed")) {
				Action currentAction = updatedCombatant.getActions().iterator()
						.next();
				currentAction.setInitiative(currentAction.getInitiative()
						- removedStatusEffect.getAttributeEffects()
								.get("Speed"));
				actionDAO.update(currentAction);
			}
		}

		return Response
				.status(Status.OK)
				.entity(combatantDAO.getByID(updatedCombatant.getCombatantID()))
				.build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int combatantID) {
		if (combatantDAO.getByID(combatantID) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		combatantDAO.deleteByID(combatantID);
		return Response.status(Status.OK).build();
	}
}