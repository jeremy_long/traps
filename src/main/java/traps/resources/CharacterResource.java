package traps.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import traps.api.Armor;
import traps.api.Armor.ArmorType;
import traps.api.Character;
import traps.api.CharacterClass;
import traps.api.Weapon;
import traps.dao.CharacterClassDAO;
import traps.dao.CharacterDAO;

import com.google.common.collect.MapDifference;
import com.google.common.collect.MapDifference.ValueDifference;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

@Path("/characters")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CharacterResource {

	private final CharacterDAO characterDAO;
	private final CharacterClassDAO classDAO;

	public CharacterResource(CharacterDAO characterDAO,
			CharacterClassDAO classDAO) {
		this.characterDAO = characterDAO;
		this.classDAO = classDAO;
	}

	/**
	 * @return a set of all character characters in the system in json form, or
	 *         an empty set if there are none.
	 */
	@GET
	public Set<Character> getAll() {
		return characterDAO.getAll();
	}

	@GET
	@Path("{id}")
	public Response getByID(@PathParam("id") int characterID) {
		Iterator<Character> character = characterDAO.getByID(characterID)
				.iterator();
		if (!character.hasNext()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.status(Status.OK).entity(character.next()).build();
	}

	/**
	 * @param newCharacter
	 *            a new Character.
	 * @return 201 if created, error otherwise
	 */
	@POST
	public Response add(@Valid Character newCharacter) {
		try {
			CharacterClass newCharacterClass = classDAO.getByID(newCharacter
					.getCharacterClass().getCharacterClassID());
			newCharacter.setCharacterClass(newCharacterClass);

			for (int level = 1; level < newCharacter.getLevel(); level++) {
				newCharacter.levelUp();
			}

			int characterID = characterDAO.add(newCharacter);
			return Response
					.status(Status.CREATED)
					.entity(characterDAO.getByID(characterID).iterator().next())
					.build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e).build();
		}
	}

	@PUT
	@Path("{id}")
	public Response update(@Valid Character updatedCharacter) {
		Iterator<Character> iterator = characterDAO.getByID(
				updatedCharacter.getCharacterID()).iterator();
		if (!iterator.hasNext()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		Character existingCharacter = iterator.next();
		int currentLevel = existingCharacter.getLevel();

		CharacterClass newCharacterClass = classDAO.getByID(updatedCharacter
				.getCharacterClass().getCharacterClassID());
		if (!newCharacterClass.equals(existingCharacter.getCharacterClass())) {
			updatedCharacter.setCharacterClass(newCharacterClass);
		}

		if (existingCharacter.getLevel() > updatedCharacter.getLevel()) {
			updatedCharacter.makeLevelOne();
			currentLevel = 1;
		}

		for (; currentLevel < updatedCharacter.getLevel(); currentLevel++) {
			updatedCharacter.levelUp();
		}

		characterDAO.update(updatedCharacter);

		// Update weapons.
		for (Weapon newWeapon : Sets.difference(updatedCharacter.getWeapons(),
				existingCharacter.getWeapons())) {
			characterDAO.addWeapon(updatedCharacter.getCharacterID(),
					newWeapon.getWeaponID());
		}
		for (Weapon removedWeapon : Sets.difference(
				existingCharacter.getWeapons(), updatedCharacter.getWeapons())) {
			characterDAO.removeWeapon(updatedCharacter.getCharacterID(),
					removedWeapon.getWeaponID());
		}

		// Update armor.
		MapDifference<ArmorType, Armor> differenceMap = Maps.difference(
				updatedCharacter.getArmor(), existingCharacter.getArmor());
		Collection<Armor> armorToRemove = differenceMap.entriesOnlyOnRight()
				.values();
		Collection<Armor> armorToAdd = differenceMap.entriesOnlyOnLeft()
				.values();
		Map<ArmorType, ValueDifference<Armor>> changedArmor = differenceMap
				.entriesDiffering();

		for (ValueDifference<Armor> differentArmor : changedArmor.values()) {
			characterDAO.removeArmor(updatedCharacter.getCharacterID(),
					differentArmor.rightValue().getArmorID());
			characterDAO.addArmor(updatedCharacter.getCharacterID(),
					differentArmor.leftValue().getArmorID());
		}

		for (Armor armor : armorToRemove) {
			characterDAO.removeArmor(updatedCharacter.getCharacterID(),
					armor.getArmorID());
		}
		for (Armor armor : armorToAdd) {
			characterDAO.addArmor(updatedCharacter.getCharacterID(),
					armor.getArmorID());
		}

		return Response
				.status(Status.OK)
				.entity(characterDAO.getByID(updatedCharacter.getCharacterID())
						.iterator().next()).build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int characterID) {
		if (characterDAO.getByID(characterID).isEmpty()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		characterDAO.deleteByID(characterID);
		return Response.status(Status.OK).build();
	}

	@GET
	@Path("sheets/{id}")
	public Response writeToFile(@PathParam("id") int characterID)
			throws IOException {
		Iterator<Character> iterator = characterDAO.getByID(characterID)
				.iterator();
		if (!iterator.hasNext()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		Character character = iterator.next();

		PrintStream ps = new PrintStream(new FileOutputStream(new File(
				"src/main/resources/assets/txt/"
						+ character.getCharacterName().replaceAll(" ", "")
						+ ".txt")));
		ps.println(character.toCharacterSheet());
		ps.close();

		return Response.ok().build();
	}
}