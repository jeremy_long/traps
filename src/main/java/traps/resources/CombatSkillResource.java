package traps.resources;

import java.util.Set;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import traps.api.CombatSkill;
import traps.dao.CombatSkillDAO;

@Path("/combatSkills")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CombatSkillResource {

	private final CombatSkillDAO combatSkillDAO;

	public CombatSkillResource(CombatSkillDAO combatSkillDAO) {
		this.combatSkillDAO = combatSkillDAO;
	}

	/**
	 * @return a set of all character combatSkilles in the system in json form,
	 *         or an empty set if there are none.
	 */
	@GET
	public Set<CombatSkill> getAll() {
		return combatSkillDAO.getAll();
	}

	@GET
	@Path("{id}")
	public Response getByID(@PathParam("id") int combatSkillID) {
		CombatSkill combatSkill = combatSkillDAO.getByID(combatSkillID);
		if (combatSkill == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.status(Status.OK).entity(combatSkill).build();
	}

	/**
	 * @param newCombatSkill
	 *            a new CombatSkill.
	 * @return 201 if created, error otherwise
	 */
	@POST
	public Response add(@Valid CombatSkill newCombatSkill) {
		try {
			int combatSkillID = combatSkillDAO.add(newCombatSkill);
			return Response.status(Status.CREATED)
					.entity(combatSkillDAO.getByID(combatSkillID)).build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e).build();
		}
	}

	@PUT
	@Path("{id}")
	public Response update(@Valid CombatSkill updatedCombatSkill) {
		if (combatSkillDAO.getByID(updatedCombatSkill.getCombatSkillID()) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		combatSkillDAO.update(updatedCombatSkill);
		return Response
				.status(Status.OK)
				.entity(combatSkillDAO.getByID(updatedCombatSkill
						.getCombatSkillID())).build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int combatSkillID) {
		if (combatSkillDAO.getByID(combatSkillID) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		combatSkillDAO.deleteByID(combatSkillID);
		return Response.status(Status.OK).build();
	}
}