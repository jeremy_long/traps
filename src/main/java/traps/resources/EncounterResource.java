package traps.resources;

import java.util.Set;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import traps.api.Action;
import traps.api.Combatant;
import traps.api.Encounter;
import traps.api.StatusEffect;
import traps.api.Weapon;
import traps.dao.ActionDAO;
import traps.dao.CombatantDAO;
import traps.dao.EncounterDAO;
import traps.util.RollUtils;

@Path("/encounters")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EncounterResource {

	private final EncounterDAO encounterDAO;
	private final ActionDAO actionDAO;
	private final CombatantDAO combatantDAO;

	public EncounterResource(EncounterDAO encounterDAO, ActionDAO actionDAO,
			CombatantDAO combatantDAO) {
		this.encounterDAO = encounterDAO;
		this.actionDAO = actionDAO;
		this.combatantDAO = combatantDAO;
	}

	/**
	 * @return a set of all character encounters in the system in json form, or
	 *         an empty set if there are none.
	 */
	@GET
	public Set<Encounter> getAll() {
		Set<Encounter> encounters = encounterDAO.getAll();
		for (Encounter encounter : encounters) {
			for (Combatant combatant : encounter.getCombatants()) {
				Set<Action> actions = actionDAO.getByCombatantID(combatant
						.getCombatantID());
				for (Action action : actions) {
					combatant.addAction(action);
				}
			}
			encounter.sortCombatants();
		}
		return encounters;
	}

	@GET
	@Path("{id}")
	public Response getByID(@PathParam("id") int encounterID) {
		Encounter encounter = encounterDAO.getByID(encounterID).iterator()
				.next();
		if (encounter == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		for (Combatant combatant : encounter.getCombatants()) {
			Set<Action> actions = actionDAO.getByCombatantID(combatant
					.getCombatantID());
			for (Action action : actions) {
				combatant.addAction(action);
			}
		}
		encounter.sortCombatants();
		return Response.status(Status.OK).entity(encounter).build();
	}

	/**
	 * @param newEncounter
	 *            a new Encounter.
	 * @return 201 if created, error otherwise
	 */
	@POST
	public Response add(@Valid Encounter newEncounter) {
		try {
			newEncounter.setCurrentRound(1);
			int encounterID = encounterDAO.add(newEncounter);
			return Response.status(Status.CREATED)
					.entity(encounterDAO.getByID(encounterID)).build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e).build();
		}
	}

	@PUT
	@Path("{id}")
	public Response update(@Valid Encounter updatedEncounter) {
		Set<Encounter> encounterSet = encounterDAO.getByID(updatedEncounter
				.getEncounterID());
		if (encounterSet == null || encounterSet.size() == 0) {
			return Response.status(Status.NOT_FOUND).build();
		}
		Encounter existingEncounter = encounterSet.iterator().next();

		int round = existingEncounter.getCurrentRound();
		if (updatedEncounter.getCurrentRound() > round) {
			round++;

			for (Combatant combatant : existingEncounter.getCombatants()) {
				combatant.getActions().clear();
				Set<Action> actions = actionDAO.getByCombatantID(combatant
						.getCombatantID());
				for (Action action : actions) {
					combatant.addAction(action);
				}
				combatant.carryOver();
				combatant.accumulateSkill();
				combatantDAO.update(combatant);

				if (combatant.getActions().size() > 0) {
					Action currentAction = combatant.getActions().iterator()
							.next();
					currentAction.setInitiative(currentAction.getSpeed()
							+ RollUtils.rollInitiative()
							+ combatant.getCurrentSpeedModifier());
					actionDAO.update(currentAction);
				}

				for (StatusEffect statusEffect : combatant.getStatusEffects()) {
					if (!statusEffect.getPersistent()
							&& statusEffect.getRoundInflicted() < (round - 2)) {
						combatantDAO.removeStatusEffect(
								combatant.getCombatantID(),
								statusEffect.getStatusEffectID());
					} else {
						combatantDAO.updateStatusEffect(
								combatant.getCombatantID(),
								statusEffect.getStatusEffectID(),
								statusEffect.getModifier() - 3);
					}
				}

				Weapon defaultWeapon = combatant.getCharacter().getWeapons()
						.iterator().next();
				int speed = RollUtils.rollInitiative()
						+ defaultWeapon.getSpeed();
				Action action = new Action(0, combatant.getCombatantID(),
						defaultWeapon.getWeaponID(), round, speed);
				actionDAO.add(action);
			}
		}
		updatedEncounter.setCurrentRound(round);

		encounterDAO.update(updatedEncounter);
		return Response
				.status(Status.OK)
				.entity(encounterDAO.getByID(updatedEncounter.getEncounterID()))
				.build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int encounterID) {
		if (encounterDAO.getByID(encounterID) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		encounterDAO.deleteByID(encounterID);
		return Response.status(Status.OK).build();
	}
}