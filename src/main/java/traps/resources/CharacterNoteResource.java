package traps.resources;

import java.util.Set;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import traps.api.CharacterNote;
import traps.dao.CharacterNoteDAO;

@Path("/notes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CharacterNoteResource {

	private final CharacterNoteDAO characterNoteDAO;

	public CharacterNoteResource(CharacterNoteDAO characterNoteDAO) {
		this.characterNoteDAO = characterNoteDAO;
	}

	/**
	 * @return a set of all character notes in the system in json form, or an
	 *         empty set if there are none.
	 */
	@GET
	public Set<CharacterNote> getAll() {
		return characterNoteDAO.getAll();
	}

	@GET
	@Path("{id}")
	public Response getByID(@PathParam("id") int characterNoteID) {
		CharacterNote characterNote = characterNoteDAO.getByID(characterNoteID);
		if (characterNote == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.status(Status.OK).entity(characterNote).build();
	}

	/**
	 * @param newCharacterNote
	 *            a new CharacterNote.
	 * @return 201 if created, error otherwise
	 */
	@POST
	public Response add(@Valid CharacterNote newCharacterNote) {
		try {
			int characterNoteID = characterNoteDAO.add(newCharacterNote);
			return Response.status(Status.CREATED)
					.entity(characterNoteDAO.getByID(characterNoteID)).build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e).build();
		}
	}

	@PUT
	@Path("{id}")
	public Response update(@Valid CharacterNote updatedCharacterNote) {
		if (characterNoteDAO.getByID(updatedCharacterNote.getCharacterNoteID()) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		characterNoteDAO.update(updatedCharacterNote);
		return Response
				.status(Status.OK)
				.entity(characterNoteDAO.getByID(updatedCharacterNote
						.getCharacterNoteID())).build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int characterNoteID) {
		if (characterNoteDAO.getByID(characterNoteID) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		characterNoteDAO.deleteByID(characterNoteID);
		return Response.status(Status.OK).build();
	}
}