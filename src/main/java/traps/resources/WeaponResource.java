package traps.resources;

import java.util.Set;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import traps.api.CombatSkill;
import traps.api.Weapon;
import traps.dao.CombatSkillDAO;
import traps.dao.WeaponDAO;

@Path("/weapons")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class WeaponResource {

	private final WeaponDAO weaponDAO;
	private final CombatSkillDAO combatSkillDAO;

	public WeaponResource(WeaponDAO weaponDAO, CombatSkillDAO combatSkillDAO) {
		this.weaponDAO = weaponDAO;
		this.combatSkillDAO = combatSkillDAO;
	}

	/**
	 * @return a set of all character weapons in the system in json form, or an
	 *         empty set if there are none.
	 */
	@GET
	public Set<Weapon> getAll() {
		return weaponDAO.getAll();
	}

	@GET
	@Path("{id}")
	public Response getByID(@PathParam("id") int weaponID) {
		Weapon weapon = weaponDAO.getByID(weaponID);
		if (weapon == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.status(Status.OK).entity(weapon).build();
	}

	/**
	 * @param newWeapon
	 *            a new Weapon.
	 * @return 201 if created, error otherwise
	 */
	@POST
	public Response add(@Valid Weapon newWeapon) {
		try {
			CombatSkill combatSkill = combatSkillDAO.getByID(newWeapon
					.getCombatSkill().getCombatSkillID());
			newWeapon.setCombatSkill(combatSkill);

			int weaponID = weaponDAO.add(newWeapon);
			return Response.status(Status.CREATED)
					.entity(weaponDAO.getByID(weaponID)).build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e).build();
		}
	}

	@PUT
	@Path("{id}")
	public Response update(@Valid Weapon updatedWeapon) {
		if (weaponDAO.getByID(updatedWeapon.getWeaponID()) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		CombatSkill combatSkill = combatSkillDAO.getByID(updatedWeapon
				.getCombatSkill().getCombatSkillID());
		if (combatSkill == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		updatedWeapon.setCombatSkill(combatSkill);
		weaponDAO.update(updatedWeapon);
		return Response.status(Status.OK)
				.entity(weaponDAO.getByID(updatedWeapon.getWeaponID())).build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int weaponID) {
		if (weaponDAO.getByID(weaponID) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		weaponDAO.deleteByID(weaponID);
		return Response.status(Status.OK).build();
	}
}