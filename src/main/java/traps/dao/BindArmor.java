package traps.dao;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import traps.api.Armor;

@BindingAnnotation(BindArmor.ArmorBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER })
public @interface BindArmor {
	public static class ArmorBinderFactory implements BinderFactory {
		@SuppressWarnings("rawtypes")
		@Override
		public Binder build(Annotation annotation) {
			return new Binder<BindArmor, Armor>() {
				@Override
				public void bind(SQLStatement<?> sql, BindArmor bind,
						Armor armor) {
					sql.bind("armor_id", armor.getArmorID());
					sql.bind("armor_name", armor.getArmorName());
					sql.bind("damage_reduction", armor.getDamageReduction());
					sql.bind("evade", armor.getEvade());
					sql.bind("weight", armor.getWeight());
					sql.bind("armor_cost", armor.getCost());
					sql.bind("type", armor.getType().toString());
					sql.bind("armor_hands", armor.getHands());
				}
			};
		}
	}
}