package traps.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import traps.api.CharacterNote;

public class CharacterNoteMapper implements ResultSetMapper<CharacterNote> {
	@Override
	public CharacterNote map(int index, ResultSet r, StatementContext ctx)
			throws SQLException {
		CharacterNote result = new CharacterNote(r.getInt("character_note_id"),
				r.getInt("character_id"), r.getString("text"));
		result.setUpdated(new DateTime(r.getDate("updated")));
		return result;
	}
}
