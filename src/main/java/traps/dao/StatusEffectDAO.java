package traps.dao;

import java.util.Set;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import traps.api.StatusEffect;

@RegisterMapper(StatusEffectMapper.class)
public interface StatusEffectDAO extends Transactional<StatusEffectDAO> {

	@SqlQuery("SELECT * FROM status_effects LEFT JOIN status_effect_modifiers ON (status_effects.status_effect_id = status_effect_modifiers.status_effect_id) ORDER BY status_effects.status_effect_name ASC")
	public Set<StatusEffect> getAll();

	@SqlQuery("SELECT * FROM status_effects LEFT JOIN status_effect_modifiers ON (status_effects.status_effect_id = status_effect_modifiers.status_effect_id) WHERE status_effects.status_effect_id = :status_effect_id ORDER BY status_effects.status_effect_name ASC")
	public Set<StatusEffect> getByID(
			@Bind("status_effect_id") int statusEffectID);

	@GetGeneratedKeys
	@SqlUpdate("INSERT INTO status_effects (status_effect_name, status_effect_description, persistent) VALUES (:status_effect_name, :status_effect_description, :persistent)")
	public int add(@BindStatusEffect StatusEffect statusEffect);

	@SqlUpdate("DELETE FROM status_effects WHERE status_effect_id = :status_effect_id")
	public void deleteByID(@Bind("status_effect_id") int statusEffectID);

	@SqlUpdate("UPDATE status_effects SET status_effect_name = :status_effect_name, status_effect_description = :status_effect_description, persistent = :persistent WHERE status_effects.status_effect_id = :status_effect_id")
	public void update(@BindStatusEffect StatusEffect statusEffect);

	@SqlUpdate("INSERT INTO status_effect_modifiers (status_effect_id, affected_attribute, attribute_effect) VALUES (:status_effect_id, :affected_attribute, :attribute_effect)")
	public void addEffectModifier(@Bind("status_effect_id") int statusEffectID,
			@Bind("affected_attribute") String affectedAttribute,
			@Bind("attribute_effect") int attributeEffect);

	@SqlUpdate("DELETE FROM status_effect_modifiers WHERE status_effect_id = :status_effect_id AND affected_attribute = :affected_attribute")
	public void removeEffectModifier(
			@Bind("status_effect_id") int statusEffectID,
			@Bind("affected_attribute") String affectedAttribute);
}