package traps.dao;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import traps.api.CharacterNote;

@BindingAnnotation(BindCharacterNote.CharacterNoteBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER })
public @interface BindCharacterNote {
	public static class CharacterNoteBinderFactory implements BinderFactory {
		@SuppressWarnings("rawtypes")
		@Override
		public Binder build(Annotation annotation) {
			return new Binder<BindCharacterNote, CharacterNote>() {
				@Override
				public void bind(SQLStatement<?> sql, BindCharacterNote bind,
						CharacterNote characterNote) {
					sql.bind("character_note_id",
							characterNote.getCharacterNoteID());
					sql.bind("character_id", characterNote.getCharacterID());
					sql.bind("text", characterNote.getText());
				}
			};
		}
	}
}