package traps.dao;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import traps.api.Technique;

@BindingAnnotation(BindTechnique.TechniqueBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER })
public @interface BindTechnique {
	public static class TechniqueBinderFactory implements BinderFactory {
		@SuppressWarnings("rawtypes")
		@Override
		public Binder build(Annotation annotation) {
			return new Binder<BindTechnique, Technique>() {
				@Override
				public void bind(SQLStatement<?> sql, BindTechnique bind,
						Technique technique) {
					sql.bind("technique_id", technique.getTechniqueID());
					sql.bind("technique_name", technique.getTechniqueName());
					sql.bind("power_cost", technique.getPowerCost());
					sql.bind("accuracy_cost", technique.getAccuracyCost());
					sql.bind("finesse_cost", technique.getFinesseCost());
					sql.bind("hit_bonus", technique.getHitBonus());
					sql.bind("damage_bonus", technique.getDamageBonus());
					sql.bind("pierce_bonus", technique.getPierceBonus());
					sql.bind("speed_bonus", technique.getSpeedBonus());
					sql.bind("evade_bonus", technique.getEvadeBonus());
					sql.bind("damage_reduction_bonus",
							technique.getDamageReductionBonus());
					sql.bind("resistance_bonus", technique.getResistanceBonus());
					sql.bind("exclusive_set", technique.getExclusiveSet());
					sql.bind("special_effects", technique.getSpecialEffects());
					sql.bind("technique_level", technique.getTechniqueLevel());
				}
			};
		}
	}
}