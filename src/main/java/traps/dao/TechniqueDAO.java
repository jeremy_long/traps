package traps.dao;

import java.util.Set;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import traps.api.Technique;

@RegisterMapper(TechniqueMapper.class)
public interface TechniqueDAO extends Transactional<TechniqueDAO> {

	@SqlQuery("SELECT * FROM techniques ORDER BY technique_id ASC")
	public Set<Technique> getAll();

	@SqlQuery("SELECT * FROM techniques WHERE technique_id = :technique_id ORDER BY technique_id ASC")
	public Technique getByID(@Bind("technique_id") int techniqueID);

	@SqlQuery("SELECT * FROM techniques WHERE technique_name = :technique_name ORDER BY technique_id ASC")
	public Technique getByName(@Bind("technique_name") String techniqueName);

	@GetGeneratedKeys
	@SqlUpdate("INSERT INTO techniques (technique_name, updated, power_cost, accuracy_cost, finesse_cost, hit_bonus, damage_bonus, pierce_bonus, speed_bonus, evade_bonus, damage_reduction_bonus, resistance_bonus, exclusive_set, special_effects, technique_level) VALUES (:technique_name, now(), :power_cost, :accuracy_cost, :finesse_cost, :hit_bonus, :damage_bonus, :pierce_bonus, :speed_bonus, :evade_bonus, :damage_reduction_bonus, :resistance_bonus, :exclusive_set, :special_effects, :technique_level)")
	public int add(@BindTechnique Technique technique);

	@SqlUpdate("DELETE FROM techniques WHERE technique_id = :technique_id")
	public void deleteByID(@Bind("technique_id") int techniqueID);

	@SqlUpdate("UPDATE techniques SET technique_name = :technique_name, updated = now(), power_cost = :power_cost, accuracy_cost = :accuracy_cost, finesse_cost = :finesse_cost, hit_bonus = :hit_bonus, damage_bonus = :damage_bonus, pierce_bonus = :pierce_bonus, speed_bonus = :speed_bonus, evade_bonus = :evade_bonus, damage_reduction_bonus = :damage_reduction_bonus, resistance_bonus = :resistance_bonus, exclusive_set = :exclusive_set, special_effects = :special_effects, technique_level = :technique_level WHERE technique_id = :technique_id")
	public void update(@BindTechnique Technique technique);
}