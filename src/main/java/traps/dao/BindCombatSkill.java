package traps.dao;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import traps.api.CombatSkill;

@BindingAnnotation(BindCombatSkill.CombatSkillBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER })
public @interface BindCombatSkill {
	public static class CombatSkillBinderFactory implements BinderFactory {
		@SuppressWarnings("rawtypes")
		@Override
		public Binder build(Annotation annotation) {
			return new Binder<BindCombatSkill, CombatSkill>() {
				@Override
				public void bind(SQLStatement<?> sql, BindCombatSkill bind,
						CombatSkill combatSkill) {
					sql.bind("combat_skill_id", combatSkill.getCombatSkillID());
					sql.bind("combat_skill_name",
							combatSkill.getCombatSkillName());
					sql.bind("power_rank", combatSkill.getPowerRank()
							.toString());
					sql.bind("accuracy_rank", combatSkill.getAccuracyRank()
							.toString());
					sql.bind("finesse_rank", combatSkill.getFinesseRank()
							.toString());
				}
			};
		}
	}
}