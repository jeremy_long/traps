package traps.dao;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import traps.api.Character;

@BindingAnnotation(BindCharacter.CharacterBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER })
public @interface BindCharacter {
	public static class CharacterBinderFactory implements BinderFactory {
		@SuppressWarnings("rawtypes")
		@Override
		public Binder build(Annotation annotation) {
			return new Binder<BindCharacter, Character>() {
				@Override
				public void bind(SQLStatement<?> sql, BindCharacter bind,
						Character character) {
					sql.bind("character_id", character.getCharacterID());
					sql.bind("character_name", character.getCharacterName());
					sql.bind("level", character.getLevel());
					sql.bind("hp", character.getHp());
					sql.bind("ep", character.getEp());
					sql.bind("hp_initial", character.getHpInitial());
					sql.bind("ep_initial", character.getEpInitial());
					sql.bind("strength", character.getStrength());
					sql.bind("dexterity", character.getDexterity());
					sql.bind("agility", character.getAgility());
					sql.bind("health", character.getHealth());
					sql.bind("cunning", character.getCunning());
					sql.bind("willpower", character.getWillpower());
					sql.bind("intelligence", character.getIntelligence());
					sql.bind("attribute", character.getAttribute());
					sql.bind("strength_attribute",
							character.getStrengthAttribute());
					sql.bind("dexterity_attribute",
							character.getDexterityAttribute());
					sql.bind("agility_attribute",
							character.getAgilityAttribute());
					sql.bind("health_attribute", character.getHealthAttribute());
					sql.bind("cunning_attribute",
							character.getCunningAttribute());
					sql.bind("willpower_attribute",
							character.getWillpowerAttribute());
					sql.bind("intelligence_attribute",
							character.getIntelligenceAttribute());
					sql.bind("skill", character.getSkill());
					sql.bind("power", character.getPower());
					sql.bind("accuracy", character.getAccuracy());
					sql.bind("finesse", character.getFinesse());
					sql.bind("wild", character.getWild());
					sql.bind("passive", character.getPassive());

					if (character.getCharacterClass() != null) {
						sql.bind("class_id", character.getCharacterClass()
								.getCharacterClassID());
					}
				}
			};
		}
	}
}