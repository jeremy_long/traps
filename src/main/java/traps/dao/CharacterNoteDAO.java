package traps.dao;

import java.util.Set;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import traps.api.CharacterNote;

@RegisterMapper(CharacterNoteMapper.class)
public interface CharacterNoteDAO extends Transactional<CharacterNoteDAO> {

	@SqlQuery("SELECT * FROM character_notes ORDER BY character_note_id ASC")
	public Set<CharacterNote> getAll();

	@SqlQuery("SELECT * FROM character_notes WHERE character_note_id = :character_note_id ORDER BY character_note_id ASC")
	public CharacterNote getByID(@Bind("character_note_id") int characterNoteID);

	@GetGeneratedKeys
	@SqlUpdate("INSERT INTO character_notes (character_id, updated, text) VALUES (:character_id, now(), :text)")
	public int add(@BindCharacterNote CharacterNote characterNote);

	@SqlUpdate("DELETE FROM character_notes WHERE character_note_id = :character_note_id")
	public void deleteByID(@Bind("character_note_id") int characterNoteID);

	@SqlUpdate("UPDATE character_notes SET character_id = :character_id, updated = now(), text = :text WHERE character_note_id = :character_note_id")
	public void update(@BindCharacterNote CharacterNote characterNote);
}
