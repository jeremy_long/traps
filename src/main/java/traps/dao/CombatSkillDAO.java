package traps.dao;

import java.util.Set;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import traps.api.CombatSkill;

@RegisterMapper(CombatSkillMapper.class)
public interface CombatSkillDAO extends Transactional<CombatSkillDAO> {

	@SqlQuery("SELECT * FROM combat_skills ORDER BY combat_skill_id ASC")
	public Set<CombatSkill> getAll();

	@SqlQuery("SELECT * FROM combat_skills WHERE combat_skill_id = :combat_skill_id ORDER BY combat_skill_id ASC")
	public CombatSkill getByID(@Bind("combat_skill_id") int combatSkillID);

	@GetGeneratedKeys
	@SqlUpdate("INSERT INTO combat_skills (combat_skill_name, updated, power_rank, accuracy_rank, finesse_rank) VALUES (:combat_skill_name, now(), :power_rank, :accuracy_rank, :finesse_rank)")
	public int add(@BindCombatSkill CombatSkill combatSkill);

	@SqlUpdate("DELETE FROM combat_skills WHERE combat_skill_id = :combat_skill_id")
	public void deleteByID(@Bind("combat_skill_id") int combatSkillID);

	@SqlUpdate("UPDATE combat_skills SET combat_skill_name = :combat_skill_name, updated = now(), power_rank = :power_rank, accuracy_rank = :accuracy_rank, finesse_rank = :finesse_rank WHERE combat_skill_id = :combat_skill_id")
	public void update(@BindCombatSkill CombatSkill combatSkill);
}
