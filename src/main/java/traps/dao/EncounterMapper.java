package traps.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import traps.api.Combatant;
import traps.api.Encounter;

public class EncounterMapper implements ResultSetMapper<Encounter> {

	private Encounter encounter;
	private CombatantMapper combatantMapper;

	@Override
	public Encounter map(int index, ResultSet r, StatementContext ctx)
			throws SQLException {
		if (encounter == null
				|| encounter.getEncounterID() != r.getInt("encounter_id")) {
			combatantMapper = new CombatantMapper();
			encounter = new Encounter(r.getInt("encounter_id"),
					r.getString("encounter_name"), r.getInt("current_round"));
			encounter.setUpdated(new DateTime(r.getTimestamp("updated")));
		}
		try {
			Combatant combatant = combatantMapper.map(index, r, ctx);
			if (combatant.getCombatantID() > 0) {
				encounter.addCombatant(combatant);
			}
		} catch (Exception e) {
		}

		return encounter;
	}
}