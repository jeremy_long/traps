package traps.dao;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import traps.api.StatusEffect;

@BindingAnnotation(BindStatusEffect.StatusEffectBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER })
public @interface BindStatusEffect {
	public static class StatusEffectBinderFactory implements BinderFactory {
		@SuppressWarnings("rawtypes")
		@Override
		public Binder build(Annotation annotation) {
			return new Binder<BindStatusEffect, StatusEffect>() {
				@Override
				public void bind(SQLStatement<?> sql, BindStatusEffect bind,
						StatusEffect statusEffect) {
					sql.bind("status_effect_id",
							statusEffect.getStatusEffectID());
					sql.bind("status_effect_name",
							statusEffect.getStatusEffectName());
					sql.bind("status_effect_description",
							statusEffect.getStatusEffectDescription());
					sql.bind("persistent", statusEffect.getPersistent());
				}
			};
		}
	}
}