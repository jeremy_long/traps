package traps.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import traps.api.Technique;

public class TechniqueMapper implements ResultSetMapper<Technique> {
	@Override
	public Technique map(int index, ResultSet r, StatementContext ctx)
			throws SQLException {
		Technique technique = new Technique(r.getInt("technique_id"),
				r.getString("technique_name"), r.getInt("power_cost"),
				r.getInt("accuracy_cost"), r.getInt("finesse_cost"),
				r.getInt("hit_bonus"), r.getInt("damage_bonus"),
				r.getInt("pierce_bonus"), r.getInt("speed_bonus"),
				r.getInt("evade_bonus"), r.getInt("damage_reduction_bonus"),
				r.getInt("resistance_bonus"), r.getString("exclusive_set"),
				r.getString("special_effects"), r.getInt("technique_level"));
		return technique;
	}
}
