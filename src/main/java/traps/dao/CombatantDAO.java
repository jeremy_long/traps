package traps.dao;

import java.util.Set;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import traps.api.Combatant;

@RegisterMapper(CombatantMapper.class)
public interface CombatantDAO extends Transactional<CombatantDAO> {

	@SqlQuery("SELECT * FROM combatants LEFT JOIN characters ON (combatants.character_id = characters.character_id) LEFT JOIN classes ON (characters.character_class = classes.class_id) LEFT JOIN character_notes ON (characters.character_id = character_notes.character_id) LEFT JOIN character_weapons ON (characters.character_id = character_weapons.character_id) LEFT JOIN weapons ON (character_weapons.weapon_id = weapons.weapon_id) LEFT JOIN combat_skills ON (weapons.combat_skill = combat_skills.combat_skill_id) LEFT JOIN character_armor ON (characters.character_id = character_armor.character_id) LEFT JOIN armor ON (character_armor.armor_id = armor.armor_id) LEFT JOIN combatant_status_effects ON (combatants.combatant_id = combatant_status_effects.combatant_id) LEFT JOIN status_effects ON (combatant_status_effects.status_effect_id = status_effects.status_effect_id) LEFT JOIN status_effect_modifiers ON (status_effects.status_effect_id = status_effect_modifiers.status_effect_id) ORDER BY combatants.combatant_id ASC, characters.character_id ASC, character_notes.character_note_id ASC, weapons.weapon_id ASC, armor.armor_id ASC, status_effects.status_effect_id ASC")
	public Set<Combatant> getAll();

	@SqlQuery("SELECT * FROM combatants LEFT JOIN characters ON (combatants.character_id = characters.character_id) LEFT JOIN classes ON (characters.character_class = classes.class_id) LEFT JOIN character_notes ON (characters.character_id = character_notes.character_id) LEFT JOIN character_weapons ON (characters.character_id = character_weapons.character_id) LEFT JOIN weapons ON (character_weapons.weapon_id = weapons.weapon_id) LEFT JOIN combat_skills ON (weapons.combat_skill = combat_skills.combat_skill_id) LEFT JOIN character_armor ON (characters.character_id = character_armor.character_id) LEFT JOIN armor ON (character_armor.armor_id = armor.armor_id) LEFT JOIN combatant_status_effects ON (combatants.combatant_id = combatant_status_effects.combatant_id) LEFT JOIN status_effects ON (combatant_status_effects.status_effect_id = status_effects.status_effect_id) LEFT JOIN status_effect_modifiers ON (status_effects.status_effect_id = status_effect_modifiers.status_effect_id) WHERE combatants.combatant_id = :combatant_id ORDER BY combatants.combatant_id ASC, characters.character_id ASC, character_notes.character_note_id ASC, weapons.weapon_id ASC, armor.armor_id ASC, status_effects.status_effect_id ASC")
	public Set<Combatant> getByID(@Bind("combatant_id") int combatantID);

	@GetGeneratedKeys
	@SqlUpdate("INSERT INTO combatants (character_id, encounter_id, current_hp, current_ep, current_power, current_accuracy, current_finesse, current_wild) VALUES (:character_id, :encounter_id, :current_hp, :current_ep, :current_power, :current_accuracy, :current_finesse, :current_wild)")
	public int add(@BindCombatant Combatant combatant);

	@SqlUpdate("DELETE FROM combatants WHERE combatant_id = :combatant_id")
	public void deleteByID(@Bind("combatant_id") int combatantID);

	@SqlUpdate("UPDATE combatants SET current_hp = :current_hp, current_ep = :current_ep, current_power = :current_power, current_accuracy = :current_accuracy, current_finesse = :current_finesse, current_wild = :current_wild WHERE combatants.combatant_id = :combatant_id")
	public void update(@BindCombatant Combatant combatant);

	@SqlUpdate("INSERT INTO combatant_status_effects (combatant_id, status_effect_id, round_inflicted, check_modifier) VALUES (:combatant_id, :status_effect_id, :round_inflicted, :check_modifier)")
	public void addStatusEffect(@Bind("combatant_id") int combatantID,
			@Bind("status_effect_id") int statusEffectID,
			@Bind("round_inflicted") int roundInflicted,
			@Bind("check_modifier") int checkModifier);

	@SqlUpdate("DELETE FROM combatant_status_effects WHERE combatant_id = :combatant_id AND status_effect_id = :status_effect_id")
	public void removeStatusEffect(@Bind("combatant_id") int combatantID,
			@Bind("status_effect_id") int statusEffectID);

	@SqlUpdate("UPDATE combatant_status_effects SET check_modifier = :check_modifier WHERE combatant_id = :combatant_id AND status_effect_id = :status_effect_id")
	public void updateStatusEffect(@Bind("combatant_id") int combatantID,
			@Bind("status_effect_id") int statusEffectID,
			@Bind("check_modifier") int check_modifier);
}