package traps.dao;

import java.util.Set;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import traps.api.Armor;

@RegisterMapper(ArmorMapper.class)
public interface ArmorDAO extends Transactional<ArmorDAO> {

	@SqlQuery("SELECT * FROM armor ORDER BY armor_id ASC")
	public Set<Armor> getAll();

	@SqlQuery("SELECT * FROM armor WHERE armor_id = :armor_id ORDER BY armor_id ASC")
	public Armor getByID(@Bind("armor_id") int armorID);

	@GetGeneratedKeys
	@SqlUpdate("INSERT INTO armor (armor_name, updated, damage_reduction, evade, weight, type, armor_hands, armor_cost) VALUES (:armor_name, now(), :damage_reduction, :evade, :weight, :type, :armor_hands, :armor_cost)")
	public int add(@BindArmor Armor armor);

	@SqlUpdate("DELETE FROM armor WHERE armor_id = :armor_id")
	public void deleteByID(@Bind("armor_id") int armorID);

	@SqlUpdate("UPDATE armor SET armor_name = :armor_name, updated = now(), damage_reduction = :damage_reduction, evade = :evade, weight = :weight, type = :type, armor_hands = :armor_hands, armor_cost = :armor_cost WHERE armor_id = :armor_id")
	public void update(@BindArmor Armor armor);
}