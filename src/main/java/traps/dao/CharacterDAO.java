package traps.dao;

import java.util.Set;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import traps.api.Character;

@RegisterMapper(CharacterMapper.class)
public interface CharacterDAO extends Transactional<CharacterDAO> {

	@SqlQuery("SELECT * FROM characters LEFT JOIN classes ON (characters.character_class = classes.class_id) LEFT JOIN character_notes ON (characters.character_id = character_notes.character_id) LEFT JOIN character_weapons ON (characters.character_id = character_weapons.character_id) LEFT JOIN weapons ON (character_weapons.weapon_id = weapons.weapon_id) LEFT JOIN combat_skills ON (weapons.combat_skill = combat_skills.combat_skill_id) LEFT JOIN character_armor ON (characters.character_id = character_armor.character_id) LEFT JOIN armor ON (character_armor.armor_id = armor.armor_id) ORDER BY characters.character_id ASC, character_notes.character_note_id ASC, weapons.weapon_id ASC, armor.armor_id ASC")
	public Set<Character> getAll();

	@SqlQuery("SELECT * FROM characters LEFT JOIN classes ON (characters.character_class = classes.class_id) LEFT JOIN character_notes ON (characters.character_id = character_notes.character_id) WHERE characters.character_class = :class_id ORDER BY characters.character_id ASC, character_notes.character_note_id ASC")
	public Set<Character> getAllByClassID(@Bind("class_id") int classID);

	@SqlQuery("SELECT * FROM characters LEFT JOIN classes ON (characters.character_class = classes.class_id) LEFT JOIN character_notes ON (characters.character_id = character_notes.character_id) LEFT JOIN character_weapons ON (characters.character_id = character_weapons.character_id) LEFT JOIN weapons ON (character_weapons.weapon_id = weapons.weapon_id) LEFT JOIN combat_skills ON (weapons.combat_skill = combat_skills.combat_skill_id) LEFT JOIN character_armor ON (characters.character_id = character_armor.character_id) LEFT JOIN armor ON (character_armor.armor_id = armor.armor_id) WHERE characters.character_id = :character_id ORDER BY characters.character_id ASC, character_notes.character_note_id ASC, weapons.weapon_id ASC, armor.armor_id ASC")
	public Set<Character> getByID(@Bind("character_id") int characterID);

	@GetGeneratedKeys
	@SqlUpdate("INSERT INTO characters (character_name, character_class, updated, level, hp, ep, hp_initial, ep_initial, strength, dexterity, agility, health, cunning, willpower, intelligence, attribute, strength_attribute, dexterity_attribute, agility_attribute, health_attribute, cunning_attribute, willpower_attribute, intelligence_attribute, skill, power, accuracy, finesse, wild, passive) VALUES (:character_name, :class_id, now(), :level, :hp, :ep, :hp_initial, :ep_initial, :strength, :dexterity, :agility, :health, :cunning, :willpower, :intelligence, :attribute, :strength_attribute, :dexterity_attribute, :agility_attribute, :health_attribute, :cunning_attribute, :willpower_attribute, :intelligence_attribute, :skill, :power, :accuracy, :finesse, :wild, :passive)")
	public int add(@BindCharacter Character character);

	@SqlUpdate("DELETE FROM characters WHERE character_id = :character_id")
	public void deleteByID(@Bind("character_id") int characterID);

	@SqlUpdate("UPDATE characters SET character_name = :character_name, character_class = :class_id, updated = now(), level = :level, hp = :hp, ep = :ep, hp_initial = :hp_initial, ep_initial = :ep_initial, strength = :strength, dexterity = :dexterity, agility = :agility, health = :health, cunning = :cunning, willpower = :willpower, intelligence = :intelligence, attribute = :attribute, strength_attribute = :strength_attribute, dexterity_attribute = :dexterity_attribute, agility_attribute = :agility_attribute, health_attribute = :health_attribute, cunning_attribute = :cunning_attribute, willpower_attribute = :willpower_attribute, intelligence_attribute = :intelligence_attribute, skill = :skill, power = :power, accuracy = :accuracy, finesse = :finesse, wild = :wild, passive = :passive WHERE character_id = :character_id")
	public void update(@BindCharacter Character character);

	@SqlUpdate("INSERT INTO character_weapons (character_id, weapon_id) VALUES (:character_id, :weapon_id)")
	public void addWeapon(@Bind("character_id") int characterID,
			@Bind("weapon_id") int weaponID);

	@SqlUpdate("DELETE FROM character_weapons WHERE character_id = :character_id AND weapon_id = :weapon_id")
	public void removeWeapon(@Bind("character_id") int characterID,
			@Bind("weapon_id") int weaponID);

	@SqlUpdate("INSERT INTO character_armor (character_id, armor_id) VALUES (:character_id, :armor_id)")
	public void addArmor(@Bind("character_id") int characterID,
			@Bind("armor_id") int armorID);

	@SqlUpdate("DELETE FROM character_armor WHERE character_id = :character_id AND armor_id = :armor_id")
	public void removeArmor(@Bind("character_id") int characterID,
			@Bind("armor_id") int armorID);
}
