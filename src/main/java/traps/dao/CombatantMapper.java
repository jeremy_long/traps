package traps.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import traps.api.Combatant;
import traps.api.StatusEffect;

public class CombatantMapper implements ResultSetMapper<Combatant> {

	private Combatant combatant;
	private CharacterMapper characterMapper;
	private StatusEffectMapper statusEffectMapper;

	@Override
	public Combatant map(int index, ResultSet r, StatementContext ctx)
			throws SQLException {
		if (combatant == null
				|| combatant.getCombatantID() != r.getInt("combatant_id")) {
			characterMapper = new CharacterMapper();
			statusEffectMapper = new StatusEffectMapper();
			combatant = new Combatant(r.getInt("combatant_id"),
					characterMapper.map(index, r, ctx),
					r.getInt("encounter_id"), r.getInt("current_hp"),
					r.getInt("current_ep"), r.getInt("current_power"),
					r.getInt("current_accuracy"), r.getInt("current_finesse"),
					r.getInt("current_wild"));
		} else {
			characterMapper.map(index, r, ctx);
		}
		try {
			StatusEffect statusEffect = statusEffectMapper.map(index, r, ctx);
			if (statusEffect.getStatusEffectID() > 0) {
				combatant.addStatusEffect(statusEffect);
			}
		} catch (Exception e) {
		}

		return combatant;
	}
}