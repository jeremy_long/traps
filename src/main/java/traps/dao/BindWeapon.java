package traps.dao;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import traps.api.Weapon;

@BindingAnnotation(BindWeapon.WeaponBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER })
public @interface BindWeapon {
	public static class WeaponBinderFactory implements BinderFactory {
		@SuppressWarnings("rawtypes")
		@Override
		public Binder build(Annotation annotation) {
			return new Binder<BindWeapon, Weapon>() {
				@Override
				public void bind(SQLStatement<?> sql, BindWeapon bind,
						Weapon weapon) {
					sql.bind("weapon_id", weapon.getWeaponID());
					sql.bind("weapon_name", weapon.getWeaponName());
					sql.bind("hit", weapon.getHitBonus());
					sql.bind("damage_dice_number", weapon.getDamageDiceNumber());
					sql.bind("damage_dice_type", weapon.getDamageDiceType());
					sql.bind("pierce", weapon.getPierce());
					sql.bind("range", weapon.getRange());
					sql.bind("speed", weapon.getSpeed());
					sql.bind("hands", weapon.getHands());
					sql.bind("cost", weapon.getCost());
					sql.bind("ep_cost", weapon.getEpCost());

					if (weapon.getCombatSkill() != null) {
						sql.bind("combat_skill", weapon.getCombatSkill()
								.getCombatSkillID());
					}
				}
			};
		}
	}
}