package traps.dao;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import traps.api.CharacterClass;

@BindingAnnotation(BindCharacterClass.CharacterClassBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER })
public @interface BindCharacterClass {
	public static class CharacterClassBinderFactory implements BinderFactory {
		@SuppressWarnings("rawtypes")
		@Override
		public Binder build(Annotation annotation) {
			return new Binder<BindCharacterClass, CharacterClass>() {
				@Override
				public void bind(SQLStatement<?> sql, BindCharacterClass bind,
						CharacterClass characterClass) {
					sql.bind("class_id", characterClass.getCharacterClassID());
					sql.bind("class_name",
							characterClass.getCharacterClassName());
					sql.bind("hp_level_bonus", characterClass.getHpLevelBonus());
					sql.bind("ep_level_bonus", characterClass.getEpLevelBonus());
					sql.bind("strength_initial_bonus",
							characterClass.getStrengthInitialBonus());
					sql.bind("dexterity_initial_bonus",
							characterClass.getDexterityInitialBonus());
					sql.bind("agility_initial_bonus",
							characterClass.getAgilityInitialBonus());
					sql.bind("health_initial_bonus",
							characterClass.getHealthInitialBonus());
					sql.bind("cunning_initial_bonus",
							characterClass.getCunningInitialBonus());
					sql.bind("willpower_initial_bonus",
							characterClass.getWillpowerInitialBonus());
					sql.bind("intelligence_initial_bonus",
							characterClass.getIntelligenceInitialBonus());
					sql.bind("attribute_initial_bonus",
							characterClass.getAttributeInitialBonus());
					sql.bind("hp_initial_bonus",
							characterClass.getHpInitialBonus());
					sql.bind("ep_initial_bonus",
							characterClass.getEpInitialBonus());
					sql.bind("skill_initial_bonus",
							characterClass.getSkillInitialBonus());
					sql.bind("passive_initial_bonus",
							characterClass.getPassiveInitialBonus());
				}
			};
		}
	}
}