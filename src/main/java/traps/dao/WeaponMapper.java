package traps.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import traps.api.Weapon;

public class WeaponMapper implements ResultSetMapper<Weapon> {
	@Override
	public Weapon map(int index, ResultSet r, StatementContext ctx)
			throws SQLException {
		Weapon weapon = new Weapon(r.getInt("weapon_id"),
				r.getString("weapon_name"), r.getInt("hit"),
				r.getInt("damage_dice_number"), r.getInt("damage_dice_type"),
				r.getInt("pierce"), r.getInt("speed"), r.getInt("range"),
				r.getInt("hands"), r.getInt("cost"), r.getInt("ep_cost"),
				new CombatSkillMapper().map(index, r, ctx));
		return weapon;
	}
}
