package traps.dao;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import traps.api.Action;

@BindingAnnotation(BindAction.ActionBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER })
public @interface BindAction {
	public static class ActionBinderFactory implements BinderFactory {
		@SuppressWarnings("rawtypes")
		@Override
		public Binder build(Annotation annotation) {
			return new Binder<BindAction, Action>() {
				@Override
				public void bind(SQLStatement<?> sql, BindAction bind,
						Action action) {
					sql.bind("action_id", action.getActionID());
					sql.bind("combatant_id", action.getCombatantID());
					sql.bind("weapon_id", action.getWeaponID());
					sql.bind("round", action.getRound());
					sql.bind("initiative", action.getInitiative());
				}
			};
		}
	}
}