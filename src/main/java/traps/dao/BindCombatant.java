package traps.dao;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import traps.api.Combatant;

@BindingAnnotation(BindCombatant.CombatantBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER })
public @interface BindCombatant {
	public static class CombatantBinderFactory implements BinderFactory {
		@SuppressWarnings("rawtypes")
		@Override
		public Binder build(Annotation annotation) {
			return new Binder<BindCombatant, Combatant>() {
				@Override
				public void bind(SQLStatement<?> sql, BindCombatant bind,
						Combatant combatant) {
					sql.bind("combatant_id", combatant.getCombatantID());
					sql.bind("character_id", combatant.getCharacter()
							.getCharacterID());
					sql.bind("encounter_id", combatant.getEncounterID());
					sql.bind("current_hp", combatant.getCurrentHp());
					sql.bind("current_ep", combatant.getCurrentEp());
					sql.bind("current_power", combatant.getCurrentPower());
					sql.bind("current_accuracy", combatant.getCurrentAccuracy());
					sql.bind("current_finesse", combatant.getCurrentFinesse());
					sql.bind("current_wild", combatant.getCurrentWild());
				}
			};
		}
	}
}