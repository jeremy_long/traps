package traps.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import traps.api.CharacterClass;

public class CharacterClassMapper implements ResultSetMapper<CharacterClass> {
	@Override
	public CharacterClass map(int index, ResultSet r, StatementContext ctx)
			throws SQLException {
		return new CharacterClass(r.getInt("class_id"),
				r.getString("class_name"), r.getInt("hp_level_bonus"),
				r.getInt("ep_level_bonus"), r.getInt("strength_initial_bonus"),
				r.getInt("dexterity_initial_bonus"),
				r.getInt("agility_initial_bonus"),
				r.getInt("health_initial_bonus"),
				r.getInt("cunning_initial_bonus"),
				r.getInt("willpower_initial_bonus"),
				r.getInt("intelligence_initial_bonus"),
				r.getInt("attribute_initial_bonus"),
				r.getInt("hp_initial_bonus"), r.getInt("ep_initial_bonus"),
				r.getInt("skill_initial_bonus"),
				r.getInt("passive_initial_bonus"));
	}
}
