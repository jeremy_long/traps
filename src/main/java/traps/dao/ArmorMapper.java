package traps.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import traps.api.Armor;

public class ArmorMapper implements ResultSetMapper<Armor> {
	@Override
	public Armor map(int index, ResultSet r, StatementContext ctx)
			throws SQLException {
		Armor armor = new Armor(r.getInt("armor_id"),
				r.getString("armor_name"), r.getInt("damage_reduction"),
				r.getInt("evade"), r.getInt("weight"), r.getString("type"),
				r.getInt("armor_hands"), r.getInt("armor_cost"));
		return armor;
	}
}
