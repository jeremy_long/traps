package traps.dao;

import java.util.Set;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import traps.api.Weapon;

@RegisterMapper(WeaponMapper.class)
public interface WeaponDAO extends Transactional<WeaponDAO> {

	@SqlQuery("SELECT * FROM weapons LEFT JOIN combat_skills ON (combat_skill = combat_skill_id) ORDER BY weapon_name ASC")
	public Set<Weapon> getAll();

	@SqlQuery("SELECT * FROM weapons LEFT JOIN combat_skills ON (combat_skill = combat_skill_id) WHERE weapon_id = :weapon_id ORDER BY weapon_id ASC")
	public Weapon getByID(@Bind("weapon_id") int weaponID);

	@GetGeneratedKeys
	@SqlUpdate("INSERT INTO weapons (weapon_name, updated, hit, damage_dice_number, damage_dice_type, pierce, speed, range, hands, cost, ep_cost, combat_skill) VALUES (:weapon_name, now(), :hit, :damage_dice_number, :damage_dice_type, :pierce, :speed, :range, :hands, :cost, :ep_cost, :combat_skill)")
	public int add(@BindWeapon Weapon weapon);

	@SqlUpdate("DELETE FROM weapons WHERE weapon_id = :weapon_id")
	public void deleteByID(@Bind("weapon_id") int weaponID);

	@SqlUpdate("UPDATE weapons SET weapon_name = :weapon_name, updated = now(), hit = :hit, damage_dice_number = :damage_dice_number, damage_dice_type = :damage_dice_type, pierce = :pierce, speed = :speed, range = :range, hands = :hands, cost = :cost, ep_cost = :ep_cost, combat_skill = :combat_skill WHERE weapon_id = :weapon_id")
	public void update(@BindWeapon Weapon weapon);
}
