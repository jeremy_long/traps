package traps.dao;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import traps.api.Encounter;

@BindingAnnotation(BindEncounter.EncounterBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER })
public @interface BindEncounter {
	public static class EncounterBinderFactory implements BinderFactory {
		@SuppressWarnings("rawtypes")
		@Override
		public Binder build(Annotation annotation) {
			return new Binder<BindEncounter, Encounter>() {
				@Override
				public void bind(SQLStatement<?> sql, BindEncounter bind,
						Encounter encounter) {
					sql.bind("encounter_id", encounter.getEncounterID());
					sql.bind("encounter_name", encounter.getEncounterName());
					sql.bind("current_round", encounter.getCurrentRound());
				}
			};
		}
	}
}