package traps.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import traps.api.StatusEffect;

public class StatusEffectMapper implements ResultSetMapper<StatusEffect> {

	private StatusEffect statusEffect;

	@Override
	public StatusEffect map(int index, ResultSet r, StatementContext ctx)
			throws SQLException {
		if (statusEffect == null
				|| statusEffect.getStatusEffectID() != r
						.getInt("status_effect_id")) {
			int checkModifier = 0;
			int roundInflicted = 0;
			try {
				checkModifier = r.getInt("check_modifier");
				roundInflicted = r.getInt("round_inflicted");
			} catch (Exception e) {
			}
			statusEffect = new StatusEffect(r.getInt("status_effect_id"),
					r.getString("status_effect_name"),
					r.getString("status_effect_description"), checkModifier,
					roundInflicted, r.getBoolean("persistent"));
		}
		try {
			if (r.getString("affected_attribute") != null) {
				statusEffect.addAttributeEffect(
						r.getString("affected_attribute"),
						r.getInt("attribute_effect"));
			}
		} catch (Exception e) {
		}
		return statusEffect;
	}
}