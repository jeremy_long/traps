package traps.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import traps.api.Armor;
import traps.api.Character;
import traps.api.CharacterNote;
import traps.api.Weapon;

public class CharacterMapper implements ResultSetMapper<Character> {

	private Character character;

	@Override
	public Character map(int index, ResultSet r, StatementContext ctx)
			throws SQLException {
		if (character == null
				|| character.getCharacterID() != r.getInt("character_id")) {
			character = new Character(r.getInt("character_id"),
					r.getString("character_name"), r.getInt("level"),
					r.getInt("hp"), r.getInt("ep"), r.getInt("hp_initial"),
					r.getInt("ep_initial"), r.getInt("strength"),
					r.getInt("dexterity"), r.getInt("agility"),
					r.getInt("health"), r.getInt("cunning"),
					r.getInt("willpower"), r.getInt("intelligence"),
					r.getInt("attribute"), r.getInt("strength_attribute"),
					r.getInt("dexterity_attribute"),
					r.getInt("agility_attribute"),
					r.getInt("health_attribute"),
					r.getInt("cunning_attribute"),
					r.getInt("willpower_attribute"),
					r.getInt("intelligence_attribute"), r.getInt("skill"),
					r.getInt("power"), r.getInt("accuracy"),
					r.getInt("finesse"), r.getInt("wild"), r.getInt("passive"));
			character.setUpdated(new DateTime(r.getDate("updated")));
			try {
				CharacterClassMapper characterClassMapper = new CharacterClassMapper();
				character.setCharacterClass(characterClassMapper.map(index, r,
						ctx));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			CharacterNote note = new CharacterNoteMapper().map(index, r, ctx);
			if (note.getCharacterNoteID() > 0) {
				character.addNote(note);
			}
		} catch (Exception e) {
		}
		try {
			Weapon weapon = new WeaponMapper().map(index, r, ctx);
			if (weapon.getWeaponID() > 0) {
				character.addWeapon(weapon);
			}
		} catch (Exception e) {
		}
		try {
			Armor armor = new ArmorMapper().map(index, r, ctx);
			if (armor.getArmorID() > 0) {
				character.addArmor(armor);
			}
		} catch (Exception e) {
		}

		return character;
	}
}