package traps.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import traps.api.CombatSkill;

public class CombatSkillMapper implements ResultSetMapper<CombatSkill> {
	@Override
	public CombatSkill map(int index, ResultSet r, StatementContext ctx)
			throws SQLException {
		return new CombatSkill(r.getInt("combat_skill_id"),
				r.getString("combat_skill_name"), r.getString("power_rank"),
				r.getString("accuracy_rank"), r.getString("finesse_rank"));
	}
}
