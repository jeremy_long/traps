package traps.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import traps.api.Action;
import traps.api.Technique;
import traps.api.Weapon;

public class ActionMapper implements ResultSetMapper<Action> {

	private Action action;
	private TechniqueMapper techniqueMapper;

	@Override
	public Action map(int index, ResultSet r, StatementContext ctx)
			throws SQLException {
		if (action == null || action.getActionID() != r.getInt("action_id")) {
			techniqueMapper = new TechniqueMapper();
			action = new Action(r.getInt("action_id"),
					r.getInt("combatant_id"), r.getInt("weapon_id"),
					r.getInt("round"), r.getInt("initiative"));
			try {
				Weapon weapon = new WeaponMapper().map(index, r, ctx);
				if (weapon.getWeaponID() > 0) {
					action.setWeapon(weapon);
				}
			} catch (Exception e) {
			}
		}
		try {
			Technique technique = techniqueMapper.map(index, r, ctx);
			if (technique.getTechniqueID() > 0) {
				action.addTechnique(technique);
			}
		} catch (Exception e) {
		}

		return action;
	}
}