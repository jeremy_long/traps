package traps.dao;

import java.util.Set;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import traps.api.Encounter;

@RegisterMapper(EncounterMapper.class)
public interface EncounterDAO extends Transactional<EncounterDAO> {

	@SqlQuery("SELECT * FROM encounters LEFT JOIN combatants ON (encounters.encounter_id = combatants.encounter_id) LEFT JOIN characters ON (combatants.character_id = characters.character_id) LEFT JOIN classes ON (characters.character_class = classes.class_id) LEFT JOIN character_notes ON (characters.character_id = character_notes.character_id) LEFT JOIN character_weapons ON (characters.character_id = character_weapons.character_id) LEFT JOIN weapons ON (character_weapons.weapon_id = weapons.weapon_id) LEFT JOIN combat_skills ON (weapons.combat_skill = combat_skills.combat_skill_id) LEFT JOIN character_armor ON (characters.character_id = character_armor.character_id) LEFT JOIN armor ON (character_armor.armor_id = armor.armor_id) LEFT JOIN combatant_status_effects ON (combatants.combatant_id = combatant_status_effects.combatant_id) LEFT JOIN status_effects ON (combatant_status_effects.status_effect_id = status_effects.status_effect_id) LEFT JOIN status_effect_modifiers ON (status_effects.status_effect_id = status_effect_modifiers.status_effect_id) ORDER BY encounters.encounter_id ASC, combatants.combatant_id ASC, characters.character_id ASC, character_notes.character_note_id ASC, weapons.weapon_id ASC, armor.armor_id ASC, status_effects.status_effect_id ASC")
	public Set<Encounter> getAll();

	@SqlQuery("SELECT * FROM encounters  LEFT JOIN combatants ON (encounters.encounter_id = combatants.encounter_id) LEFT JOIN characters ON (combatants.character_id = characters.character_id) LEFT JOIN classes ON (characters.character_class = classes.class_id) LEFT JOIN character_notes ON (characters.character_id = character_notes.character_id) LEFT JOIN character_weapons ON (characters.character_id = character_weapons.character_id) LEFT JOIN weapons ON (character_weapons.weapon_id = weapons.weapon_id) LEFT JOIN combat_skills ON (weapons.combat_skill = combat_skills.combat_skill_id) LEFT JOIN character_armor ON (characters.character_id = character_armor.character_id) LEFT JOIN armor ON (character_armor.armor_id = armor.armor_id) LEFT JOIN combatant_status_effects ON (combatants.combatant_id = combatant_status_effects.combatant_id) LEFT JOIN status_effects ON (combatant_status_effects.status_effect_id = status_effects.status_effect_id) LEFT JOIN status_effect_modifiers ON (status_effects.status_effect_id = status_effect_modifiers.status_effect_id) WHERE encounters.encounter_id = :encounter_id ORDER BY encounters.encounter_id ASC, combatants.combatant_id ASC, characters.character_id ASC, character_notes.character_note_id ASC, weapons.weapon_id ASC, armor.armor_id ASC, status_effects.status_effect_id ASC")
	public Set<Encounter> getByID(@Bind("encounter_id") int encounterID);

	@SqlQuery("SELECT current_round FROM encounters WHERE encounters.encounter_id = :encounter_id")
	public int getCurrentRoundByID(@Bind("encounter_id") int encounterID);

	@GetGeneratedKeys
	@SqlUpdate("INSERT INTO encounters (encounter_name, updated, current_round) VALUES (:encounter_name, now(), :current_round)")
	public int add(@BindEncounter Encounter encounter);

	@SqlUpdate("DELETE FROM encounters WHERE encounter_id = :encounter_id")
	public void deleteByID(@Bind("encounter_id") int encounterID);

	@SqlUpdate("UPDATE encounters SET encounter_name = :encounter_name, updated = now(), current_round = :current_round")
	public void update(@BindEncounter Encounter encounter);
}