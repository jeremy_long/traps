package traps.dao;

import java.util.Set;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import traps.api.CharacterClass;

@RegisterMapper(CharacterClassMapper.class)
public interface CharacterClassDAO extends Transactional<CharacterClassDAO> {

	@SqlQuery("SELECT * FROM classes ORDER BY class_id ASC")
	public Set<CharacterClass> getAll();

	@SqlQuery("SELECT * FROM classes WHERE class_id = :class_id ORDER BY class_id ASC")
	public CharacterClass getByID(@Bind("class_id") int classID);

	@GetGeneratedKeys
	@SqlUpdate("INSERT INTO classes (class_name, updated, hp_level_bonus, ep_level_bonus, strength_initial_bonus, dexterity_initial_bonus, agility_initial_bonus, health_initial_bonus, cunning_initial_bonus, willpower_initial_bonus, intelligence_initial_bonus, attribute_initial_bonus, hp_initial_bonus, ep_initial_bonus, skill_initial_bonus, passive_initial_bonus) VALUES (:class_name, now(), :hp_level_bonus, :ep_level_bonus, :strength_initial_bonus, :dexterity_initial_bonus, :agility_initial_bonus, :health_initial_bonus, :cunning_initial_bonus, :willpower_initial_bonus, :intelligence_initial_bonus, :attribute_initial_bonus, :hp_initial_bonus, :ep_initial_bonus, :skill_initial_bonus, :passive_initial_bonus)")
	public int add(@BindCharacterClass CharacterClass characterClass);

	@SqlUpdate("DELETE FROM classes WHERE class_id = :class_id")
	public void deleteByID(@Bind("class_id") int characterClassID);

	@SqlUpdate("UPDATE classes SET class_name = :class_name, updated = now(), hp_level_bonus = :hp_level_bonus, ep_level_bonus = :ep_level_bonus, strength_initial_bonus = :strength_initial_bonus, dexterity_initial_bonus = :dexterity_initial_bonus, agility_initial_bonus = :agility_initial_bonus, health_initial_bonus = :health_initial_bonus, cunning_initial_bonus = :cunning_initial_bonus, willpower_initial_bonus = :willpower_initial_bonus, intelligence_initial_bonus = :intelligence_initial_bonus, attribute_initial_bonus = :attribute_initial_bonus, hp_initial_bonus = :hp_initial_bonus, ep_initial_bonus = :ep_initial_bonus, skill_initial_bonus = :skill_initial_bonus, passive_initial_bonus = :passive_initial_bonus WHERE class_id = :class_id")
	public void update(@BindCharacterClass CharacterClass characterClass);
}
