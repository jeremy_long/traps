package traps.dao;

import java.util.Set;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import traps.api.Action;

@RegisterMapper(ActionMapper.class)
public interface ActionDAO extends Transactional<ActionDAO> {

	@SqlQuery("SELECT * FROM actions LEFT JOIN action_techniques ON (actions.action_id = action_techniques.action_id) LEFT JOIN techniques ON (action_techniques.technique_id = techniques.technique_id) LEFT JOIN weapons ON (actions.weapon_id = weapons.weapon_id) LEFT JOIN combat_skills ON (weapons.combat_skill = combat_skills.combat_skill_id) ORDER BY actions.action_id ASC, techniques.technique_id ASC")
	public Set<Action> getAll();

	@SqlQuery("SELECT * FROM actions LEFT JOIN action_techniques ON (actions.action_id = action_techniques.action_id) LEFT JOIN techniques ON (action_techniques.technique_id = techniques.technique_id) LEFT JOIN weapons ON (actions.weapon_id = weapons.weapon_id) LEFT JOIN combat_skills ON (weapons.combat_skill = combat_skills.combat_skill_id) WHERE actions.action_id = :action_id ORDER BY actions.action_id ASC")
	public Set<Action> getByID(@Bind("action_id") int actionID);

	@SqlQuery("SELECT * FROM actions LEFT JOIN action_techniques ON (actions.action_id = action_techniques.action_id) LEFT JOIN techniques ON (action_techniques.technique_id = techniques.technique_id) LEFT JOIN weapons ON (actions.weapon_id = weapons.weapon_id) INNER JOIN combat_skills ON (weapons.combat_skill = combat_skills.combat_skill_id) WHERE actions.combatant_id = :combatant_id ORDER BY actions.round DESC, actions.action_id ASC, actions.initiative DESC")
	public Set<Action> getByCombatantID(@Bind("combatant_id") int combatantID);

	@GetGeneratedKeys
	@SqlUpdate("INSERT INTO actions (combatant_id, weapon_id, round, initiative) VALUES (:combatant_id, :weapon_id, :round, :initiative)")
	public int add(@BindAction Action action);

	@SqlUpdate("DELETE FROM actions WHERE action_id = :action_id")
	public void deleteByID(@Bind("action_id") int actionID);

	@SqlUpdate("UPDATE actions SET weapon_id = :weapon_id, initiative = :initiative WHERE action_id = :action_id")
	public void update(@BindAction Action action);

	@SqlUpdate("INSERT INTO action_techniques (action_id, technique_id) VALUES (:action_id, :technique_id)")
	public void addTechnique(@Bind("action_id") int actionID,
			@Bind("technique_id") int techniqueID);

	@SqlUpdate("DELETE FROM action_techniques WHERE action_id = :action_id AND technique_id = :technique_id")
	public void removeTechnique(@Bind("action_id") int actionID,
			@Bind("technique_id") int techniqueID);
}