package traps.service;

import java.io.IOException;
import java.net.URL;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

@Path("/traps")
public class ServiceResource {

	@GET
	public Response index() {
		String pageContent = "";
		try {
			URL clientPage = Resources.getResource("assets/index.html");
			pageContent = Resources.toString(clientPage, Charsets.UTF_8);
		} catch (IOException e) {
			return Response.serverError().build();
		}
		return Response.ok(pageContent).build();
	}

}
