package traps.service;

import org.skife.jdbi.v2.DBI;

import traps.dao.ActionDAO;
import traps.dao.ArmorDAO;
import traps.dao.CharacterClassDAO;
import traps.dao.CharacterDAO;
import traps.dao.CharacterNoteDAO;
import traps.dao.CombatSkillDAO;
import traps.dao.CombatantDAO;
import traps.dao.EncounterDAO;
import traps.dao.StatusEffectDAO;
import traps.dao.TechniqueDAO;
import traps.dao.WeaponDAO;
import traps.resources.ActionResource;
import traps.resources.ArmorResource;
import traps.resources.CharacterClassResource;
import traps.resources.CharacterNoteResource;
import traps.resources.CharacterResource;
import traps.resources.CombatSkillResource;
import traps.resources.CombatantResource;
import traps.resources.EncounterResource;
import traps.resources.StatusEffectResource;
import traps.resources.TechniqueResource;
import traps.resources.WeaponResource;
import traps.util.DownloadFilter;

import com.bazaarvoice.dropwizard.assets.ConfiguredAssetsBundle;
import com.yammer.dropwizard.Service;
import com.yammer.dropwizard.config.Bootstrap;
import com.yammer.dropwizard.config.Environment;
import com.yammer.dropwizard.db.DatabaseConfiguration;
import com.yammer.dropwizard.jdbi.DBIFactory;
import com.yammer.dropwizard.migrations.MigrationsBundle;

public class TRAPSService extends Service<TRAPSServiceConfiguration> {
	public static final String APP_NAME = "TRAPS";

	@Override
	public void initialize(Bootstrap<TRAPSServiceConfiguration> bootstrap) {
		bootstrap.setName(APP_NAME);

		// enable asset loading and caching
		bootstrap.addBundle(new ConfiguredAssetsBundle("/assets", "/assets"));

		bootstrap.addBundle(new MigrationsBundle<TRAPSServiceConfiguration>() {
			@Override
			public DatabaseConfiguration getDatabaseConfiguration(
					TRAPSServiceConfiguration configuration) {
				return configuration.getDatabaseConfiguration();
			}
		});
	}

	@Override
	public void run(TRAPSServiceConfiguration configuration,
			Environment environment) throws Exception {
		// connect to database
		final DBIFactory factory = new DBIFactory();
		final DBI db = factory.build(
				environment,
				configuration.getDatabaseConfiguration(),
				configuration.getDatabaseConfiguration().getProperties()
						.get("databaseName"));

		// provide data access objects
		final CharacterClassDAO classDAO = db.onDemand(CharacterClassDAO.class);
		final CharacterDAO characterDAO = db.onDemand(CharacterDAO.class);
		final CharacterNoteDAO characterNoteDAO = db
				.onDemand(CharacterNoteDAO.class);
		final CombatSkillDAO combatSkillDAO = db.onDemand(CombatSkillDAO.class);
		final WeaponDAO weaponDAO = db.onDemand(WeaponDAO.class);
		final ArmorDAO armorDAO = db.onDemand(ArmorDAO.class);
		final TechniqueDAO techniqueDAO = db.onDemand(TechniqueDAO.class);
		final EncounterDAO encounterDAO = db.onDemand(EncounterDAO.class);
		final CombatantDAO combatantDAO = db.onDemand(CombatantDAO.class);
		final ActionDAO actionDAO = db.onDemand(ActionDAO.class);
		final StatusEffectDAO statusEffectDAO = db
				.onDemand(StatusEffectDAO.class);

		environment.addResource(new ServiceResource());
		environment.addResource(new CharacterClassResource(classDAO,
				characterDAO));
		environment.addResource(new CharacterResource(characterDAO, classDAO));
		environment.addResource(new CharacterNoteResource(characterNoteDAO));
		environment.addResource(new CombatSkillResource(combatSkillDAO));
		environment.addResource(new WeaponResource(weaponDAO, combatSkillDAO));
		environment.addResource(new ArmorResource(armorDAO));
		environment.addResource(new TechniqueResource(techniqueDAO));
		environment.addResource(new EncounterResource(encounterDAO, actionDAO,
				combatantDAO));
		environment.addResource(new CombatantResource(combatantDAO,
				characterDAO, actionDAO, encounterDAO));
		environment.addResource(new ActionResource(actionDAO, combatantDAO,
				weaponDAO));
		environment.addResource(new StatusEffectResource(statusEffectDAO));

		environment.addFilter(new DownloadFilter(), "/assets/txt/*");

		// Disable caching
		environment.addFilter(new CacheControlFilter(), "/*");
	}

	public static void main(String... args) throws Exception {
		new TRAPSService().run(args);
	}
}