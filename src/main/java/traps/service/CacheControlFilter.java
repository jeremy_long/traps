package traps.service;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * This filter is used to add the Cache-Control and Pragma headers to HTTP
 * responses to prevent sensitive data from being cached.
 * </p>
 * 
 * @see <a href="http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html">HTTP
 *      header specs</a>
 */
public class CacheControlFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// No action required
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		httpResponse.addHeader("Cache-Control",
				"no-cache, no-store, must-revalidate");
		httpResponse.addHeader("Pragma", "no-cache");
		httpResponse.addHeader("Expires", "0");
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// No action required
	}

}
