#!/bin/bash

mvn clean package && java -jar target/traps-1.0.0-SNAPSHOT.jar db migrate src/main/resources/webapp.yaml && java -jar target/traps-1.0.0-SNAPSHOT.jar server src/main/resources/webapp.yaml