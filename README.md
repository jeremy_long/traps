# TRAPS

Web application for managing TRAPS characters. For more details on the TRAPS rules, see http://jeremy.longsquared.com

To run, execute:

./deploy.sh

and visit localhost:9090/traps in your favorite web browser.

Requires:

- Maven

- src/main/resources/webapp.yaml must contain connection information for an instance of PostgreSQL

The application includes a REST API developed using Dropwizard, which connects through JDBI to a PostgreSQL database. The application frontend is developed in Backbone.js, which consumes the output 
of the REST API. Twitter Bootstrap is used for styling.